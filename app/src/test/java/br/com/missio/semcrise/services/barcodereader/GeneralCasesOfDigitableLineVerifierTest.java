package br.com.missio.semcrise.services.barcodereader;

import org.junit.Test;

public class GeneralCasesOfDigitableLineVerifierTest extends DigitableLineTestCase {

    @Test
    public void test_first_field_for_digitable_line_with_non_existent_identification_of_real_value_or_reference() {
        givenDigitableLineWith("1111111111111");
        whenTestDigitableLine();
        thenFieldValidityShouldBe(false);
    }
}
package br.com.missio.semcrise.dao;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.rule.PowerMockRule;

import static org.junit.Assert.*;

import br.com.missio.semcrise.MockSupport;
import br.com.missio.semcrise.model.Creditor;
import io.realm.Realm;
import io.realm.RealmQuery;

@PrepareForTest({Realm.class, RealmQuery.class})
public class CreditorDaoTest {
    private String code;
    private Creditor creditor;
    private CreditorDao creditorManager;

    @Rule
    public PowerMockRule rule = new PowerMockRule();

    @Before
    public void setUp() throws Exception {
        creditor = new Creditor();
    }

    @Test
    public void test_get_bank() {
        creditorManager = new CreditorDao(MockSupport.mockBradescoRealm());
        givenCreditorCode(MockSupport.BRADESCO_CODE);
        whenGetCreditor();
        thenBankShouldBe(MockSupport.getBradescoBank());
    }

    @Test
    public void test_get_company() {
        creditorManager = new CreditorDao(MockSupport.mockNetVirtuaRealm());
        givenCreditorCode(MockSupport.NET_VIRTUA_CODE);
        whenGetCreditor();
        thenBankShouldBe(MockSupport.getNetVirtuaCompany());
    }

    private void givenCreditorCode(String code) {
        this.code = code;
    }

    private void whenGetCreditor() {
        this.creditor = creditorManager.getCreditorByCode(code);
    }

    private void thenBankShouldBe(Creditor expectedCreditor) {
        assertEquals(expectedCreditor, creditor);
    }
}
package br.com.missio.semcrise.services.barcodereader;

import org.junit.Test;

import br.com.missio.semcrise.exceptions.BarcodeInvalidException;
import br.com.missio.semcrise.exceptions.DigitableLineWrongSizeException;
import br.com.missio.semcrise.utils.BarcodeTestCase;

import static org.junit.Assert.*;

public class DigitableLineParserTest extends BarcodeTestCase{

    private static final String BANK_SLIP_DIGITABLE_LINE = "23794600139000368321734999999708270230000012697";
    public static final String BANK_SLIP_PART_OF_DIGITABLE_LINE = "2379460013900036832173499999970827023000";
    private static final String BANK_SLIP_DIGITABLE_LINE_OUT_OF_BOUNDS = "23794600139000368321734999999708270230000012697123";
    private static final String BANK_SLIP_DIGITABLE_LINE_WITH_INVALID_BARCODE = "23794600139000368321734999999708270230000012345";
    private static final String DEALER_DIGITABLE_LINE = "846200000004837802962014407152370000000465307882";
    private String digitableLine;
    private String barcode;

    @Test
    public void test_extractBarcode_for_bank_slip_digitable_line() throws Exception {
        given(BANK_SLIP_DIGITABLE_LINE);
        whenExtractBarcode();
        thenBarcodeShouldBe(BANK_SLIP_BARCODE);
    }

    @Test
    public void test_extractBarcode_for_dealer_digitable_line() throws Exception {
        given(DEALER_DIGITABLE_LINE);
        whenExtractBarcode();
        thenBarcodeShouldBe(DEALER_COMPANY_WITHOUT_CNPJ_BARCODE);
    }

    @Test(expected = DigitableLineWrongSizeException.class)
    public void test_throws_exception_when_digitable_line_is_out_of_bounds() throws Exception {
        given(BANK_SLIP_DIGITABLE_LINE_OUT_OF_BOUNDS);
        whenExtractBarcode();
    }

    @Test(expected = DigitableLineWrongSizeException.class)
    public void test_throws_exception_when_digitable_line_is_lower_then_right_size() throws Exception {
        given(BANK_SLIP_PART_OF_DIGITABLE_LINE);
        whenExtractBarcode();
    }

    @Test(expected = BarcodeInvalidException.class)
    public void test_throws_exception_when_digitable_line_has_invalid_barcode() throws Exception {
        given(BANK_SLIP_DIGITABLE_LINE_WITH_INVALID_BARCODE);
        whenExtractBarcode();
    }

    private void given(String digitableLine) {
        this.digitableLine = digitableLine;
    }

    private void whenExtractBarcode() throws DigitableLineWrongSizeException, BarcodeInvalidException {
        barcode = DigitableLineParser.extractBarcode(digitableLine);
    }

    private void thenBarcodeShouldBe(String expectedBarcode) {
        assertEquals(expectedBarcode,barcode);
    }
}
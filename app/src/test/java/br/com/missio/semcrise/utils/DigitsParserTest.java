package br.com.missio.semcrise.utils;

import org.junit.Test;

import java.util.Arrays;

import static org.junit.Assert.*;

public class DigitsParserTest extends BarcodeTestCase{
    private int[] barcodeDigits;

    @Test
    public void test_getDigitsWithoutCodeChecker() throws Exception {
        givenBarcode(FAKE_BARCODE);
        whenGetBarcodeDigits();
        thenBarcodeDigitsShouldBe(1,2,3,4,6,7,8,9,0,0,9,5,8,7,6,5,4,3,2,1,2,3,4,5,6,7,7,7,8,9,0,9,8,7,7,6,6,5,4,3,3,2,1);
    }

    private void thenBarcodeDigitsShouldBe(int ... expectedBarcodeDigts) {
        assertEquals(Arrays.toString(expectedBarcodeDigts), Arrays.toString(barcodeDigits));
    }

    private void whenGetBarcodeDigits() throws Exception {
        barcodeDigits = new DigitsParser(getGivenBarcode()).getDigitsWithoutCodeChecker(Constants.BANK_SLIP_CODE_CHECKER_POSITION);
    }
}
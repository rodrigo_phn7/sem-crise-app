package br.com.missio.semcrise.dao;

import android.os.Build;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.rule.PowerMockRule;
import org.robolectric.RobolectricGradleTestRunner;
import org.robolectric.annotation.Config;

import br.com.missio.semcrise.BuildConfig;
import br.com.missio.semcrise.MockSupport;
import br.com.missio.semcrise.model.User;
import io.realm.Realm;
import io.realm.RealmQuery;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@RunWith(RobolectricGradleTestRunner.class)
@Config(constants = BuildConfig.class,sdk = Build.VERSION_CODES.LOLLIPOP)
@PrepareForTest({Realm.class, RealmQuery.class, User.class})
public class UserDaoTest {

    UserDao userDataManager;
    Realm realmMock;
    User fakeUser;
    @Rule
    public PowerMockRule rule = new PowerMockRule();

    @Before
    public void setUp() throws Exception {
        realmMock = MockSupport.mockRealm();
        userDataManager = new UserDao(realmMock);
    }

    private User givenAFakeUserWith(String email, String name, String phone, String pass) {
        fakeUser = new User();
        fakeUser.setName(name);
        fakeUser.setEmail(email);
        fakeUser.setPhone(phone);
        fakeUser.setPass(pass);
        return fakeUser;
    }

    @Test
    public void user_is_added_in_database() {
        User fakeUser = givenAFakeUserWith("someone@fake.com","Foo People","5521977775555","123456");

        //when
        userDataManager.add(fakeUser);

        //then
        verify(realmMock, times(1)).beginTransaction();
        verify(realmMock, times(1)).copyToRealm(fakeUser);
        verify(realmMock, times(1)).commitTransaction();
    }

    @Test
    public void get_only_one_user_from_database_by_email() {
        //given
        String email = "someone@fake.com";

        //when
        User someone = userDataManager.getUserDataBy(email);

        //then
        assertEquals(email, someone.getEmail());

        verify(realmMock, times(1)).where(User.class);
        verify(realmMock.where(User.class), times(1)).equalTo("email", email);
        verify(realmMock.where(User.class), times(1)).findFirst();
    }

    @Test
    public void user_basic_contract() {
        String email = "someone@fake.com";
        givenAFakeUserWith(email, "Foo People", "5521977775555", "123456");

        User someone = userDataManager.getUserDataBy(email);
        Boolean isUserOK = fakeUser.equals(someone);
        assertEquals(isUserOK, true);
    }

    @Test
    public void update_user(){
        User fakeUser = givenAFakeUserWith("updated@fake.com","Foop People","5521979975555","123456");

        //when
        userDataManager.update(fakeUser);

        //then
        verify(realmMock, times(1)).beginTransaction();
        verify(realmMock, times(1)).copyToRealmOrUpdate(fakeUser);
        verify(realmMock, times(1)).commitTransaction();
    }

}
package br.com.missio.semcrise.model;

import org.junit.Test;

import static org.junit.Assert.*;

public class MoneyTest {

    private Money money;
    private String formattedMoney;

    @Test
    public void testToString() throws Exception {
        given(130.5);
        whenToString();
        thenFormattedMoneyShouldBe("R$ 130,50");
    }

    @Test
    public void testToString_with_non_decimal_value() throws Exception {
        given(12d);
        whenToString();
        thenFormattedMoneyShouldBe("R$ 12,00");
    }

    @Test
    public void testToString_rounding_to_down() throws Exception {
        given(12.321);
        whenToString();
        thenFormattedMoneyShouldBe("R$ 12,32");
    }

    @Test
    public void testToString_rounding_to_up() throws Exception {
        given(12.328);
        whenToString();
        thenFormattedMoneyShouldBe("R$ 12,33");
    }

    @Test
    public void testToString_rounding() throws Exception {
        given(12.325);
        whenToString();
        thenFormattedMoneyShouldBe("R$ 12,32");
    }

    @Test
    public void testToString_returning_zero_money_value_given_null_parameter() throws Exception {
        given(null);
        whenToString();
        thenFormattedMoneyShouldBe("R$ 0,00");
    }

    private void given(Double decimalValue) {
        money = new Money(decimalValue);
    }

    private void whenToString() {
        formattedMoney = money.toString();
    }

    private void thenFormattedMoneyShouldBe(String expectedFormattedMoney) {
        assertEquals(expectedFormattedMoney,formattedMoney);
    }
}
package br.com.missio.semcrise.services.barcodereader;

import org.junit.Test;

public class BankSlipDigitableLineVerifierTest extends DigitableLineTestCase {

    private static final String BANK_SLIP_FIRST_FIELD = "2379460013";
    private static final String BANK_SLIP_SECOND_FIELD = "90003683217";
    private static final String BANK_SLIP_THIRD_FIELD = "34999999708";
    private static final String BANK_SLIP_PART_OF_SECOND_FIELD = "9000";
    private static final String BANK_SLIP_WRONG_FIRST_FIELD = "2378460013";
    private static final String BANK_SLIP_WRONG_SECOND_FIELD = "90004683217";
    private static final String BANK_SLIP_WRONG_THIRD_FIELD = "34999599708";
    private static final String BANK_SLIP_PART_OF_THIRD_FIELD = "349999";
    private static final String BANK_SLIP_PART_OF_END_OF_DIGITABLE_LINE = "3213499";

    @Test
    public void test_bank_slip_first_field_isValid() {
        givenDigitableLineWith(BANK_SLIP_FIRST_FIELD);
        whenTestDigitableLine();
        thenFieldValidityShouldBe(true);
    }

    @Test
    public void test_bank_slip_first_field_isValid_while_user_is_typing_to_second_field() {
        givenDigitableLineWith(BANK_SLIP_FIRST_FIELD + BANK_SLIP_PART_OF_SECOND_FIELD);
        whenTestDigitableLine();
        thenFieldValidityShouldBe(true);
    }

    @Test
    public void test_bank_slip_first_field_is_NOT_Valid_while_user_is_typing_to_second_field() {
        givenDigitableLineWith(BANK_SLIP_WRONG_FIRST_FIELD + BANK_SLIP_PART_OF_SECOND_FIELD);
        whenTestDigitableLine();
        thenFieldValidityShouldBe(false);
    }

    @Test
    public void test_bank_slip_second_field_isValid() {
        givenDigitableLineWith(BANK_SLIP_FIRST_FIELD + BANK_SLIP_SECOND_FIELD);
        whenTestDigitableLine();
        thenFieldValidityShouldBe(true);
    }

    @Test
    public void test_bank_slip_second_field_isValid_while_user_is_typing_to_third_field() {
        givenDigitableLineWith(BANK_SLIP_FIRST_FIELD + BANK_SLIP_SECOND_FIELD + BANK_SLIP_PART_OF_THIRD_FIELD);
        whenTestDigitableLine();
        thenFieldValidityShouldBe(true);
    }

    @Test
    public void test_bank_slip_second_field_is_NOT_Valid_while_user_is_typing_to_third_field() {
        givenDigitableLineWith(BANK_SLIP_FIRST_FIELD + BANK_SLIP_WRONG_SECOND_FIELD
                + BANK_SLIP_PART_OF_THIRD_FIELD);
        whenTestDigitableLine();
        thenFieldValidityShouldBe(false);
    }

    @Test
    public void test_bank_slip_third_field_isValid() {
        givenDigitableLineWith(BANK_SLIP_FIRST_FIELD + BANK_SLIP_SECOND_FIELD + BANK_SLIP_THIRD_FIELD);
        whenTestDigitableLine();
        thenFieldValidityShouldBe(true);
    }

    @Test
    public void test_bank_slip_third_field_isValid_while_user_is_typing_to_end_of_digitable_line() {
        givenDigitableLineWith(BANK_SLIP_FIRST_FIELD + BANK_SLIP_SECOND_FIELD + BANK_SLIP_THIRD_FIELD
                + BANK_SLIP_PART_OF_END_OF_DIGITABLE_LINE);
        whenTestDigitableLine();
        thenFieldValidityShouldBe(true);
    }

    @Test
    public void test_bank_slip_third_field_is_NOT_Valid_while_user_is_typing_to_end_of_digitable_line() {
        givenDigitableLineWith(BANK_SLIP_FIRST_FIELD + BANK_SLIP_SECOND_FIELD
                + BANK_SLIP_WRONG_THIRD_FIELD + BANK_SLIP_PART_OF_END_OF_DIGITABLE_LINE);
        whenTestDigitableLine();
        thenFieldValidityShouldBe(false);
    }
}
package br.com.missio.semcrise.utils;

import org.junit.Test;

import br.com.missio.semcrise.exceptions.InvalidBarcodeBillValueException;

import static org.junit.Assert.*;

public class BarcodeBillValueParserTest {

    private String barcodeBillValue;
    private Double billValue;

    @Test
    public void test_parse_bill_value() throws Exception {
        givenBarcodeBillValue("0000012697");
        whenParseBillValue();
        thenBillValueShouldBe(126.97);
    }

    @Test(expected = InvalidBarcodeBillValueException.class)
    public void test_throw_exception_when_barcode_bill_value_is_invalid() throws Exception {
        givenBarcodeBillValue("97");
        whenParseBillValue();
    }

    @Test(expected = InvalidBarcodeBillValueException.class)
    public void test_throw_exception_when_barcode_bill_value_is_invalid_given_null_barcode_bill_value() throws Exception {
        givenBarcodeBillValue(null);
        whenParseBillValue();
    }

    private void givenBarcodeBillValue(String barcodeBillValue) {
        this.barcodeBillValue = barcodeBillValue;
    }

    private void whenParseBillValue() throws InvalidBarcodeBillValueException {
        billValue = BarcodeBillValueParser.parseBillValue(barcodeBillValue);
    }

    private void thenBillValueShouldBe(Double expectedBillValue) {
        assertEquals(expectedBillValue,billValue);
    }
}
package br.com.missio.semcrise.services.barcodereader;

import org.junit.Test;

public class CompanyDealerDigitableLineVerifierTest extends DigitableLineTestCase {

    private static final String COMPANY_DEALER_FIRST_FIELD = "846200000004";
    private static final String COMPANY_DEALER_INVALID_FIRST_FIELD = "846200001114";
    private static final String COMPANY_DEALER_PART_OF_SECOND_FIELD = "8378029";
    private static final String COMPANY_DEALER_SECOND_FIELD = "837802962014";
    private static final String COMPANY_DEALER_INVALID_SECOND_FIELD = "837802962114";
    private static final String COMPANY_DEALER_PART_OF_THIRD_FIELD = "40715237";
    private static final String COMPANY_DEALER_THIRD_FIELD = "407152370000";
    private static final String COMPANY_DEALER_INVALID_THIRD_FIELD = "407152371110";
    private static final String COMPANY_DEALER_PART_OF_FOURTH_FIELD = "0004653";
    private static final String COMPANY_DEALER_FOURTH_FIELD = "000465307882";
    private static final String COMPANY_DEALER_INVALID_FOURTH_FIELD = "000465307112";

    @Test
    public void test_company_dealer_first_field_isValid() {
        givenDigitableLineWith(COMPANY_DEALER_FIRST_FIELD);
        whenTestDigitableLine();
        thenFieldValidityShouldBe(true);
    }

    @Test
    public void test_company_dealer_first_field_is_NOT_Valid() {
        givenDigitableLineWith(COMPANY_DEALER_INVALID_FIRST_FIELD);
        whenTestDigitableLine();
        thenFieldValidityShouldBe(false);
    }

    @Test
    public void test_company_dealer_first_field_isValid_while_user_is_typing_to_second_field() {
        givenDigitableLineWith(COMPANY_DEALER_FIRST_FIELD + COMPANY_DEALER_PART_OF_SECOND_FIELD);
        whenTestDigitableLine();
        thenFieldValidityShouldBe(true);
    }

    @Test
    public void test_company_dealer_second_field_isValid() {
        givenDigitableLineWith(COMPANY_DEALER_FIRST_FIELD + COMPANY_DEALER_SECOND_FIELD);
        whenTestDigitableLine();
        thenFieldValidityShouldBe(true);
    }

    @Test
    public void test_company_dealer_second_field_is_NOT_Valid() {
        givenDigitableLineWith(COMPANY_DEALER_FIRST_FIELD + COMPANY_DEALER_INVALID_SECOND_FIELD);
        whenTestDigitableLine();
        thenFieldValidityShouldBe(false);
    }

    @Test
    public void test_company_dealer_second_field_isValid_while_user_is_typing_to_third_field() {
        givenDigitableLineWith(COMPANY_DEALER_FIRST_FIELD
                + COMPANY_DEALER_SECOND_FIELD + COMPANY_DEALER_PART_OF_THIRD_FIELD);
        whenTestDigitableLine();
        thenFieldValidityShouldBe(true);
    }

    @Test
    public void test_company_dealer_third_field_isValid() {
        givenDigitableLineWith(COMPANY_DEALER_FIRST_FIELD + COMPANY_DEALER_SECOND_FIELD + COMPANY_DEALER_THIRD_FIELD);
        whenTestDigitableLine();
        thenFieldValidityShouldBe(true);
    }

    @Test
    public void test_company_dealer_third_field_is_NOT_Valid() {
        givenDigitableLineWith(COMPANY_DEALER_FIRST_FIELD + COMPANY_DEALER_SECOND_FIELD
                + COMPANY_DEALER_INVALID_THIRD_FIELD);
        whenTestDigitableLine();
        thenFieldValidityShouldBe(false);
    }

    @Test
    public void test_company_dealer_third_field_isValid_while_user_is_typing_to_fourth_field() {
        givenDigitableLineWith(COMPANY_DEALER_FIRST_FIELD
                + COMPANY_DEALER_SECOND_FIELD + COMPANY_DEALER_THIRD_FIELD
                + COMPANY_DEALER_PART_OF_FOURTH_FIELD);
        whenTestDigitableLine();
        thenFieldValidityShouldBe(true);
    }

    @Test
    public void test_company_dealer_fourth_field_isValid() {
        givenDigitableLineWith(COMPANY_DEALER_FIRST_FIELD + COMPANY_DEALER_SECOND_FIELD
                + COMPANY_DEALER_THIRD_FIELD + COMPANY_DEALER_FOURTH_FIELD);
        whenTestDigitableLine();
        thenFieldValidityShouldBe(true);
    }

    @Test
    public void test_company_dealer_fourth_field_is_NOT_Valid() {
        givenDigitableLineWith(COMPANY_DEALER_FIRST_FIELD + COMPANY_DEALER_SECOND_FIELD
                + COMPANY_DEALER_THIRD_FIELD + COMPANY_DEALER_INVALID_FOURTH_FIELD);
        whenTestDigitableLine();
        thenFieldValidityShouldBe(false);
    }
}
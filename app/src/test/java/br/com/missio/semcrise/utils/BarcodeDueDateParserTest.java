package br.com.missio.semcrise.utils;

import org.junit.Rule;
import org.junit.Test;
import org.powermock.modules.junit4.rule.PowerMockRule;

import java.util.Calendar;

import br.com.missio.semcrise.MockSupport;
import br.com.missio.semcrise.exceptions.InvalidLiteralDueDateException;

import static org.junit.Assert.*;

public class BarcodeDueDateParserTest {

    private int dueDateFactor;
    private Calendar dueDate;

    @Rule
    public PowerMockRule rule = new PowerMockRule();
    private String literalDueDate;

    @Test
    public void test_get_due_date() {
        givenDueDateFactor(4789);
        whenGetDueDate();
        thenDueDateShouldBe(MockSupport.getExpectedDueDate(2010,Calendar.NOVEMBER,17));
    }

    @Test
    public void test_get_due_date_with_lower_limit_factor() {
        givenDueDateFactor(9999);
        whenGetDueDate();
        thenDueDateShouldBe(MockSupport.getExpectedDueDate(2025, Calendar.FEBRUARY, 21));
    }

    @Test
    public void test_get_due_date_with_lower_limit_factor_day_close_of_2025_year() {
        givenDueDateFactor(9999);
        whenGetDueDateAndCurrentYearIsCloseOf2025Year();
        thenDueDateShouldBe(MockSupport.getExpectedDueDate(2025, Calendar.FEBRUARY, 21));
    }

    @Test
    public void test_get_due_date_with_upper_limit_factor() {
        givenDueDateFactor(1000);
        whenGetDueDateAndCurrentYearIsCloseOf2025Year();
        thenDueDateShouldBe(MockSupport.getExpectedDueDate(2025, Calendar.FEBRUARY, 22));
    }

    @Test
    public void test_get_due_date_with_middle_limit_factor_considering_current_day() {
        givenDueDateFactor(5000);
        whenGetDueDate();
        thenDueDateShouldBe(MockSupport.getExpectedDueDate(2011, Calendar.JUNE, 16));
    }

    @Test
    public void test_get_due_date_with_middle_limit_factor_considering_day_close_of_2025_year() {
        givenDueDateFactor(5000);
        whenGetDueDateAndCurrentYearIsCloseOf2025Year();
        thenDueDateShouldBe(MockSupport.getExpectedDueDate(2011, Calendar.JUNE, 16));
    }

    @Test
    public void test_get_due_date_with_begin_limit_factor_considering_day_close_of_2025_year() {
        givenDueDateFactor(2087);
        whenGetDueDateAndCurrentYearIsCloseOf2025Year();
        thenDueDateShouldBe(MockSupport.getExpectedDueDate(2028, Calendar.FEBRUARY, 14));
    }

    @Test
    public void test_get_due_date_with_begin_limit_factor_considering_current_day() {
        givenDueDateFactor(2087);
        whenGetDueDate();
        thenDueDateShouldBe(MockSupport.getExpectedDueDate(2003, Calendar.JUNE, 25));
    }

    @Test
    public void test_get_due_date_when_due_date_is_out_of_range_upper_mode() {
        givenDueDateFactor(10000);
        whenGetDueDate();
        thenDueDateShouldBe(null);
    }

    @Test
    public void test_get_due_date_when_due_date_is_out_of_range_lower_mode() {
        givenDueDateFactor(999);
        whenGetDueDate();
        thenDueDateShouldBe(null);
    }

    @Test
    public void test_get_due_date_with_literal_description() throws InvalidLiteralDueDateException {
        givenLiteralDueDate("20140715");
        whenGetDueDateThroughLiteralFormat();
        thenDueDateShouldBe(MockSupport.getExpectedDueDate(2014, Calendar.JULY,15));
    }

    @Test(expected = InvalidLiteralDueDateException.class)
    public void test_throws_exception_with_invalid_literal_due_date() throws InvalidLiteralDueDateException {
        givenLiteralDueDate("2014075");
        whenGetDueDateThroughLiteralFormat();
    }

    @Test(expected = InvalidLiteralDueDateException.class)
    public void test_throws_exception_with_invalid_literal_due_date_more_than_8_digits() throws InvalidLiteralDueDateException {
        givenLiteralDueDate("2014075123");
        whenGetDueDateThroughLiteralFormat();
    }

    @Test(expected = InvalidLiteralDueDateException.class)
    public void test_throws_exception_with_null_literal_due_date() throws InvalidLiteralDueDateException {
        givenLiteralDueDate(null);
        whenGetDueDateThroughLiteralFormat();
    }

    @Test(expected = InvalidLiteralDueDateException.class)
    public void test_throws_exception_with_invalid_month() throws InvalidLiteralDueDateException {
        givenLiteralDueDate("20148815");
        whenGetDueDateThroughLiteralFormat();
    }

    @Test(expected = InvalidLiteralDueDateException.class)
    public void test_throws_exception_with_invalid_day_lower_limit() throws InvalidLiteralDueDateException {
        givenLiteralDueDate("20141200");
        whenGetDueDateThroughLiteralFormat();
    }

    @Test(expected = InvalidLiteralDueDateException.class)
    public void test_throws_exception_with_invalid_day_upper_limit() throws InvalidLiteralDueDateException {
        givenLiteralDueDate("20141232");
        whenGetDueDateThroughLiteralFormat();
    }

    @Test(expected = InvalidLiteralDueDateException.class)
    public void test_throws_exception_with_invalid_year_lower_limit() throws InvalidLiteralDueDateException {
        givenLiteralDueDate("18880101");
        whenGetDueDateThroughLiteralFormat();
    }

    @Test(expected = InvalidLiteralDueDateException.class)
    public void test_throws_exception_with_invalid_year_upper_limit() throws InvalidLiteralDueDateException {
        givenLiteralDueDate("31110101");
        whenGetDueDateThroughLiteralFormat();
    }

    private void givenLiteralDueDate(String literalDueDate) {
        this.literalDueDate = literalDueDate;
    }

    private void whenGetDueDateAndCurrentYearIsCloseOf2025Year() {
        Calendar mockCalendar = MockSupport.mockGetCalendar2020();
        dueDate = BarcodeDueDateParser.getDueDate(mockCalendar,dueDateFactor);
    }

    private void givenDueDateFactor(int dueDateFactor) {
        this.dueDateFactor = dueDateFactor;
    }

    private void whenGetDueDate() {
        dueDate = BarcodeDueDateParser.getDueDate(Calendar.getInstance(),dueDateFactor);
    }

    private void whenGetDueDateThroughLiteralFormat() throws InvalidLiteralDueDateException {
        dueDate = BarcodeDueDateParser.getDueDate(literalDueDate);
    }
    private void thenDueDateShouldBe(Calendar expectedDueDate) {
        if(expectedDueDate == null) {
            assertNull(dueDate);
        } else {
            int eYear = expectedDueDate.get(Calendar.YEAR);
            int year = dueDate.get(Calendar.YEAR);
            assertEquals(eYear,year);
            int eMonth = expectedDueDate.get(Calendar.MONTH);
            int month = dueDate.get(Calendar.MONTH);
            assertEquals(eMonth,month);
            int eDay = expectedDueDate.get(Calendar.DAY_OF_MONTH);
            int day = dueDate.get(Calendar.DAY_OF_MONTH);
            assertEquals(eDay,day);
        }
    }
}
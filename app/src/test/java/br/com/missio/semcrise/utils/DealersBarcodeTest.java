package br.com.missio.semcrise.utils;

import android.os.Build;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.rule.PowerMockRule;
import org.robolectric.RobolectricGradleTestRunner;
import org.robolectric.annotation.Config;

import java.util.Calendar;

import br.com.missio.semcrise.BuildConfig;
import br.com.missio.semcrise.MockSupport;
import br.com.missio.semcrise.exceptions.InvalidIdentificationOfRealValueOrReferenceException;
import br.com.missio.semcrise.exceptions.InvalidSegmentException;
import br.com.missio.semcrise.model.Creditor;
import io.realm.Realm;
import io.realm.RealmQuery;

import static org.junit.Assert.*;

@RunWith(RobolectricGradleTestRunner.class)
@Config(constants = BuildConfig.class,sdk = Build.VERSION_CODES.LOLLIPOP)
@PrepareForTest({Realm.class, RealmQuery.class})
public class DealersBarcodeTest extends BarcodeTestCase{

    private BarcodeType parsedBarcode;
    private boolean isProductIdentificationRight;
    private Segment segmentIdentification;
    private RealValueOrReference realValueOrReference;
    private Double billValue;
    private Creditor companyOrAgency;
    private Calendar dueDate;

    @Rule
    public PowerMockRule rule = new PowerMockRule();

    @Before
    public void setUp() throws Exception {
        MockSupport.mockNetVirtuaRealm();
    }

    @Test(expected = Exception.class)
    public void test_throw_exception_when_barcode_is_invalid_when_bank_slip_barcode_is_created() throws Exception {
        givenBarcode(WRONG_BARCODE);
        whenDealersBarcodeClassIsCreated();
    }

    @Test
    public void test_parse_dealer_company_without_cnpj_barcode() throws Exception {
        givenBarcode(DEALER_COMPANY_WITHOUT_CNPJ_BARCODE);
        whenDealersBarcodeClassIsCreated();
        thenParseShouldBe(MockSupport.getDealerCompanyWithoutCnpjBarcode());
    }

    @Test
    public void test_parse_dealer_public_agency_without_cnpj_barcode() throws Exception {
        givenBarcode(DEALER_PUBLIC_AGENCY_WITHOUT_CNPJ_BARCODE);
        whenDealersBarcodeClassIsCreated();
        thenParseShouldBe(MockSupport.getDealerPublicAgencyWithoutCnpjBarcode());
    }

    @Test
    public void test_is_product_identification_right() {
        givenBarcode(DEALER_COMPANY_WITHOUT_CNPJ_BARCODE);
        whenProductIdentificationIsVerified();
        thenProductIdentificationRightShouldBe(true);
    }

    @Test
    public void test_get_segment_identification_for_dealer_company_without_cnpj() throws InvalidSegmentException {
        givenBarcode(DEALER_COMPANY_WITHOUT_CNPJ_BARCODE);
        whenGetSegmentIdentification();
        thenSegmentIdentificationShouldBe(Segment.TELCOM);
    }

    @Test
    public void test_get_segment_identification_for_dealer_public_agency_without_cnpj() throws InvalidSegmentException {
        givenBarcode(DEALER_PUBLIC_AGENCY_WITHOUT_CNPJ_BARCODE);
        whenGetSegmentIdentification();
        thenSegmentIdentificationShouldBe(Segment.GOVERNMENT_AGENCIES);
    }

    @Test
    public void test_get_identification_of_real_value_or_reference_for_company_dealer() throws InvalidIdentificationOfRealValueOrReferenceException {
        givenBarcode(DEALER_COMPANY_WITHOUT_CNPJ_BARCODE);
        whenGetIdentificationOfRealValueOrReference();
        thenIdentificationOfRealValueOrReferenceShouldBe(RealValueOrReference.EFFECTIVE_MOD10);
    }

    @Test
    public void test_get_identification_of_real_value_or_reference_for_public_agency_dealer() throws InvalidIdentificationOfRealValueOrReferenceException {
        givenBarcode(DEALER_PUBLIC_AGENCY_WITHOUT_CNPJ_BARCODE);
        whenGetIdentificationOfRealValueOrReference();
        thenIdentificationOfRealValueOrReferenceShouldBe(RealValueOrReference.EFFECTIVE_MOD11);
    }

    @Test
    public void test_get_bill_value_company_dealer() throws InvalidIdentificationOfRealValueOrReferenceException {
        givenBarcode(DEALER_COMPANY_WITHOUT_CNPJ_BARCODE);
        whenGetBillValue();
        thenBillValueShouldBe(83.78);
    }

    @Test
    public void test_get_bill_value_dealer_public_agency() throws InvalidIdentificationOfRealValueOrReferenceException {
        givenBarcode(DEALER_PUBLIC_AGENCY_WITHOUT_CNPJ_BARCODE);
        whenGetBillValue();
        thenBillValueShouldBe(57.98);
    }

    @Test
    public void test_get_company_without_cnpj() {
        givenBarcode(DEALER_COMPANY_WITHOUT_CNPJ_BARCODE);
        whenGetCompanyOrAgency();
        thenCompanyOrAgencyShouldBe(MockSupport.getNetVirtuaCompany());
    }

    @Test
    public void test_get_public_agency_without_cnpj() {
        MockSupport.mockSimplesNacionalRealm();
        givenBarcode(DEALER_PUBLIC_AGENCY_WITHOUT_CNPJ_BARCODE);
        whenGetCompanyOrAgency();
        thenCompanyOrAgencyShouldBe(MockSupport.getSimplesNacionalAgency());
    }

    @Test
    public void test_get_due_date() {
        givenBarcode(DEALER_COMPANY_WITHOUT_CNPJ_BARCODE);
        whenGetDueDate();
        thenDueDateShouldBe(MockSupport.getExpectedDueDate(2014, Calendar.JULY,15));
    }

    @Test
    public void test_get_due_date_dealer_company() {
        givenBarcode(DEALER_COMPANY_WITHOUT_CNPJ_BARCODE);
        whenGetDueDate();
        thenDueDateShouldBe(MockSupport.getExpectedDueDate(2014, Calendar.JULY,15));
    }

    @Test
    public void test_get_due_date_dealer_public_agency() {
        givenBarcode(DEALER_PUBLIC_AGENCY_WITHOUT_CNPJ_BARCODE);
        whenGetDueDate();
        thenDueDateShouldBe(null);
    }

    private void whenGetCompanyOrAgency() {
        companyOrAgency = new DealersBarcode(getGivenBarcode()).getCompanyOrAgency();
    }

    private void whenGetBillValue() {
        billValue = new DealersBarcode(getGivenBarcode()).getBillValue();
    }

    private void whenGetIdentificationOfRealValueOrReference() throws InvalidIdentificationOfRealValueOrReferenceException {
        realValueOrReference = new DealersBarcode(getGivenBarcode()).getRealValueOrReference();

    }

    private void whenGetSegmentIdentification() throws InvalidSegmentException {
        segmentIdentification = new DealersBarcode(getGivenBarcode()).getSegment();
    }

    private void whenProductIdentificationIsVerified() {
        isProductIdentificationRight = new DealersBarcode(getGivenBarcode()).isProductIdentificationRight();
    }

    private void whenDealersBarcodeClassIsCreated() {
        parsedBarcode = new DealersBarcode(getGivenBarcode());
    }

    private void whenGetDueDate() {
        dueDate = new DealersBarcode(getGivenBarcode()).getDueDate();
    }

    private void thenIdentificationOfRealValueOrReferenceShouldBe(RealValueOrReference expectedRealValueOrReference) {
        assertEquals(expectedRealValueOrReference,realValueOrReference);
    }

    private void thenCompanyOrAgencyShouldBe(Creditor expectedCompanyOrAgency) {
        assertEquals(expectedCompanyOrAgency,companyOrAgency);
    }

    private void thenBillValueShouldBe(Double expectedBillValue) {
        assertEquals(expectedBillValue,billValue);
    }

    private void thenSegmentIdentificationShouldBe(Segment expectedSegment) {
        assertEquals(expectedSegment,segmentIdentification);
    }

    private void thenProductIdentificationRightShouldBe(boolean expectedProductIdentificationStatus) {
        assertEquals(expectedProductIdentificationStatus,isProductIdentificationRight);
    }

    private void thenParseShouldBe(BarcodeType expectedParsedBarcode) {
        assertEquals(expectedParsedBarcode, parsedBarcode);
    }

    private void thenDueDateShouldBe(Calendar expectedDueDate) {
        if(expectedDueDate == null) {
            assertNull(dueDate);
        } else {
            assertEquals(expectedDueDate.getTime(),dueDate.getTime());
        }
    }
}
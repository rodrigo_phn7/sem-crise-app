package br.com.missio.semcrise.model;

import org.junit.Test;

import static org.junit.Assert.*;

public class CreditorTypeTest {

    @Test
    public void test_to_string() {
        assertEquals("BANK", CreditorType.BANK.toString());
        assertEquals("COMPANY", CreditorType.COMPANY.toString());
        assertEquals("PUBLIC_AGENCY", CreditorType.PUBLIC_AGENCY.toString());
        assertEquals("PERSON", CreditorType.PERSON.toString());
    }
}
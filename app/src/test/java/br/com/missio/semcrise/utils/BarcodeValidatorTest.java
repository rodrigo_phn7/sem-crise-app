package br.com.missio.semcrise.utils;

import static org.junit.Assert.*;

import org.junit.Test;

public class BarcodeValidatorTest extends BarcodeTestCase {

    private static final boolean VALID = true;
    private boolean barcodeValidStatus;

    @Test
    public void test_bank_slip_barcode_is_valid() {
        givenBarcode(BANK_SLIP_BARCODE);
        whenValidatorIsCalled();
        thenBarcodeShouldBe(VALID);
    }

    @Test
    public void test_dealers_barcode_is_valid() {
        givenBarcode(DEALER_COMPANY_WITHOUT_CNPJ_BARCODE);
        whenValidatorIsCalled();
        thenBarcodeShouldBe(VALID);
    }

    @Test
    public void test_bank_slip_barcode_is_invalid() {
        givenBarcode(FAKE_BARCODE);
        whenValidatorIsCalled();
        thenBarcodeShouldBe(!VALID);
    }

    @Test
    public void test_barcode_is_invalid_when_barcode_has_not_only_numbers() {
        givenBarcode(WRONG_BARCODE_WITH_RIGHT_SIZE);
        whenValidatorIsCalled();
        thenBarcodeShouldBe(!VALID);
    }

    @Test
    public void test_barcode_is_invalid_when_barcode_is_out_of_type() {
        givenBarcode(BARCODE_WITH_WRONG_SIZE);
        whenValidatorIsCalled();
        thenBarcodeShouldBe(!VALID);
    }

    private void whenValidatorIsCalled() {
        barcodeValidStatus = BarcodeValidator.validate(getGivenBarcode());
    }

    private void thenBarcodeShouldBe(boolean validStatus) {
        assertEquals(validStatus,barcodeValidStatus);
    }
}
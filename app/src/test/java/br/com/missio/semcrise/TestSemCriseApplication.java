package br.com.missio.semcrise;

import android.content.Context;

import org.robolectric.TestLifecycleApplication;

import java.lang.reflect.Method;

public class TestSemCriseApplication extends SemCriseApplication implements TestLifecycleApplication{

    @Override
    protected void configRealmDatabase(Context context) {
    }

    @Override
    public void beforeTest(Method method) {

    }

    @Override
    public void prepareTest(Object test) {

    }

    @Override
    public void afterTest(Method method) {

    }
}

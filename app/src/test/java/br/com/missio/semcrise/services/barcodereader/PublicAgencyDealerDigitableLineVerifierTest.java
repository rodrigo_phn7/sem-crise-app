package br.com.missio.semcrise.services.barcodereader;

import org.junit.Test;

public class PublicAgencyDealerDigitableLineVerifierTest extends DigitableLineTestCase {

    private static final String PUBLIC_AGENCY_DEALER_FIRST_FIELD = "858000000003";
    private static final String PUBLIC_AGENCY_DEALER_INVALID_FIRST_FIELD = "858000001113";
    private static final String PUBLIC_AGENCY_DEALER_PART_OF_SECOND_FIELD = "8357980";
    private static final String PUBLIC_AGENCY_DEALER_SECOND_FIELD = "579803281613";
    private static final String PUBLIC_AGENCY_DEALER_INVALID_SECOND_FIELD = "579803255553";
    private static final String PUBLIC_AGENCY_DEALER_PART_OF_THIRD_FIELD = "02070816";
    private static final String PUBLIC_AGENCY_DEALER_THIRD_FIELD = "020708160977";
    private static final String PUBLIC_AGENCY_DEALER_INVALID_THIRD_FIELD = "021118992377";
    private static final String PUBLIC_AGENCY_DEALER_PART_OF_FOURTH_FIELD = "4283426";
    private static final String PUBLIC_AGENCY_DEALER_FOURTH_FIELD = "428342663955";
    private static final String PUBLIC_AGENCY_DEALER_INVALID_FOURTH_FIELD = "342831363955";

    @Test
    public void test_public_agency_dealer_first_field_isValid() {
        givenDigitableLineWith(PUBLIC_AGENCY_DEALER_FIRST_FIELD);
        whenTestDigitableLine();
        thenFieldValidityShouldBe(true);
    }

    @Test
    public void test_public_agency_dealer_first_field_is_NOT_Valid() {
        givenDigitableLineWith(PUBLIC_AGENCY_DEALER_INVALID_FIRST_FIELD);
        whenTestDigitableLine();
        thenFieldValidityShouldBe(false);
    }

    @Test
    public void test_public_agency_dealer_first_field_isValid_while_user_is_typing_to_second_field() {
        givenDigitableLineWith(PUBLIC_AGENCY_DEALER_FIRST_FIELD + PUBLIC_AGENCY_DEALER_PART_OF_SECOND_FIELD);
        whenTestDigitableLine();
        thenFieldValidityShouldBe(true);
    }

    @Test
    public void test_public_agency_dealer_second_field_isValid() {
        givenDigitableLineWith(PUBLIC_AGENCY_DEALER_FIRST_FIELD + PUBLIC_AGENCY_DEALER_SECOND_FIELD);
        whenTestDigitableLine();
        thenFieldValidityShouldBe(true);
    }

    @Test
    public void test_public_agency_dealer_second_field_is_NOT_Valid() {
        givenDigitableLineWith(PUBLIC_AGENCY_DEALER_FIRST_FIELD + PUBLIC_AGENCY_DEALER_INVALID_SECOND_FIELD);
        whenTestDigitableLine();
        thenFieldValidityShouldBe(false);
    }

    @Test
    public void test_public_agency_dealer_second_field_isValid_while_user_is_typing_to_third_field() {
        givenDigitableLineWith(PUBLIC_AGENCY_DEALER_FIRST_FIELD
                + PUBLIC_AGENCY_DEALER_SECOND_FIELD + PUBLIC_AGENCY_DEALER_PART_OF_THIRD_FIELD);
        whenTestDigitableLine();
        thenFieldValidityShouldBe(true);
    }

    @Test
    public void test_public_agency_dealer_third_field_isValid() {
        givenDigitableLineWith(PUBLIC_AGENCY_DEALER_FIRST_FIELD + PUBLIC_AGENCY_DEALER_SECOND_FIELD
                + PUBLIC_AGENCY_DEALER_THIRD_FIELD);
        whenTestDigitableLine();
        thenFieldValidityShouldBe(true);
    }

    @Test
    public void test_public_agency_dealer_third_field_is_NOT_Valid() {
        givenDigitableLineWith(PUBLIC_AGENCY_DEALER_FIRST_FIELD + PUBLIC_AGENCY_DEALER_SECOND_FIELD
                + PUBLIC_AGENCY_DEALER_INVALID_THIRD_FIELD);
        whenTestDigitableLine();
        thenFieldValidityShouldBe(false);
    }

    @Test
    public void test_public_agency_dealer_third_field_isValid_while_user_is_typing_to_fourth_field() {
        givenDigitableLineWith(PUBLIC_AGENCY_DEALER_FIRST_FIELD
                + PUBLIC_AGENCY_DEALER_SECOND_FIELD + PUBLIC_AGENCY_DEALER_THIRD_FIELD
                + PUBLIC_AGENCY_DEALER_PART_OF_FOURTH_FIELD);
        whenTestDigitableLine();
        thenFieldValidityShouldBe(true);
    }

    @Test
    public void test_public_agency_dealer_fourth_field_isValid() {
        givenDigitableLineWith(PUBLIC_AGENCY_DEALER_FIRST_FIELD + PUBLIC_AGENCY_DEALER_SECOND_FIELD
                + PUBLIC_AGENCY_DEALER_THIRD_FIELD + PUBLIC_AGENCY_DEALER_FOURTH_FIELD);
        whenTestDigitableLine();
        thenFieldValidityShouldBe(true);
    }

    @Test
    public void test_public_agency_dealer_fourth_field_is_NOT_Valid() {
        givenDigitableLineWith(PUBLIC_AGENCY_DEALER_FIRST_FIELD + PUBLIC_AGENCY_DEALER_SECOND_FIELD
                + PUBLIC_AGENCY_DEALER_THIRD_FIELD + PUBLIC_AGENCY_DEALER_INVALID_FOURTH_FIELD);
        whenTestDigitableLine();
        thenFieldValidityShouldBe(false);
    }
}
package br.com.missio.semcrise.utils;

import org.junit.Test;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import br.com.missio.semcrise.exceptions.InvalidMonthNumberException;
import br.com.missio.semcrise.exceptions.MonthNumberOutOfBoundsException;
import br.com.missio.semcrise.model.DateStatus;

import static junit.framework.TestCase.assertEquals;

public class DateHelperTest {

    private static final boolean MATCH = true;
    private static final boolean DONT_MATCH = false;
    private Calendar today;
    private int period;
    private DateStatus dateStatus;
    private boolean response;

    @Test
    public void date_matching_with_today() {
        Calendar today = Calendar.getInstance();
        given(today,30, DateStatus.PAST);

        whenMethodIsCalled();

        dateShould(MATCH);
    }

    @Test
    public void date_matching_in_past_period() {
        Calendar past = Calendar.getInstance();
        past.add(Calendar.DAY_OF_MONTH,-20);

        given(past,30, DateStatus.PAST);

        whenMethodIsCalled();

        dateShould(MATCH);
    }

    @Test
    public void date_not_matching_in_past_before_period() {
        Calendar past = Calendar.getInstance();
        past.add(Calendar.DAY_OF_MONTH,-40);

        given(past,30, DateStatus.PAST);

        whenMethodIsCalled();

        dateShould(DONT_MATCH);
    }

    @Test
    public void date_not_matching_in_past_after_period() {
        Calendar past = Calendar.getInstance();
        past.add(Calendar.DAY_OF_MONTH,+1);

        given(past,30, DateStatus.PAST);

        whenMethodIsCalled();

        dateShould(DONT_MATCH);
    }

    @Test
    public void date_matching_in_future_period() {
        Calendar future = Calendar.getInstance();
        future.add(Calendar.DAY_OF_MONTH, +20);

        given(future,30, DateStatus.FUTURE);

        whenMethodIsCalled();

        dateShould(MATCH);
    }

    @Test
    public void date_not_matching_in_future_after_period() {
        Calendar future = Calendar.getInstance();
        future.add(Calendar.DAY_OF_MONTH, +40);

        given(future,30, DateStatus.FUTURE);

        whenMethodIsCalled();

        dateShould(DONT_MATCH);
    }

    @Test
    public void date_not_matching_in_future_before_period() {
        Calendar future = Calendar.getInstance();
        future.add(Calendar.DAY_OF_MONTH, -1);

        given(future,30, DateStatus.FUTURE);

        whenMethodIsCalled();

        dateShould(DONT_MATCH);
    }

    @Test
    public void date_go_to_next_working_day_if_weekend_sunday_example() {
        Calendar weekendDate = Calendar.getInstance();
        weekendDate.set(Calendar.DAY_OF_MONTH,13);
        weekendDate.set(Calendar.MONTH,Calendar.MARCH);
        weekendDate.set(Calendar.YEAR,2016);

        Calendar firstWorkingDate =  Calendar.getInstance();
        firstWorkingDate.set(Calendar.DAY_OF_MONTH,14);
        firstWorkingDate.set(Calendar.MONTH,Calendar.MARCH);
        firstWorkingDate.set(Calendar.YEAR,2016);

        DateHelper.adjustTimeForWorkingDate(weekendDate, new ArrayList<Calendar>());
        assertEquals(firstWorkingDate.get(Calendar.DAY_OF_MONTH),weekendDate.get(Calendar.DAY_OF_MONTH));
        assertEquals(firstWorkingDate.get(Calendar.MONTH),weekendDate.get(Calendar.MONTH));
        assertEquals(firstWorkingDate.get(Calendar.YEAR),weekendDate.get(Calendar.YEAR));
    }

    @Test
    public void date_go_to_next_working_day_if_weekend_saturday_example() {
        Calendar weekendDate = Calendar.getInstance();
        weekendDate.set(Calendar.DAY_OF_MONTH,12);
        weekendDate.set(Calendar.MONTH,Calendar.MARCH);
        weekendDate.set(Calendar.YEAR,2016);

        Calendar firstWorkingDate =  Calendar.getInstance();
        firstWorkingDate.set(Calendar.DAY_OF_MONTH,14);
        firstWorkingDate.set(Calendar.MONTH,Calendar.MARCH);
        firstWorkingDate.set(Calendar.YEAR,2016);

        DateHelper.adjustTimeForWorkingDate(weekendDate, new ArrayList<Calendar>());
        assertEquals(firstWorkingDate.get(Calendar.DAY_OF_MONTH),weekendDate.get(Calendar.DAY_OF_MONTH));
        assertEquals(firstWorkingDate.get(Calendar.MONTH),weekendDate.get(Calendar.MONTH));
        assertEquals(firstWorkingDate.get(Calendar.YEAR),weekendDate.get(Calendar.YEAR));
    }

    @Test
    public void date_stays_the_same_if_not_weekend_friday_example() {
        Calendar dateForAdjust = Calendar.getInstance();
        dateForAdjust.set(Calendar.DAY_OF_MONTH,11);
        dateForAdjust.set(Calendar.MONTH,Calendar.MARCH);
        dateForAdjust.set(Calendar.YEAR,2016);

        Calendar firstWorkingDate =  Calendar.getInstance();
        firstWorkingDate.set(Calendar.DAY_OF_MONTH,11);
        firstWorkingDate.set(Calendar.MONTH,Calendar.MARCH);
        firstWorkingDate.set(Calendar.YEAR,2016);

        DateHelper.adjustTimeForWorkingDate(dateForAdjust, getHolidaysList());
        assertEquals(firstWorkingDate.get(Calendar.DAY_OF_MONTH),dateForAdjust.get(Calendar.DAY_OF_MONTH));
        assertEquals(firstWorkingDate.get(Calendar.MONTH),dateForAdjust.get(Calendar.MONTH));
        assertEquals(firstWorkingDate.get(Calendar.YEAR),dateForAdjust.get(Calendar.YEAR));
    }

    @Test
    public void date_go_to_next_working_day_if_weekend_end_of_month_example() {
        Calendar weekendDate = Calendar.getInstance();
        weekendDate.set(Calendar.DAY_OF_MONTH,30);
        weekendDate.set(Calendar.MONTH,Calendar.JANUARY);
        weekendDate.set(Calendar.YEAR,2016);

        Calendar firstWorkingDate =  Calendar.getInstance();
        firstWorkingDate.set(Calendar.DAY_OF_MONTH,1);
        firstWorkingDate.set(Calendar.MONTH,Calendar.FEBRUARY);
        firstWorkingDate.set(Calendar.YEAR,2016);

        DateHelper.adjustTimeForWorkingDate(weekendDate, getHolidaysList());
        assertEquals(firstWorkingDate.get(Calendar.DAY_OF_MONTH),weekendDate.get(Calendar.DAY_OF_MONTH));
        assertEquals(firstWorkingDate.get(Calendar.MONTH),weekendDate.get(Calendar.MONTH));
        assertEquals(firstWorkingDate.get(Calendar.YEAR),weekendDate.get(Calendar.YEAR));
    }

    @Test
    public void date_go_to_next_working_day_if_weekend_or_holidays() {
        Calendar weekendDate = Calendar.getInstance();
        weekendDate.set(Calendar.DAY_OF_MONTH,12);
        weekendDate.set(Calendar.MONTH,Calendar.MARCH);
        weekendDate.set(Calendar.YEAR,2016);

        Calendar firstWorkingDate =  Calendar.getInstance();
        firstWorkingDate.set(Calendar.DAY_OF_MONTH,15);
        firstWorkingDate.set(Calendar.MONTH,Calendar.MARCH);
        firstWorkingDate.set(Calendar.YEAR,2016);

        List<Calendar> holidays = getHolidaysList();

        DateHelper.adjustTimeForWorkingDate(weekendDate,holidays);
        assertEquals(firstWorkingDate.get(Calendar.DAY_OF_MONTH),weekendDate.get(Calendar.DAY_OF_MONTH));
        assertEquals(firstWorkingDate.get(Calendar.MONTH),weekendDate.get(Calendar.MONTH));
        assertEquals(firstWorkingDate.get(Calendar.YEAR),weekendDate.get(Calendar.YEAR));
    }

    @Test
    public void date_go_to_next_working_day_if_holiday() {
        Calendar adjustedDate = Calendar.getInstance();
        adjustedDate.set(Calendar.DAY_OF_MONTH,22);
        adjustedDate.set(Calendar.MONTH,Calendar.MARCH);
        adjustedDate.set(Calendar.YEAR,2016);

        Calendar firstWorkingDate =  Calendar.getInstance();
        firstWorkingDate.set(Calendar.DAY_OF_MONTH,23);
        firstWorkingDate.set(Calendar.MONTH,Calendar.MARCH);
        firstWorkingDate.set(Calendar.YEAR,2016);

        List<Calendar> holidays = getHolidaysList();

        DateHelper.adjustTimeForWorkingDate(adjustedDate,holidays);
        assertEquals(firstWorkingDate.get(Calendar.DAY_OF_MONTH),adjustedDate.get(Calendar.DAY_OF_MONTH));
        assertEquals(firstWorkingDate.get(Calendar.MONTH),adjustedDate.get(Calendar.MONTH));
        assertEquals(firstWorkingDate.get(Calendar.YEAR),adjustedDate.get(Calendar.YEAR));
    }

    @Test
    public void test_get_month() throws Exception {
        assertEquals(Calendar.JULY,DateHelper.parseMonth("07"));
    }

    @Test(expected = InvalidMonthNumberException.class)
    public void test_throws_exception_when_get_invalid_month() throws Exception {
        DateHelper.parseMonth("7");
    }

    @Test(expected = MonthNumberOutOfBoundsException.class)
    public void test_throws_exception_when_get_invalid_month_upper_limit() throws Exception {
        DateHelper.parseMonth("13");
    }

    @Test(expected = MonthNumberOutOfBoundsException.class)
    public void test_throws_exception_when_get_invalid_month_lower_limit() throws Exception {
        DateHelper.parseMonth("00");
    }

    private List<Calendar> getHolidaysList() {
        Calendar firstHoliday = Calendar.getInstance();
        firstHoliday.set(Calendar.DAY_OF_MONTH,14);
        firstHoliday.set(Calendar.MONTH,Calendar.MARCH);
        firstHoliday.set(Calendar.YEAR,2016);

        Calendar secondHoliday = Calendar.getInstance();
        secondHoliday.set(Calendar.DAY_OF_MONTH,22);
        secondHoliday.set(Calendar.MONTH,Calendar.MARCH);
        secondHoliday.set(Calendar.YEAR,2016);

        List<Calendar> holidays = new ArrayList<>();
        holidays.add(firstHoliday);
        holidays.add(secondHoliday);
        return holidays;
    }

    private void dateShould(boolean match) {
        assertEquals(match,response);
    }

    private void whenMethodIsCalled() {
        response = DateHelper.dateMatchesWith(today.getTime(),period, dateStatus);
    }

    private void given(Calendar today, int period, DateStatus dateStatus) {
        this.today = today;
        this.period = period;
        this.dateStatus = dateStatus;

    }
}

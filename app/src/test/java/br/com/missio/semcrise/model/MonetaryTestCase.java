package br.com.missio.semcrise.model;

import java.util.Calendar;
import java.util.Date;

import static junit.framework.Assert.assertEquals;

public class MonetaryTestCase {
    protected MonetaryEventsList events;
    protected Double receiptCurrentMonthAmount;

    protected MonetaryEventsList givenEventsFromCurrentMonthWithAmounts(Double... amounts) {
        Calendar calendar = getCalendar();
        Date currentMonth = calendar.getTime();
        return getEventsListWith(amounts, currentMonth,MonetaryEvent.RECEIPT);
    }

    protected MonetaryEventsList andGivenEventsFromPastMonthsWithAmounts(Double... amounts) {
        Calendar calendar = getCalendar();
        calendar.roll(Calendar.MONTH, false);
        Date lastMonth = calendar.getTime();
        return getEventsListWith(amounts, lastMonth, MonetaryEvent.RECEIPT);
    }

    protected MonetaryEventsList andGivenEventsFromTomorrowWithAmounts(Double... amounts) {
        Calendar calendar = getCalendar();
        calendar.add(Calendar.DAY_OF_MONTH, 1);
        Date tomorrow = calendar.getTime();
        return getEventsListWith(amounts, tomorrow, MonetaryEvent.RECEIPT);
    }

    protected Double whenMonthlyUntilNowAmountIsCalled() {
        return events.getMonthlyEventsUntilNow().getTotalAmount();
    }

    protected void receiptMonthTotalAmountShouldBe(Double amount) {
        assertEquals(amount, receiptCurrentMonthAmount);
    }

    protected MonetaryEventsList getEventsListWith(Double[] amounts, Date time, String eventType) {
        MonetaryEventsList events = new MonetaryEventsList();
        int receiptNumber = 1;
        for (Double amount : amounts) {
            MonetaryEvent event = eventType.equals(MonetaryEvent.RECEIPT) ? getDefaultReceipt() : getDefaultExpense();
            String defaultName = event.getName();
            event.setName(defaultName + receiptNumber);
            event.setValue(amount);
            event.setExpectedDate(time);
            event.setEffectiveDate(time);
            events.add(event);
            receiptNumber++;
        }
        return events;
    }

    protected Receipt getDefaultReceipt() {
        Receipt receipt = new Receipt();
        receipt.setName("Salario");
        receipt.setSource("Coisita");
        Calendar calendar = getCalendar();
        receipt.setEffectiveDate(calendar.getTime());
        receipt.setExpectedDate(calendar.getTime());
        return receipt;
    }

    protected Expense getDefaultExpense() {
        Expense expense = new Expense();
        expense.setName("Luz");
        expense.setCreditorId("Light");
        Calendar calendar = getCalendar();
        expense.setEffectiveDate(calendar.getTime());
        expense.setExpectedDate(calendar.getTime());
        return expense;
    }

    protected Calendar getCalendar() {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 1);
        return calendar;
    }
}

package br.com.missio.semcrise.services.barcodereader;

import static org.junit.Assert.assertEquals;

public class DigitableLineTestCase {
    private String digitableLine;
    private boolean isValid;

    protected void givenDigitableLineWith(String digitableLine) {
        this.digitableLine = digitableLine;
    }

    protected void whenTestDigitableLine() {
        this.isValid = new DigitableLineVerifier(digitableLine).isValid();
    }

    protected void thenFieldValidityShouldBe(boolean expectedValidity) {
        assertEquals(expectedValidity,isValid);
    }
}

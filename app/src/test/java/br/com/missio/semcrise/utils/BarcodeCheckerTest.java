package br.com.missio.semcrise.utils;

import org.junit.Test;

import java.util.Arrays;

import static junit.framework.TestCase.*;
import static org.junit.Assert.assertEquals;

public class BarcodeCheckerTest extends BarcodeTestCase{
    private String type;

    @Test
    public void test_barcode_has_only_numbers() throws Exception {
        givenBarcode(WRONG_BARCODE_WITH_RIGHT_SIZE);
        whenGetBarcodeType();
        thenBarcodeTypeShouldBe(BarcodeType.NULL);
    }

    @Test
    public void test_barcode_type_is_bank_slip() throws Exception {
        givenBarcode(BANK_SLIP_BARCODE);
        whenGetBarcodeType();
        thenBarcodeTypeShouldBe(BarcodeType.BANK_SLIP);
    }

    @Test
    public void test_barcode_type_is_dealers_company() throws Exception {
        givenBarcode(DEALER_COMPANY_WITHOUT_CNPJ_BARCODE);
        whenGetBarcodeType();
        thenBarcodeTypeShouldBe(BarcodeType.DEALERS);
    }

    @Test
    public void test_barcode_type_is_dealers_public_agency() throws Exception {
        givenBarcode(DEALER_PUBLIC_AGENCY_WITHOUT_CNPJ_BARCODE);
        whenGetBarcodeType();
        thenBarcodeTypeShouldBe(BarcodeType.DEALERS);
    }

    @Test
    public void test_barcode_type_is_null() throws Exception {
        givenBarcode(FAKE_BARCODE);
        whenGetBarcodeType();
        thenBarcodeTypeShouldBe(BarcodeType.NULL);
    }

    private void whenGetBarcodeType() throws Exception {
        type = new BarcodeChecker(getGivenBarcode()).getBarcodeType();
    }

    private void thenBarcodeTypeShouldBe(String expectedBarcodeType) {
        assertEquals(expectedBarcodeType,type);
    }
}
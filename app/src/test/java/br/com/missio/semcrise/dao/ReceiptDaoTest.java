package br.com.missio.semcrise.dao;

import android.os.Build;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.rule.PowerMockRule;
import org.robolectric.RobolectricGradleTestRunner;
import org.robolectric.annotation.Config;

import br.com.missio.semcrise.BuildConfig;
import br.com.missio.semcrise.MockSupport;
import br.com.missio.semcrise.model.Receipt;
import io.realm.Realm;
import io.realm.RealmQuery;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@RunWith(RobolectricGradleTestRunner.class)
@Config(constants = BuildConfig.class,sdk = Build.VERSION_CODES.LOLLIPOP)
@PrepareForTest({Realm.class, RealmQuery.class})
public class ReceiptDaoTest {

    ReceiptDao receiptManager;
    Realm realmMock;

    @Rule
    public PowerMockRule rule = new PowerMockRule();

    @Before
    public void setUp() throws Exception {
        realmMock = MockSupport.mockRealm();
        receiptManager = new ReceiptDao(realmMock);
    }

    @Test
    public void add_or_update_receipt_event_data(){
        Receipt fakeReceipt = MockSupport.givenAFakeReceipt();

        //when
        receiptManager.add(fakeReceipt);

        //then
        verify(realmMock, times(1)).beginTransaction();
        verify(realmMock, times(1)).copyToRealm(fakeReceipt);
        verify(realmMock, times(1)).commitTransaction();
    }

}
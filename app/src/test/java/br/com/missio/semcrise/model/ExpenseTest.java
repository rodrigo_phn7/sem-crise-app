package br.com.missio.semcrise.model;

import org.junit.Before;
import org.junit.Test;

import java.util.Calendar;
import java.util.Date;

import br.com.missio.semcrise.MockSupport;

import static junit.framework.TestCase.assertEquals;

public class ExpenseTest {

    public static final String LATE = "late";
    public static final String ON_TIME = "onTime";
    private Expense expense;
    private Boolean lateStatus;

    @Before
    public void setUp() {
        lateStatus = null;
    }

    @Test
    public void expense_is_late(){
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DAY_OF_MONTH, -1);
        Date pastExpectedDate = calendar.getTime();
        givenExpenseWith(null, pastExpectedDate);

        whenIsLateMethodIsCalled();

        thenExpenseShouldBe(LATE);
    }

    @Test
    public void expense_with_now_effective_date() {
        Date nowEffectiveDate = new Date();
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DAY_OF_MONTH, -1);
        Date pastExpectedDate = calendar.getTime();

        givenExpenseWith(nowEffectiveDate,pastExpectedDate);

        whenIsLateMethodIsCalled();
        thenExpenseShouldBe(ON_TIME);
    }

    @Test
    public void expense_late_is_not_current_day_of_month_in_first_second() {
        Calendar expectedCalendar = Calendar.getInstance();
        expectedCalendar.set(Calendar.HOUR_OF_DAY,0);
        expectedCalendar.set(Calendar.MINUTE,0);
        expectedCalendar.set(Calendar.SECOND, 1);
        Date todayExpectedDate = expectedCalendar.getTime();

        givenExpenseWith(null,todayExpectedDate);

        whenIsLateMethodIsCalled();

        thenExpenseShouldBe(ON_TIME);
    }

    @Test
    public void expense_late_is_not_current_day_of_month_in_last_second() {
        Calendar expectedCalendar = Calendar.getInstance();
        expectedCalendar.set(Calendar.HOUR_OF_DAY,23);
        expectedCalendar.set(Calendar.MINUTE,59);
        expectedCalendar.set(Calendar.SECOND,59);
        Date todayExpectedDate = expectedCalendar.getTime();

        givenExpenseWith(null,todayExpectedDate);

        whenIsLateMethodIsCalled();

        thenExpenseShouldBe(ON_TIME);
    }

    private void givenExpenseWith(Date effectiveDate, Date pastExpectedDate) {
        expense = MockSupport.givenAFakeExpense();
        expense.setEffectiveDate(effectiveDate);
        expense.setExpectedDate(pastExpectedDate);
    }

    private void whenIsLateMethodIsCalled() {
        lateStatus = expense.isLate();
    }

    private void thenExpenseShouldBe(String timeStatus) {
        assertEquals(get(timeStatus),lateStatus);
    }

    private Boolean get(String timeStatus) {
        return timeStatus.equals(LATE);
    }
}
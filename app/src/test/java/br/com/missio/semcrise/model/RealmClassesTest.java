package br.com.missio.semcrise.model;

import org.junit.Test;

import io.realm.RealmObject;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsInstanceOf.instanceOf;

public class RealmClassesTest {

    @Test
    public void User_class_is_a_object_persistable_in_a_database(){
        assertThat(new User(),is(instanceOf(RealmObject.class)));
    }

    @Test
    public void Receipt_class_is_a_object_persistable_in_a_database(){
        assertThat(new Receipt(),is(instanceOf(RealmObject.class)));
    }

    @Test
    public void Expense_class_is_a_object_persistable_in_a_database(){
        assertThat(new Expense(),is(instanceOf(RealmObject.class)));
    }

}

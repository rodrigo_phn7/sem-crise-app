package br.com.missio.semcrise.utils;

import android.os.Build;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.rule.PowerMockRule;
import org.robolectric.RobolectricGradleTestRunner;
import org.robolectric.annotation.Config;

import java.util.Calendar;

import br.com.missio.semcrise.BuildConfig;
import br.com.missio.semcrise.MockSupport;
import br.com.missio.semcrise.model.Creditor;
import io.realm.Realm;
import io.realm.RealmQuery;

import static org.junit.Assert.*;

@RunWith(RobolectricGradleTestRunner.class)
@Config(constants = BuildConfig.class,sdk = Build.VERSION_CODES.LOLLIPOP)
@PrepareForTest({Realm.class, RealmQuery.class})
public class BankSlipBarcodeTest extends BarcodeTestCase {

    private Creditor creditor;
    private Calendar dueDate;
    private Double billValue;
    private BarcodeType parsedBarcode;

    @Rule
    public PowerMockRule rule = new PowerMockRule();

    @Before
    public void setUp() throws Exception {
        MockSupport.mockBradescoRealm();
    }

    @Test(expected = Exception.class)
    public void test_throw_exception_when_barcode_is_invalid_when_bank_slip_barcode_is_created() throws Exception {
        givenBarcode(WRONG_BARCODE);
        whenBankSlipBarcodeClassIsCreated();
    }

    @Test
    public void test_parse_bank_slip_barcode() throws Exception {
        givenBarcode(BANK_SLIP_BARCODE);
        whenBankSlipBarcodeClassIsCreated();
        thenParseShouldBe(MockSupport.getBankSlipBarcode());
    }

    @Test
    public void test_get_bank() throws Exception {
        givenBarcode(BANK_SLIP_BARCODE);
        whenGetBank();
        thenBankShouldBe(MockSupport.getBradescoBank());
    }

    @Test
    public void test_get_due_date() throws Exception {
        givenBarcode(BANK_SLIP_BARCODE);
        whenGetDueDate();
        thenDueDateShouldBe(MockSupport.getExpectedDueDate(2016,Calendar.DECEMBER,29));
    }

    @Test
    public void test_get_monetary_value() throws Exception {
        givenBarcode(BANK_SLIP_BARCODE);
        whenGetBillValue();
        thenValueShouldBe(126.97);
    }

    private void whenGetBank() throws Exception {
        BankSlipBarcode bankSlipBarcode = new BankSlipBarcode(getGivenBarcode());
        creditor = bankSlipBarcode.getBank();
    }

    private void whenBankSlipBarcodeClassIsCreated() throws Exception {
        parsedBarcode = new BankSlipBarcode(getGivenBarcode());
    }

    private void whenGetDueDate() throws Exception {
        BankSlipBarcode bankSlipBarcode = new BankSlipBarcode(getGivenBarcode());
        dueDate = bankSlipBarcode.getDueDate();
    }

    private void whenGetBillValue() throws Exception {
        BankSlipBarcode bankSlipBarcode = new BankSlipBarcode(getGivenBarcode());
        billValue = bankSlipBarcode.getBillValue();
    }

    private void thenBankShouldBe(Creditor expectedCreditor) {
        assertEquals(expectedCreditor, creditor);
    }

    private void thenDueDateShouldBe(Calendar expectedDueDate) {
        assertEquals(expectedDueDate.getTime(),dueDate.getTime());
    }

    private void thenValueShouldBe(Double expectedBillValue) {
        assertEquals(expectedBillValue,billValue);
    }

    private void thenParseShouldBe(BarcodeType expectedParsedBarcode) {
        assertEquals(expectedParsedBarcode, parsedBarcode);
    }
}
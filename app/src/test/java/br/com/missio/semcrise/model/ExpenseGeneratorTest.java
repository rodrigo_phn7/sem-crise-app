package br.com.missio.semcrise.model;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.rule.PowerMockRule;

import br.com.missio.semcrise.MockSupport;
import br.com.missio.semcrise.exceptions.BarcodeInvalidException;
import br.com.missio.semcrise.utils.BarcodeTestCase;
import io.realm.Realm;
import io.realm.RealmQuery;

@PrepareForTest({Realm.class, RealmQuery.class})
public class ExpenseGeneratorTest extends BarcodeTestCase{

    private Expense expense;

    @Rule
    public PowerMockRule rule = new PowerMockRule();

    @Before
    public void setUp() throws Exception {
        MockSupport.mockBradescoRealm();
    }

    @Test
    public void test_create_expense_through_bank_slip_barcode() throws Exception {
        givenBarcode(BANK_SLIP_BARCODE);
        whenGenerateExpense();
        thenExpenseShouldBe(MockSupport.getBankSlipBradescoExpense());
    }

    @Test
    public void test_create_expense_through_dealers_company_barcode() throws Exception {
        MockSupport.mockNetVirtuaRealm();
        givenBarcode(DEALER_COMPANY_WITHOUT_CNPJ_BARCODE);
        whenGenerateExpense();
        thenExpenseShouldBe(MockSupport.getNetVirtuaExpense());
    }

    @Test
    public void test_create_expense_through_dealers_public_agency_barcode() throws Exception {
        MockSupport.mockSimplesNacionalRealm();
        givenBarcode(DEALER_PUBLIC_AGENCY_WITHOUT_CNPJ_BARCODE);
        whenGenerateExpense();
        thenExpenseShouldBe(MockSupport.getSimplesNacionalExpense());
    }

    @Test(expected = BarcodeInvalidException.class)
    public void test_throws_exception_when_barcode_has_wrong_size() throws Exception {
        givenBarcode(BARCODE_WITH_WRONG_SIZE);
        whenGenerateExpense();
    }

    @Test(expected = BarcodeInvalidException.class)
    public void test_throws_exception_when_barcode_is_invalid() throws Exception {
        givenBarcode(FAKE_BARCODE);
        whenGenerateExpense();
    }

    @Test(expected = BarcodeInvalidException.class)
    public void test_throws_exception_when_barcode_has_right_size_and_wrong_digits() throws Exception {
        givenBarcode(WRONG_BARCODE_WITH_RIGHT_SIZE);
        whenGenerateExpense();
    }

    @Test(expected = BarcodeInvalidException.class)
    public void test_throws_exception_when_barcode_is_totally_wrong() throws Exception {
        givenBarcode(WRONG_BARCODE);
        whenGenerateExpense();
    }

    private void whenGenerateExpense() throws Exception {
        expense = ExpenseGenerator.generateExpenseThrough(getGivenBarcode());
    }

    private void thenExpenseShouldBe(Expense expectedExpense) {
        assertEquals(expectedExpense,expense);
    }
}
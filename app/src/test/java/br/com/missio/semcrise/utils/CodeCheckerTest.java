package br.com.missio.semcrise.utils;

import org.junit.Test;

import static org.junit.Assert.*;

public class CodeCheckerTest {

    public static final int MOD_10_EXAMPLE_POSITION = 9;
    public static final int MOD_11_EXAMPLE_POSITION = 11;
    private String digits;
    private int codeChecker;

    @Test
    public void test_calculateMod11BankSlipCodeChecker() throws Exception {
        givenDigits(BarcodeTestCase.BANK_SLIP_BARCODE);
        whenCalculateMod11BankSlipCodeChecker();
        thenCodeCheckerShouldBe(2);
    }

    @Test
    public void test_calculateMod11CodeChecker() throws Exception {
        givenDigits("012300678960");
        whenCalculateMod11CodeCheckerBy(MOD_11_EXAMPLE_POSITION);
        thenCodeCheckerShouldBe(0);
    }

    @Test
    public void test_calculateMod11CodeChecker_when_rest_is_lower_than_eleven() throws Exception {
        givenDigits("858000000003");
        whenCalculateMod11CodeCheckerBy(MOD_11_EXAMPLE_POSITION);
        thenCodeCheckerShouldBe(3);
    }

    @Test
    public void test_calculateMod10CodeChecker() throws Exception {
        givenDigits("0019050095");
        whenCalculateMod10CodeCheckerBy(MOD_10_EXAMPLE_POSITION);
        thenCodeCheckerShouldBe(5);
    }

    private void givenDigits(String digits) {
        this.digits =  digits;
    }

    private void whenCalculateMod11BankSlipCodeChecker() {
        codeChecker = new CodeChecker(digits).calculateMod11BankSlipCodeChecker(Constants.BANK_SLIP_CODE_CHECKER_POSITION);
    }

    private void whenCalculateMod10CodeCheckerBy(int position) {
        codeChecker = new CodeChecker(digits).calculateMod10CodeChecker(position);
    }

    private void whenCalculateMod11CodeCheckerBy(int position) {
        codeChecker = new CodeChecker(digits).calculateMod11CodeChecker(position);
    }

    private void thenCodeCheckerShouldBe(int expectedCodeChecker) {
        assertEquals(expectedCodeChecker,codeChecker);
    }
}
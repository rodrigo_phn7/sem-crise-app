package br.com.missio.semcrise.model;

import org.junit.Before;
import org.junit.Test;

public class MonetaryEventsListTest extends MonetaryTestCase{

    private MonetaryEventsList receipts;

    @Before
    public void setUp(){
        receiptCurrentMonthAmount = (double) 0;
        receipts = new MonetaryEventsList();
        receipts = givenEventsFromCurrentMonthWithAmounts((double) 19, (double) 70);
        receipts.addAll(andGivenEventsFromPastMonthsWithAmounts((double) 50, (double) 90));
        receipts.addAll(andGivenEventsFromTomorrowWithAmounts((double) 30, (double) 13));
    }

    @Test
    public void get_current_month_until_now_total_amount() {
        events = receipts;
        receiptCurrentMonthAmount = whenMonthlyUntilNowAmountIsCalled();
        receiptMonthTotalAmountShouldBe((double) (19 + 70));
    }

}
package br.com.missio.semcrise;

import java.util.Calendar;
import java.util.Date;

import br.com.missio.semcrise.model.Creditor;
import br.com.missio.semcrise.model.CreditorType;
import br.com.missio.semcrise.model.Expense;
import br.com.missio.semcrise.model.Receipt;
import br.com.missio.semcrise.model.User;
import br.com.missio.semcrise.utils.BankSlipBarcode;
import br.com.missio.semcrise.utils.BarcodeType;
import br.com.missio.semcrise.utils.DealersBarcode;
import io.realm.Realm;
import io.realm.RealmObject;
import io.realm.RealmQuery;
import io.realm.RealmResults;

import static org.mockito.Matchers.anyString;
import static org.powermock.api.mockito.PowerMockito.mock;
import static org.powermock.api.mockito.PowerMockito.mockStatic;
import static org.powermock.api.mockito.PowerMockito.when;

public class MockSupport {

    public static final String BRADESCO_CODE = "237";
    public static final String NET_VIRTUA_CODE = "0296";

    public static Realm mockRealm() {
        mockStatic(Realm.class);

        Realm mockRealm = mock(Realm.class);
        when(Realm.getDefaultInstance()).thenReturn(mockRealm);

        RealmQuery<User> mockRealmUserQuery = mockRealmQuery();
        User fakeUser = new User();
        fakeUser.setName("Foo People");
        fakeUser.setEmail("someone@fake.com");
        fakeUser.setPhone("5521977775555");
        fakeUser.setPass("123456");
        when(mockRealm.createObject(User.class)).thenReturn(fakeUser);
        when(mockRealm.where(User.class)).thenReturn(mockRealmUserQuery);
        when(mockRealmUserQuery.equalTo("email", fakeUser.getEmail())).thenReturn(mockRealmUserQuery);
        when(mockRealmUserQuery.findFirst()).thenReturn(fakeUser);

        return mockRealm;
    }

    public static Realm mockBradescoRealm() {
        mockStatic(Realm.class);

        Realm mockRealm = mock(Realm.class);
        when(Realm.getDefaultInstance()).thenReturn(mockRealm);


        RealmQuery<Creditor> mockRealmBankQuery = mockRealmQuery();
        when(mockRealm.where(Creditor.class)).thenReturn(mockRealmBankQuery);

        RealmQuery<Creditor> mockRealmCreditorQuery = mockRealmQuery();
        when(mockRealmBankQuery.equalTo(anyString(),anyString())).thenReturn(mockRealmCreditorQuery);
        when(mockRealmCreditorQuery.findFirst()).thenReturn(getBradescoBank());

        return mockRealm;
    }

    public static Realm mockNetVirtuaRealm() {
        mockStatic(Realm.class);

        Realm mockRealm = mock(Realm.class);
        when(Realm.getDefaultInstance()).thenReturn(mockRealm);


        RealmQuery<Creditor> mockRealmBankQuery = mockRealmQuery();
        when(mockRealm.where(Creditor.class)).thenReturn(mockRealmBankQuery);

        RealmQuery<Creditor> mockRealmCreditorQuery = mockRealmQuery();
        when(mockRealmBankQuery.equalTo(anyString(),anyString())).thenReturn(mockRealmCreditorQuery);
        when(mockRealmCreditorQuery.findFirst()).thenReturn(getNetVirtuaCompany());

        return mockRealm;
    }

    public static Realm mockSimplesNacionalRealm() {
        mockStatic(Realm.class);

        Realm mockRealm = mock(Realm.class);
        when(Realm.getDefaultInstance()).thenReturn(mockRealm);


        RealmQuery<Creditor> mockRealmQuery = mockRealmQuery();
        when(mockRealm.where(Creditor.class)).thenReturn(mockRealmQuery);

        RealmQuery<Creditor> mockRealmCreditorQuery = mockRealmQuery();
        when(mockRealmQuery.equalTo(anyString(),anyString())).thenReturn(mockRealmCreditorQuery);
        when(mockRealmCreditorQuery.findFirst()).thenReturn(getSimplesNacionalAgency());

        return mockRealm;
    }

    public static Receipt givenAFakeReceipt() {
        Receipt receipt = new Receipt();
        receipt.setName("Job job");
        receipt.setSource("Coisita");
        receipt.setValue(new Double(900));
        receipt.setEffectiveDate(new Date());
        receipt.setExpectedDate(new Date());
        receipt.setPrefixedValue(false);
        receipt.setMonthlyRecurrent(false);
        return receipt;
    }

    public static Expense givenAFakeExpense() {
        Expense expense = new Expense();
        expense.setName("Celular");
        expense.setExpenseType("Eletronicos");
        expense.setValue(new Double(900));
        expense.setEffectiveDate(new Date());
        expense.setMonthlyRecurrent(false);
        expense.setPrefixedValue(false);
        return expense;
    }

    public static Creditor getBradescoBank() {
        Creditor bradesco = new Creditor();
        bradesco.setCode("237");
        bradesco.setName("Bradesco S/A");
        bradesco.setKnownName("Bradesco");
        bradesco.saveCreditorType(CreditorType.BANK);
        return bradesco;
    }

    @SuppressWarnings("unchecked")
    private static <T extends RealmObject> RealmQuery<T> mockRealmQuery() {
        return mock(RealmQuery.class);
    }

    @SuppressWarnings("unchecked")
    private static <T extends RealmObject> RealmResults<T> mockRealmResults() {
        return mock(RealmResults.class);
    }

    public static Calendar getExpectedDueDate(int year, int month, int dayOfMonth) {
        Calendar dueDate = Calendar.getInstance();
        dueDate.set(Calendar.HOUR_OF_DAY,0);
        dueDate.set(Calendar.MINUTE,0);
        dueDate.set(Calendar.SECOND, 1);
        dueDate.set(Calendar.MILLISECOND,0);
        dueDate.set(Calendar.YEAR,year);
        dueDate.set(Calendar.MONTH,month);
        dueDate.set(Calendar.DAY_OF_MONTH,dayOfMonth);
        return dueDate;
    }

    public static Calendar mockGetCalendar2020() {
        Calendar mockCalendar = mock(Calendar.class);
        when(mockCalendar.get(Calendar.YEAR)).thenReturn(2020);
        when(mockCalendar.get(Calendar.MONTH)).thenReturn(Calendar.JANUARY);
        when(mockCalendar.get(Calendar.DAY_OF_MONTH)).thenReturn(2);
        return mockCalendar;
    }

    public static Expense getBankSlipBradescoExpense() {
        Expense bradescoExpense = new Expense();
        bradescoExpense.setCreditorId(getBradescoBank().getId());
        bradescoExpense.setValue(126.97);
        bradescoExpense.setExpectedDate(getExpectedDueDate(2016,Calendar.DECEMBER,29).getTime());
        return bradescoExpense;
    }

    public static Expense getNetVirtuaExpense() {
        Expense netVirtua = new Expense();
        netVirtua.setCreditorId(getNetVirtuaCompany().getId());
        netVirtua.setValue(83.78);
        netVirtua.setExpectedDate(getExpectedDueDate(2014,Calendar.JULY,15).getTime());
        netVirtua.setExpenseType("Tarifas/Contas#7");
        return netVirtua;
    }

    public static BarcodeType getBankSlipBarcode() {
        BankSlipBarcode bankSlipBarcode = new BankSlipBarcode();
        bankSlipBarcode.setBankCode("237");
        bankSlipBarcode.setCodeChecker("2");
        bankSlipBarcode.setDueDateFactor("7023");
        bankSlipBarcode.setBillValue("0000012697");
        bankSlipBarcode.setFreeField("4600190003683213499999970");
        return bankSlipBarcode;
    }

    public static BarcodeType getDealerCompanyWithoutCnpjBarcode() {
        DealersBarcode dealersBarcode = new DealersBarcode();
        dealersBarcode.setProductIdentification("8");
        dealersBarcode.setSegmentIdentification("4");
        dealersBarcode.setIdentificationOfRealValueOrReference("6");
        dealersBarcode.setCheckerDigit("2");
        dealersBarcode.setBillValue("00000008378");
        dealersBarcode.setCompanyOrAgencyIdentification("0296");
        dealersBarcode.setFreeField("2014071523700000046530788");
        dealersBarcode.setDueDate("20140715");
        return dealersBarcode;
    }

    public static BarcodeType getDealerPublicAgencyWithoutCnpjBarcode() {
        DealersBarcode dealersBarcode = new DealersBarcode();
        dealersBarcode.setProductIdentification("8");
        dealersBarcode.setSegmentIdentification("5");
        dealersBarcode.setIdentificationOfRealValueOrReference("8");
        dealersBarcode.setCheckerDigit("0");
        dealersBarcode.setBillValue("00000005798");
        dealersBarcode.setCompanyOrAgencyIdentification("0328");
        dealersBarcode.setFreeField("1610207081609742834266395");
        dealersBarcode.setDueDate("16102070");
        return dealersBarcode;
    }

    public static Creditor getNetVirtuaCompany() {
        Creditor netVirtua = new Creditor();
        netVirtua.setCode("0296");
        netVirtua.setName("NET SERVICOS DE COMUNICACAO S/A");
        netVirtua.setKnownName("NET Virtua");
        netVirtua.saveCreditorType(CreditorType.COMPANY);
        return netVirtua;
    }

    public static Creditor getSimplesNacionalAgency() {
        Creditor simplesNacional = new Creditor();
        simplesNacional.setCode("0328");
        simplesNacional.setName("MINISTÉRIO DA FAZENDA - CGSN - Simples Nacional");
        simplesNacional.setKnownName("Simples Nacional - CGSN");
        simplesNacional.saveCreditorType(CreditorType.PUBLIC_AGENCY);
        return simplesNacional;
    }

    public static Expense getSimplesNacionalExpense() {
        Expense simplesNacionalExpense = new Expense();
        simplesNacionalExpense.setCreditorId(getSimplesNacionalAgency().getId());
        simplesNacionalExpense.setValue(57.98);
        simplesNacionalExpense.setExpectedDate(null);
        simplesNacionalExpense.setExpenseType("Tarifas/Contas#7");
        return simplesNacionalExpense;
    }
}

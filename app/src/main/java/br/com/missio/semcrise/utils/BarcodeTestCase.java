package br.com.missio.semcrise.utils;

public class BarcodeTestCase {
    public static final String DEALER_PUBLIC_AGENCY_WITHOUT_CNPJ_BARCODE = "85800000000579803281610207081609742834266395";
    public static final String DEALER_COMPANY_WITHOUT_CNPJ_BARCODE = "84620000000837802962014071523700000046530788";
    public static final String WRONG_BARCODE = "1234567890098765ada.-4321";
    public static final String BANK_SLIP_BARCODE = "23792702300000126974600190003683213499999970";
    public static final String FAKE_BARCODE = "12345678900958765432123456777890987766543321";
    public static final String WRONG_BARCODE_WITH_RIGHT_SIZE = "asdasdasd12123123123123asd11231123123123sd12";
    public static final String BARCODE_WITH_WRONG_SIZE = "79270230000012697460019000368321349999";
    private String givenBarcode;

    protected void givenBarcode(String barcode) {
        this.givenBarcode = barcode;
    }

    public String getGivenBarcode() {
        return givenBarcode;
    }

}
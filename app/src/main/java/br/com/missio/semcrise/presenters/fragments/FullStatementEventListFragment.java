package br.com.missio.semcrise.presenters.fragments;

import android.content.Context;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import br.com.missio.semcrise.model.DateStatus;
import br.com.missio.semcrise.model.MonetaryEvent;
import br.com.missio.semcrise.model.MonetaryEventsList;
import br.com.missio.semcrise.services.ExpensesService;
import br.com.missio.semcrise.services.ReceiptsService;
import br.com.missio.semcrise.presenters.viewAdapters.MonetaryEventsArrayAdapter;

public class FullStatementEventListFragment extends SemCriseListFragment {

    private static final int MONTHLY_PERIOD = 30;
    private int period = MONTHLY_PERIOD;
    private DateStatus dateStatus = DateStatus.PAST;
    private String eventType;

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        eventType = getSemCriseActivity().getEventType();
    }

    @Override
    public void onResume() {
        super.onResume();
        fillList(dateStatus, period);
    }

    public void fillList(DateStatus dateStatus, int period) {
        this.period = period;
        this.dateStatus = dateStatus;
        Context context = getSemCriseActivity().getApplicationContext();
        if(!MonetaryEvent.NONE.equals(eventType)) {
            MonetaryEventsList events;
            if(MonetaryEvent.RECEIPT.equals(eventType)) {
                ReceiptsService receiptsService = new ReceiptsService(context);
                setReceiptsService(receiptsService);
                events = receiptsService.getAllEventsIn(period,dateStatus);
            } else {
                ExpensesService expensesService = new ExpensesService(context);
                setExpensesService(expensesService);
                events = expensesService.getAllEventsIn(period,dateStatus);
            }
            setEvents(events);
            ArrayAdapter<MonetaryEvent> adapter = new MonetaryEventsArrayAdapter(getSemCriseActivity(),events);
            setListAdapter(adapter);
        }
    }

    public int getPeriod() {
        return period;
    }

    public DateStatus getDateStatus() {
        return dateStatus;
    }
}

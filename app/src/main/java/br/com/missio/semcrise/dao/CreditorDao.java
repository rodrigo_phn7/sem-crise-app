package br.com.missio.semcrise.dao;

import br.com.missio.semcrise.model.Creditor;
import br.com.missio.semcrise.model.CreditorType;
import io.realm.Realm;

public class CreditorDao {
    private Realm realmDatabase;

    public CreditorDao(Realm realmDatabase) {
        this.realmDatabase = realmDatabase;
    }

    public Creditor getCreditorByCode(String code) {
        return realmDatabase.where(Creditor.class).equalTo("code",code).findFirst();
    }

    public boolean thereIsNotCreditorRegisteredFor(CreditorType type) {
        return realmDatabase.where(Creditor.class).equalTo("creditorType", type.toString()).findFirst() == null;
    }

    public Creditor getCreditorById(String id) {
        Creditor creditorByCode = getCreditorByCode(id);
        if(creditorByCode == null) {
            return realmDatabase.where(Creditor.class).equalTo("id",id).findFirst();
        }
        return creditorByCode;
    }
}
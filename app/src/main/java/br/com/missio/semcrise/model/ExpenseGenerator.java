package br.com.missio.semcrise.model;

import android.support.annotation.Nullable;

import java.util.Calendar;
import java.util.Date;

import br.com.missio.semcrise.exceptions.BarcodeInvalidException;
import br.com.missio.semcrise.exceptions.BarcodeIsOutOfTypeException;
import br.com.missio.semcrise.utils.BankSlipBarcode;
import br.com.missio.semcrise.utils.BarcodeChecker;
import br.com.missio.semcrise.utils.BarcodeType;
import br.com.missio.semcrise.utils.BarcodeValidator;
import br.com.missio.semcrise.utils.DealersBarcode;

public class ExpenseGenerator {
    public static Expense generateExpenseThrough(String barcode) throws Exception {
        BarcodeChecker barcodeChecker = new BarcodeChecker(barcode);
        if(BarcodeValidator.validate(barcode)) {
            if(BarcodeType.BANK_SLIP.equals(barcodeChecker.getBarcodeType())) {
                return generateBankSlipExpense(barcode);
            } else if(BarcodeType.DEALERS.equals(barcodeChecker.getBarcodeType())) {
                return generateDealerExpense(barcode);
            } else {
                throw new BarcodeIsOutOfTypeException("Barcode " + barcode + " not recognized. It's out of barcode type.");
            }
        } else {
            throw new BarcodeInvalidException("Invalid Barcode " + barcode);
        }
    }

    private static Expense generateDealerExpense(String barcode) {
        DealersBarcode dealersBarcode = new DealersBarcode(barcode);
        Expense generatedExpense = new Expense();
        Creditor creditor = dealersBarcode.getCompanyOrAgency();
        if(creditor != null) {
            generatedExpense.setCreditorId(creditor.getId());
        }
        generatedExpense.setValue(dealersBarcode.getBillValue());
        Date expectedDate = getExpectedDate(dealersBarcode.getDueDate());
        generatedExpense.setExpectedDate(expectedDate);
        generatedExpense.setExpenseType("Tarifas/Contas#7");
        return generatedExpense;
    }

    private static Expense generateBankSlipExpense(String barcode) {
        BankSlipBarcode bankSlipBarcode = new BankSlipBarcode(barcode);
        Expense generatedExpense = new Expense();
        Creditor creditor = bankSlipBarcode.getBank();
        if(creditor != null) {
            generatedExpense.setCreditorId(creditor.getId());
        }
        generatedExpense.setValue(bankSlipBarcode.getBillValue());
        Date expectedDate = getExpectedDate(bankSlipBarcode.getDueDate());
        generatedExpense.setExpectedDate(expectedDate);
        return generatedExpense;
    }

    @Nullable
    private static Date getExpectedDate(Calendar dueDate) {
        Date expectedDate = null;
        if(dueDate != null) {
            expectedDate = dueDate.getTime();
        }
        return expectedDate;
    }
}
package br.com.missio.semcrise.presenters.fragments;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.andreabaccega.widget.FormEditText;

import java.util.Calendar;

import br.com.missio.semcrise.R;
import br.com.missio.semcrise.utils.ValidationHelper;

public class ConfirmVerificationCodeFragment extends Fragment {

    private FormEditText verificationCodeEditText;
    private OnVerificationCodeValidListener verificationCodeValidListener;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_change_pass_second_step, container, false);
        verificationCodeEditText = (FormEditText) view.findViewById(R.id.verification_code_edit_text);
        return view;
    }

    public void validateCode(String verificationCode, Calendar verificationCodeExpiration) {
        FormEditText[] allFields = {verificationCodeEditText};
        boolean formIsValid = ValidationHelper.validateEachField(allFields);
        if(formIsValid) {
            String verificationCodeByView = verificationCodeEditText.getText().toString();
            Calendar now = Calendar.getInstance();
            if(verificationCodeByView.equals(verificationCode) && now.before(verificationCodeExpiration)) {
               verificationCodeValidListener.onVerificationCodeValid();
            } else {
                String verificationCodeError = getActivity().getString(R.string.verification_code_error);
                verificationCodeEditText.setError(verificationCodeError);
            }
        }
    }

    public void setVerificationCodeValidListener(OnVerificationCodeValidListener verificationCodeValidListener) {
        this.verificationCodeValidListener = verificationCodeValidListener;
    }

    public interface OnVerificationCodeValidListener {
        void onVerificationCodeValid();
    }
}

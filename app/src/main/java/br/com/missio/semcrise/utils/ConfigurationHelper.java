package br.com.missio.semcrise.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import br.com.missio.semcrise.R;
import br.com.missio.semcrise.services.AlarmReceiver;

public class ConfigurationHelper {

    public static void setUpAlarm(Context context, AlarmReceiver alarm) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        boolean alarmEnabled = sharedPreferences.getBoolean(context.getString(R.string.key_pref_alarm),true);
        if(alarmEnabled) {
            alarm.setAlarm(context);
        } else {
            alarm.cancelAlarm(context);
        }
    }
}

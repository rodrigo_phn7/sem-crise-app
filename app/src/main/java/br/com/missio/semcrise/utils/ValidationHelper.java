package br.com.missio.semcrise.utils;

import com.andreabaccega.widget.FormAutoCompleteTextView;
import com.andreabaccega.widget.FormEditText;

public class ValidationHelper {

    public static boolean validateEachField(FormEditText[] allFields) {
        boolean allValid = true;
        for (FormEditText field : allFields) {
            allValid = field.testValidity() && allValid;
        }
        return allValid;
    }

    public static boolean validateEachField(FormAutoCompleteTextView[] allFields) {
        boolean allValid = true;
        for (FormAutoCompleteTextView field : allFields) {
            allValid = field.testValidity() && allValid;
        }
        return allValid;
    }
}

package br.com.missio.semcrise.model;

public enum CreditorType {
    BANK, COMPANY, PUBLIC_AGENCY, PERSON
}

package br.com.missio.semcrise.presenters.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.andreabaccega.widget.FormAutoCompleteTextView;
import com.andreabaccega.widget.FormEditText;

import java.util.Date;
import java.util.HashMap;

import br.com.missio.semcrise.R;
import br.com.missio.semcrise.exceptions.NullCreditorException;
import br.com.missio.semcrise.model.Expense;
import br.com.missio.semcrise.model.MonetaryEvent;
import br.com.missio.semcrise.model.Receipt;
import br.com.missio.semcrise.model.User;
import br.com.missio.semcrise.presenters.ViewFinderAdapter;
import br.com.missio.semcrise.presenters.ViewManager;
import br.com.missio.semcrise.presenters.activities.SemCriseActivity;
import br.com.missio.semcrise.services.ExpensesService;
import br.com.missio.semcrise.services.ReceiptsService;
import br.com.missio.semcrise.utils.SessionManager;
import static br.com.missio.semcrise.utils.ValidationHelper.validateEachField;

public class AddOrUpdateMonetaryEventFragment extends Fragment implements OnClickListener, CompoundButton.OnCheckedChangeListener {

    private Spinner expenseTypeSpinner;
    private Spinner paymentMethodSpinner;
    private Button saveButton;
    private FormEditText monetaryEventNameEditText;
    private FormEditText moneyValueEditText;
    private CheckBox isPrefixedValueCheckbox;
    private CheckBox isMonthlyRecurrentCheckbox;

    private ExpensesService expensesService;
    private ReceiptsService receiptsService;
    private SemCriseActivity eventActivity;
    private String name;
    private Double value;
    private boolean isPrefixedValue;
    private Date expectedDate;
    private String expenseType;
    private String source;
    private String paymentMethod;
    private boolean isMonthlyRecurrent;
    private boolean isReceipt = false;
    private CheckBox effectOnThatDateCheckbox;
    private Date effectiveDate;
    private Expense expense;
    private Receipt receipt;
    private boolean isEditMode = false;
    private TextView dateLabelTextView;
    private ViewManager viewManager;
    private View view;
    private String creditorId;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_add_or_update_monetary_event, container, false);
        eventActivity = (SemCriseActivity) getActivity();
        setUpMonetaryEventService();
        setUpInputs();
        return view;
    }

    private void setUpMonetaryEventService() {
        Intent intent = eventActivity.getIntent();
        String actionOrEventId = intent.getAction();
        String eventType = intent.getStringExtra(SemCriseActivity.EVENT_TYPE_TAG);
        isReceipt = MonetaryEvent.RECEIPT.equals(eventType);
        isEditMode = !SemCriseActivity.ADD_EVENT.equals(actionOrEventId);
        if(isReceipt) {
            receiptsService = new ReceiptsService(eventActivity);
            if(isEditMode) {
                receipt = receiptsService.findById(actionOrEventId);
                effectiveDate = receipt.getEffectiveDate();
            }
        } else {
            expensesService = new ExpensesService(eventActivity);
            if(isEditMode) {
                expense = expensesService.findById(actionOrEventId);
                effectiveDate = expense.getEffectiveDate();
            }
        }
        MonetaryEvent event = isReceipt ? receipt : expense;
        viewManager = new ViewManager(new ViewFinderAdapter(view),eventActivity,event);
    }

    private void setUpInputs() {
        setUpMonetaryEventNameEditText();

        setUpMoneyValueEditText();

        setUpIsPrefixedValueCheckbox();

        setUpEffectOnThatDateCheckboxAndEditText();

        setUpExpectedDateEditText();

        setUpExpenseTypeSpinner();

        setUpSourceOrCreditorEditText();

        setUpPaymentMethodSpinner();

        setUpIsMonthlyRecurrentCheckbox();

        saveButton = (Button) view.findViewById(R.id.save_button);
        saveButton.setOnClickListener(this);
    }

    private void setUpIsMonthlyRecurrentCheckbox() {
        isMonthlyRecurrentCheckbox = (CheckBox) view.findViewById(R.id.is_monthly_recurrent_checkbox);
        if(isEditMode) {
            if(isReceipt) {
                isMonthlyRecurrentCheckbox.setChecked(receipt.isMonthlyRecurrent());
            } else {
                isMonthlyRecurrentCheckbox.setChecked(expense.isMonthlyRecurrent());
            }
        }
    }

    private void setUpEffectOnThatDateCheckboxAndEditText() {
        TextView effectOnThatDateTextView = (TextView) view.findViewById(R.id.effect_on_that_date_text_view);
        int thisEventEffective = isReceipt ? R.string.receipt_received : R.string.expense_paid;
        effectOnThatDateTextView.setText(thisEventEffective);

        effectOnThatDateCheckbox = (CheckBox) view.findViewById(R.id.effect_on_that_date);
        if(isEditMode) {
            if(isEffective()) {
                LinearLayout effectEventLinearLayout = (LinearLayout) view.findViewById(R.id.effect_event_linear_layout);
                effectEventLinearLayout.setVisibility(View.GONE);
            } else {
                thisEventEffective = isReceipt ? R.string.confirm_receipt : R.string.make_payment;
                effectOnThatDateTextView.setText(thisEventEffective);
            }
        }
        effectOnThatDateCheckbox.setOnCheckedChangeListener(this);
    }

    private boolean isEffective() {
        if(isReceipt) {
            return receipt.isEffective();
        } else {
            return expense.isEffective();
        }
    }

    private void setUpIsPrefixedValueCheckbox() {
        isPrefixedValueCheckbox = (CheckBox) view.findViewById(R.id.is_prefixed_value_checkbox);
        if(isEditMode) {
            if(isReceipt) {
                isPrefixedValueCheckbox.setChecked(receipt.isPrefixedValue());
            } else {
                isPrefixedValueCheckbox.setChecked(expense.isPrefixedValue());
            }
        }
    }

    private void setUpMoneyValueEditText() {
        moneyValueEditText = (FormEditText) view.findViewById(R.id.money_value_edit_text);
        String moneyValueString;
        if(isEditMode) {
            if(isReceipt) {
                moneyValueString = String.valueOf(receipt.getValue());
            } else {
                moneyValueString = String.valueOf(expense.getValue());
            }
            moneyValueEditText.setText(moneyValueString);
        }
    }

    private void setUpPaymentMethodSpinner() {
        if(isReceipt) {
            LinearLayout paymentMethodLinearLayout = (LinearLayout) view.findViewById(R.id.payment_method_linear_layout);
            paymentMethodLinearLayout.setVisibility(View.GONE);
        } else {
            paymentMethodSpinner = viewManager.createPaymentMethodSpinner(isEditMode);
        }
    }

    private void setUpSourceOrCreditorEditText() {
        viewManager.setUpCreditorOrSourceTextViewOrEditText(isEditMode);
    }

    private void setUpExpenseTypeSpinner() {
        if(isReceipt) {
            LinearLayout expenseTypeLinearLayout = (LinearLayout) view.findViewById(R.id.expense_type_linear_layout);
            expenseTypeLinearLayout.setVisibility(View.GONE);
        } else {
            expenseTypeSpinner = viewManager.createExpenseTypeSpinner(isEditMode);
        }
    }

    private void setUpExpectedDateEditText() {
        dateLabelTextView = (TextView) view.findViewById(R.id.date_label_text_view);
        if(isEditMode) {
            int dateLabel = isReceipt ? R.string.receipt_date_label : R.string.expense_date_label;
            dateLabelTextView.setText(dateLabel);
        }
        viewManager.createExpectedDateEditText();
    }

    private void setUpMonetaryEventNameEditText() {
        monetaryEventNameEditText = (FormEditText) view.findViewById(R.id.monetary_event_name_edit_text);
        if(isReceipt) {
            monetaryEventNameEditText.setHint(R.string.receipt);
        } else {
            monetaryEventNameEditText.setHint(R.string.expense);
        }
        if(isEditMode) {
            if (isReceipt) {
                monetaryEventNameEditText.setText(receipt.getName());
            } else {
                monetaryEventNameEditText.setText(expense.getName());
            }
        }
    }

    @Override
    public void onClick(View v) {
       if(v == saveButton) {
            boolean formIsValid = validateForm();
            if(formIsValid) {
                try {
                    save();
                    Toast.makeText(eventActivity, R.string.add_success, Toast.LENGTH_SHORT).show();
                } catch(Exception e) {
                    //TODO: make report of the error.
                    Toast.makeText(eventActivity, R.string.cant_add_item_error, Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }
                eventActivity.finish();
            }
        }
    }

    private boolean validateForm() {
        prepareMoneyValueEditTextForValidation(moneyValueEditText);
        FormEditText[] allFields = {monetaryEventNameEditText,moneyValueEditText};
        FormAutoCompleteTextView[] creditorField = {viewManager.getCreditorOrSourceEditText()};
        return validateEachField(allFields) && validateEachField(creditorField);
    }

    private void prepareMoneyValueEditTextForValidation(FormEditText valueEditText) {
        String moneyValueString = valueEditText.getText().toString();
        String[] results = moneyValueString.split("[.,]");
        moneyValueString += (results.length > 1 && results[1].length() < 2) ? "0" : "";
        valueEditText.setText(moneyValueString);
    }

    private void save() throws NullCreditorException {
        getAllInputData();
        SessionManager session = new SessionManager(eventActivity.getApplicationContext());
        HashMap<String,String> sessionUserData = session.getUserData();
        String userEmail = sessionUserData.get(User.EMAIL_TAG);
        if(isReceipt) {
            if(isEditMode) {
                receiptsService.update(receipt, name, source, value, isPrefixedValue, expectedDate, isMonthlyRecurrent, effectiveDate);
            } else {
                receiptsService.addReceiptWith(name, source,value,isPrefixedValue,expectedDate,isMonthlyRecurrent,userEmail,effectiveDate);
            }
        } else {
            if(isEditMode) {
                expensesService.update(expense, name, creditorId, value, isPrefixedValue, expectedDate, isMonthlyRecurrent, paymentMethod, expenseType, effectiveDate);
            } else {
                expense = new Expense();
                expense.setName(name);
                expense.setExpectedDate(expectedDate);
                expense.setCreditorId(creditorId);
                expense.setValue(value);
                expense.setPrefixedValue(isPrefixedValue);
                expense.setMonthlyRecurrent(isMonthlyRecurrent);
                expense.setExpenseType(expenseType);
                expense.setPaymentMethod(paymentMethod);
                expense.setEffectiveDate(effectiveDate);
                expense.setUserEmail(userEmail);
                expensesService.add(expense);
            }
        }
    }

    private void getAllInputData() throws NullCreditorException {
        name = monetaryEventNameEditText.getText().toString();
        String moneyValueString = moneyValueEditText.getText().toString().replace(',','.');
        value = Double.valueOf(moneyValueString);
        isPrefixedValue = isPrefixedValueCheckbox.isChecked();
        expectedDate = viewManager.getEventDate();
        if(isReceipt) {
            source = viewManager.getTypedCreditorOrSourceName();
        } else {
            creditorId = viewManager.getCreditorId();
            String expenseTypeValue = expenseTypeSpinner.getSelectedItem().toString();
            Integer expenseTypeIndex = expenseTypeSpinner.getSelectedItemPosition();
            expenseType = expenseTypeValue + SemCriseActivity.HASHTAG_SEPARATOR + expenseTypeIndex;
            String paymentMethodValue = paymentMethodSpinner.getSelectedItem().toString();
            Integer paymentMethodIndex = paymentMethodSpinner.getSelectedItemPosition();
            paymentMethod = paymentMethodValue + SemCriseActivity.HASHTAG_SEPARATOR + paymentMethodIndex;
        }
        isMonthlyRecurrent = isMonthlyRecurrentCheckbox.isChecked();
        setEffectiveEventStatus();
    }

    private void setEffectiveEventStatus() {
        boolean eventIsEffective = effectOnThatDateCheckbox.isChecked() || effectiveDate != null;
        if(eventIsEffective) {
            effectiveDate = expectedDate;
        } else {
            effectiveDate = null;
        }
    }

    @Override
    public void onDestroy() {
        if(isReceipt) {
            receiptsService.closeDatabase();
        } else {
            expensesService.closeDatabase();
        }
        super.onDestroy();
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        if(isChecked) {
            int dateLabel = isReceipt ? R.string.receipt_date_label : R.string.expense_date_label;
            dateLabelTextView.setText(dateLabel);
        } else {
            dateLabelTextView.setText(R.string.expected_date_text);
        }
    }
}
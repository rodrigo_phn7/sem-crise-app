package br.com.missio.semcrise.services.barcodereader;

import br.com.missio.semcrise.utils.CodeChecker;
import br.com.missio.semcrise.utils.RealValueOrReference;

public class DigitableLineVerifier {

    public static final int BANK_SLIP_FIRST_FIELD_SIZE = 10;
    public static final int BANK_SLIP_FIELD_SIZE = 11;
    public static final int DEALER_FIELD_SIZE = 12;
    private String digitableLine;
    private int identificationOfRealValueOrReference = 0;
    private int digitableLineLength;

    public DigitableLineVerifier(String digitableLine) {
        this.digitableLine = digitableLine;
        digitableLineLength = digitableLine.length();
        if(digitableLineLength >= 3) {
            identificationOfRealValueOrReference = Integer.parseInt(digitableLine.substring(2, 3));
        }
    }

    public boolean isValid() {
        return isBankSlipDigitableLineValid()
                || isDealerDigitableLineValid();
    }

    private boolean isDealerDigitableLineValid() {
        boolean firstFieldValid = isDealerFieldValid(DEALER_FIELD_SIZE);
        boolean secondFieldValid = isDealerFieldValid(2*DEALER_FIELD_SIZE);
        boolean thirdFieldValid = isDealerFieldValid(3*DEALER_FIELD_SIZE);
        boolean fourthFieldValid = isDealerFieldValid(4*DEALER_FIELD_SIZE);
        return firstFieldValid && secondFieldValid && thirdFieldValid && fourthFieldValid;
    }

    private boolean isDealerFieldValid(int dealerFieldSize) {
        boolean fieldValid = true;
        if(digitableLineLength >= dealerFieldSize) {
            String field = extractDealerField(dealerFieldSize);
            CodeChecker codeChecker = new CodeChecker(field);
            int position = field.length() - 1;
            if(is10Module(identificationOfRealValueOrReference)) {
                fieldValid = codeChecker.getCodeChecker(position) == codeChecker.calculateMod10CodeChecker(position);
            } else if(is11Module(identificationOfRealValueOrReference)) {
                int fomDigitableLine = codeChecker.getCodeChecker(position);
                int calculated = codeChecker.calculateMod11CodeChecker(position);
                fieldValid = fomDigitableLine == calculated;
            } else {
                return false;
            }
        }
        return fieldValid;
    }

    private String extractDealerField(int dealerFieldSize) {
        return digitableLine.substring(dealerFieldSize - DEALER_FIELD_SIZE, dealerFieldSize);
    }

    private boolean isBankSlipDigitableLineValid() {
        boolean firstFieldValid = isBankSlipFieldValid(BANK_SLIP_FIRST_FIELD_SIZE);
        boolean secondFieldValid = isBankSlipFieldValid(BANK_SLIP_FIRST_FIELD_SIZE + BANK_SLIP_FIELD_SIZE);
        boolean thirdFieldValid = isBankSlipFieldValid(BANK_SLIP_FIRST_FIELD_SIZE + 2*BANK_SLIP_FIELD_SIZE);
        return firstFieldValid && secondFieldValid && thirdFieldValid;
    }

    private boolean isBankSlipFieldValid(int bankSlipFieldSize) {
        boolean fieldValid = true;
        if(digitableLineLength >= bankSlipFieldSize) {
            String field = extractBankSlipField(bankSlipFieldSize);
            CodeChecker codeChecker = new CodeChecker(field);
            int position = field.length() - 1;
             fieldValid = (codeChecker.getCodeChecker(position)
                    == codeChecker.calculateMod10CodeChecker(position));
        }
        return fieldValid;
    }

    private String extractBankSlipField(int bankSlipFieldSize) {
        int start = bankSlipFieldSize - BANK_SLIP_FIELD_SIZE;
        start = (start == -1) ? 0 : start;
        return digitableLine.substring(start, bankSlipFieldSize);
    }

    private boolean is11Module(int identificationOfRealValueOrReference) {
        return RealValueOrReference.CURRENCY_QUANTITY_MOD11.getIdentification() == identificationOfRealValueOrReference
                || RealValueOrReference.EFFECTIVE_MOD11.getIdentification() == identificationOfRealValueOrReference;
    }

    private boolean is10Module(int identificationOfRealValueOrReference) {
        return RealValueOrReference.CURRENCY_QUANTITY_MOD10.getIdentification() == identificationOfRealValueOrReference
                || RealValueOrReference.EFFECTIVE_MOD10.getIdentification() == identificationOfRealValueOrReference;
    }
}
package br.com.missio.semcrise.utils;

public interface Constants {
    long ONE_SECOND = 1000;
    int BARCODE_SIZE = 44;
    int BANK_SLIP_CODE_CHECKER_POSITION = 4;
    int PARSED_BARCODE_BANK_SLIP_SIZE = 6;
    int DEALERS_CODE_CHECKER_POSITION = 3;
}

package br.com.missio.semcrise.presenters.fragments;

import android.widget.ArrayAdapter;
import java.util.Collections;
import br.com.missio.semcrise.model.MonetaryEvent;
import br.com.missio.semcrise.model.MonetaryEventsList;
import br.com.missio.semcrise.presenters.viewAdapters.MonetaryEventsArrayAdapter;
import br.com.missio.semcrise.services.ExpensesService;
import br.com.missio.semcrise.services.ReceiptsService;

public class DailyEventsListFragment extends SemCriseListFragment {

    @Override
    public void onResume() {
        super.onResume();
        setUpList();
    }

    private void setUpList() {
        ExpensesService expensesService = new ExpensesService(getSemCriseActivity().getApplicationContext());
        ReceiptsService receiptsService = new ReceiptsService(getSemCriseActivity().getApplicationContext());
        MonetaryEventsList events = new MonetaryEventsList();
        events.addAll(expensesService.getDailyExpenses());
        events.addAll(receiptsService.getDailyReceipts());
        Collections.sort(events);
        setEvents(events);
        setExpensesService(expensesService);
        setReceiptsService(receiptsService);
        ArrayAdapter<MonetaryEvent> adapter = new MonetaryEventsArrayAdapter(getSemCriseActivity(), events);
        setListAdapter(adapter);
    }
}


package br.com.missio.semcrise.presenters.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import java.util.Calendar;
import java.util.Date;

import br.com.missio.semcrise.R;
import br.com.missio.semcrise.model.User;
import br.com.missio.semcrise.presenters.activities.SemCriseActivity;
import br.com.missio.semcrise.services.ExpensesService;
import br.com.missio.semcrise.utils.DateHelper;

public class MainFragment extends SemCriseFragment {

    private View fragmentMainView;
    private SemCriseActivity activity;
    private DailyEventsListFragment dailyEventsListFragment;
    private ExpensesService expensesService;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        activity = (SemCriseActivity) getActivity();
        expensesService = new ExpensesService(activity);
        fragmentMainView = inflater.inflate(R.layout.fragment_main, container, false);
        return fragmentMainView;
    }

    @Override
    public void onResume() {
        super.onResume();
        setEvents(expensesService.getAllExpenses());
        setUpMonthlyFragment();
        setUpDailyEventsListFragment();
        setUpDailyBalanceFragment();
        setUpCompleteRegisterButton();
        setUpLateExpensesAlertLinearLayout();
        setUpAppBarTitle(activity);
        setUpDate();
    }

    private void setUpLateExpensesAlertLinearLayout() {
        Button lateExpensesAlertButton = (Button) fragmentMainView.findViewById(R.id.late_expenses_alert_button);
        if(hasLateExpenses()) {
            String alertText = activity.getString(R.string.more_than_one_late_expenses_message_pt01) +
                    getLateExpensesQuantity() +
                    activity.getString(R.string.more_than_one_late_expenses_message_pt02);
            lateExpensesAlertButton.setText(alertText);
        }else {
            lateExpensesAlertButton.setVisibility(View.GONE);
        }
    }

    private void setUpCompleteRegisterButton() {
        Button completeRegisterButton = (Button) fragmentMainView.findViewById(R.id.main_complete_register_button);
        if(userRegisterIsComplete()) {
            completeRegisterButton.setVisibility(View.GONE);
        }
    }

    private boolean userRegisterIsComplete() {
        User user = SemCriseActivity.getUser(activity);
        return user.getCpf() != null;
    }

    private void setUpDate() {
        Date now = Calendar.getInstance().getTime();
        String dateToShow = DateHelper.formatDate(now, getString(R.string.main_activity_date_format));
        TextView dateTextView = (TextView) fragmentMainView.findViewById(R.id.actual_date);
        dateTextView.setText(dateToShow);
    }

    private void setUpMonthlyFragment() {
        MonthlyBalanceFragment monthlyBalanceFragment = new MonthlyBalanceFragment();
        FragmentManager fragmentManager = activity.getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.add(R.id.container_monthly_balance, monthlyBalanceFragment);
        fragmentTransaction.commit();
    }

    private void setUpDailyEventsListFragment() {
        dailyEventsListFragment = new DailyEventsListFragment();
        FragmentManager fragmentManager = getFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.add(R.id.container_daily_events_list, dailyEventsListFragment);
        fragmentTransaction.commit();
    }

    private void setUpDailyBalanceFragment() {
        DailyBalanceFragment dailyBalanceFragment = new DailyBalanceFragment();
        FragmentManager fragmentManager = activity.getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.add(R.id.container_daily_balance, dailyBalanceFragment);
        fragmentTransaction.commit();
    }

    @Override
    public void onPause() {
        super.onPause();
        removeFragment(activity,dailyEventsListFragment);
    }
}

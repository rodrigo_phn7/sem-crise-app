package br.com.missio.semcrise.presenters.activities;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import java.io.IOException;

import br.com.missio.semcrise.model.Creditor;
import br.com.missio.semcrise.services.CreditorsService;
import br.com.missio.semcrise.services.UserService;
import br.com.missio.semcrise.utils.SessionManager;
import io.realm.Realm;

public class SplashActivity extends SemCriseActivity {

    private static final String TAG = SplashActivity.class.getSimpleName();
    private UserService userService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        loadConfigurations();
        giveDirectionsToStartApp();
        finish();
    }

    private void loadConfigurations(){
        Context context = getApplicationContext();
        CreditorsService creditorsService = new CreditorsService();
        try {
            creditorsService.loadCreditors();
        } catch (IOException e) {
            e.printStackTrace();
            Log.e(TAG,"Unable load creditors for this reason: " + e.getMessage());
        }
        userService = new UserService(context);
    }

    private void giveDirectionsToStartApp() {
        if(thereIsUserRegistered()) {
            SessionManager session = new SessionManager(getApplicationContext());
            session.clearSession();
            goTo(LoginActivity.class);
        } else {
            goTo(BeginningAppActivity.class);
        }
    }

    private boolean thereIsUserRegistered() {
        return userService.thereIsUserRegistered();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}
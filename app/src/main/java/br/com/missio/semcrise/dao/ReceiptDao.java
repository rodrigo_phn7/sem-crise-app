package br.com.missio.semcrise.dao;

import android.util.Log;

import java.util.Calendar;
import java.util.Date;
import java.util.UUID;

import br.com.missio.semcrise.model.Receipt;
import br.com.missio.semcrise.model.MonetaryEventsList;
import io.realm.Realm;
import io.realm.RealmResults;

public class ReceiptDao {

    private Realm realmDatabase;

    public ReceiptDao(Realm realmDatabase) {
        this.realmDatabase = realmDatabase;
    }

    public MonetaryEventsList findByEmail(String email) {
        RealmResults<Receipt> receiptsResults = realmDatabase.where(Receipt.class).equalTo("userEmail", email).findAll();
        MonetaryEventsList receipts = new MonetaryEventsList();
        for (Receipt result : receiptsResults) {
            receipts.add(result);
        }
        return receipts;
    }

    public Receipt findById(String id) {
        return realmDatabase.where(Receipt.class).equalTo("id",id).findFirst();
    }

    public void delete(Receipt receiptToDelete){
        realmDatabase.beginTransaction();
        try {
            receiptToDelete.removeFromRealm();
        } catch (Exception e) {
            e.printStackTrace();
            Log.i("TryDelete", e.getMessage());
        }
        realmDatabase.commitTransaction();
    }

    public void add(Receipt newReceipt) {
        newReceipt.setId(UUID.randomUUID().toString());
        newReceipt.setDateItWasAdded(Calendar.getInstance().getTime());
        realmDatabase.beginTransaction();
        realmDatabase.copyToRealm(newReceipt);
        realmDatabase.commitTransaction();
    }

    public void update(Receipt receipt, String name, String sourceOrCreditor, Double value, boolean prefixedValue, Date expectedDate, boolean monthlyRecurrent, Date effectiveDate) {
        realmDatabase.beginTransaction();
        receipt.setName(name);
        receipt.setSource(sourceOrCreditor);
        receipt.setValue(value);
        receipt.setPrefixedValue(prefixedValue);
        receipt.setExpectedDate(expectedDate);
        receipt.setMonthlyRecurrent(monthlyRecurrent);
        receipt.setEffectiveDate(effectiveDate);
        realmDatabase.copyToRealm(receipt);
        realmDatabase.commitTransaction();
    }

    public void update(Receipt receipt, Date effectiveDate) {
        realmDatabase.beginTransaction();
        receipt.setEffectiveDate(effectiveDate);
        realmDatabase.copyToRealm(receipt);
        realmDatabase.commitTransaction();
    }
}
package br.com.missio.semcrise.services;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;

import java.util.Date;

import br.com.missio.semcrise.R;
import br.com.missio.semcrise.dao.UserDao;
import br.com.missio.semcrise.model.User;
import br.com.missio.semcrise.utils.SessionManager;
import io.realm.Realm;

public class UserService {
    private static final String TAG = UserService.class.getSimpleName();
    private Context context;
    private UserDao userDao;
    Realm realmDatabase;

    public UserService(Context context) {
        this.context = context;
        try {
            realmDatabase = Realm.getDefaultInstance();
            userDao = new UserDao(realmDatabase);
        } catch (Exception e) {
            Log.e(TAG,e.getMessage());
            e.printStackTrace();
            throw e;
        }
    }

    public boolean thereIsUserRegistered() {
        return userDao.findAll().size() > 0;
    }

    public void addUserWith(String name, String email, String phone, String password) {
        User user = new User();
        user.setName(name);
        user.setEmail(email);
        user.setPhone(phone);
        user.setPass(password);
        userDao.add(user);
    }

    public boolean login(String email, String password) {
        User loginUser = userDao.getUserDataBy(email);
        if((loginUser != null) && loginUser.getPass().equals(password)){
            putUserInSession(loginUser);
            return true;
        }
        return false;
    }

    private void putUserInSession(User loginUser) {
        SessionManager session = new SessionManager(context);
        session.createLoginSession(loginUser.getUserSessionData());
    }

    public User getUserByEmail(String email) {
        return userDao.getUserDataBy(email);
    }

    public void updateUserWith(User user, String cpf, String postalCode, Date birthDate, String profession, String education, Boolean negotiationEnabled) {
        userDao.updateUserWith(user,cpf,postalCode,birthDate,profession,education,negotiationEnabled);
    }

    public void changePassword(String pass) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        String email = sharedPreferences.getString(context.getString(R.string.key_pref_user_email), "");
        User user = userDao.getUserDataBy(email);
        userDao.update(user,pass);
    }
}
package br.com.missio.semcrise.stubs;

import java.util.Calendar;
import java.util.Date;

import br.com.missio.semcrise.presenters.activities.SemCriseActivity;
import br.com.missio.semcrise.services.ExpensesService;
import br.com.missio.semcrise.services.ReceiptsService;

public class StubGenerator {

    public static final String RODRIGO_PHN7_GMAIL_COM = "rodrigo.phn7@gmail.com";

    public static void loadReceiptsStubData(ReceiptsService receiptsService) {
        String userEmail = RODRIGO_PHN7_GMAIL_COM;
        int a;
        int b;
        for(int i = 1; i<21; i++) {
            Calendar calendar = Calendar.getInstance();
            String name = "Salario" + i;
            String receiptSource = "Important Thing";
            Double value = (double) i*3;

            if(i % 10 == 0) {
                a = (i/10);
                calendar.add(Calendar.MONTH, -a);
            }

            if(i>=11) {
                calendar.add(Calendar.YEAR,1);
            }

            if(i % 5 == 0) {
                b = (i/5);
                calendar.add(Calendar.DAY_OF_MONTH,-b);
            }
            Date expectedDate = calendar.getTime();
            Date effectiveDate = calendar.getTime();
            receiptsService.addReceiptWith(name,receiptSource,value,false,expectedDate,false,userEmail,effectiveDate);
        }

    }

    public static void loadExpensesStubData(ExpensesService expensesService) {
        String userEmail = RODRIGO_PHN7_GMAIL_COM;
        int a;
        int b;
        for(int i = 1; i<21; i++) {
            Calendar calendar = Calendar.getInstance();
            String name = "Biscoito" + i;
            String sourceOrCreditor = "thatThing";
            Double value = (double) i*2;
            if(i % 10 == 0) {
                a = (i/10);
                calendar.add(Calendar.MONTH, -a);
            }

            if(i>=11) {
                calendar.add(Calendar.YEAR,1);
            }

            if(i % 5 == 0) {
                b = (i/5);
                calendar.add(Calendar.DAY_OF_MONTH,-b);
            }
            Date expectedDate = calendar.getTime();
            Date effectiveDate = calendar.getTime();
            String paymentMethod = "Qualquer coisa" + SemCriseActivity.HASHTAG_SEPARATOR + 1;
            String expenseType = "Coisa alguma"+ SemCriseActivity.HASHTAG_SEPARATOR + 1;
            expensesService.addExpenseWith(name, sourceOrCreditor, value, false, expectedDate, false, paymentMethod, expenseType, userEmail, effectiveDate);
        }

        for(int i = 22; i<42; i++) {
            Calendar calendar = Calendar.getInstance();
            String name = "Fatura do Cartão de Crédito" + i;
            String sourceId = "Banco Fictício";
            Double value = (double) i+3;

            if(i % 10 == 0) {
                a = (i/10);
                calendar.add(Calendar.MONTH, -a);
            }

            if(i>=32) {
                calendar.add(Calendar.YEAR,-1);
            }

            if(i % 5 == 0) {
                b = (i/5);
                calendar.add(Calendar.DAY_OF_MONTH,-b);
            }
            Date expectedDate = calendar.getTime();
            String paymentMethod = "Débito em Conta" + SemCriseActivity.HASHTAG_SEPARATOR + 1;
            String expenseType = "Carnê ou Prestação" + SemCriseActivity.HASHTAG_SEPARATOR + 1;
            expensesService.addExpenseWith(name, sourceId, value, false, expectedDate, false, paymentMethod, expenseType, userEmail, null);
        }
    }
}
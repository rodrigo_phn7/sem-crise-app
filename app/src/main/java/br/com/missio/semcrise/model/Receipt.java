package br.com.missio.semcrise.model;

import android.content.Context;

import java.util.Date;

import br.com.missio.semcrise.R;
import br.com.missio.semcrise.utils.DateHelper;
import br.com.missio.semcrise.utils.EventDateHelper;
import io.realm.RealmObject;
import io.realm.annotations.Required;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false, exclude = {"userEmail","source","expectedDate","effectiveDate","name","monthlyRecurrent","prefixedValue"})
public class Receipt extends RealmObject implements MonetaryEvent {

    @Required
    private String id;
    @Required
    private String name;
    @Required
    private String source;
    @Required
    private Double value;
    @Required
    private Date expectedDate;
    @Required
    private String userEmail;
    @Required
    private Date dateItWasAdded;

    private Date effectiveDate;
    private boolean prefixedValue;
    private boolean monthlyRecurrent;

    @Override
    public Money getMoneyValue() {
        return new Money(getValue());
    }

    @Override
    public String getFormattedActualDate(Context context) {
        return DateHelper.formatDate(getActualDate(), context.getString(R.string.tiny_date_format));
    }

    @Override
    public Date getActualDate() {
        Date effectiveDate = getEffectiveDate();
        if(effectiveDate==null){
            return getExpectedDate();
        }
        return effectiveDate;
    }

    @Override
    public boolean isLate() {
        return EventDateHelper.isLate(effectiveDate,expectedDate);
    }

    @Override
    public String getEventType() {
        return MonetaryEvent.RECEIPT;
    }

    @Override
    public boolean isEffective() {
        return getEffectiveDate() != null;
    }

    @Override
    public boolean isNegotiationActive() {
        return false;
    }

    @Override
    public String getEventStatus() {
        if(isEffective()) {
            return "(recebido)";
        } else {
            return "(a receber)";
        }
    }

    @Override
    public int compareTo(MonetaryEvent another) {
        return getDateItWasAdded().compareTo(another.getDateItWasAdded());
    }

}
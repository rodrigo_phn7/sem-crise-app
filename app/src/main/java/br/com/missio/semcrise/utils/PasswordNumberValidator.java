package br.com.missio.semcrise.utils;

import android.widget.EditText;

import com.andreabaccega.formedittextvalidator.Validator;
public class PasswordNumberValidator extends Validator {
    public PasswordNumberValidator(String _customErrorMessage) {
        super(_customErrorMessage);
    }

    @Override
    public boolean isValid(EditText et) {
        return et.getText().length() >= 6;
    }
}
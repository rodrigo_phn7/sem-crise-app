package br.com.missio.semcrise.model;


import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Locale;

public class BrazilianDecimalFormat {
    private static Locale BRAZIL = new Locale("pt","BR");
    private static DecimalFormatSymbols REAL = new DecimalFormatSymbols(BRAZIL);
    public static DecimalFormat MONEY_REAL = new DecimalFormat("¤ ###,###,##0.00",REAL);
}
package br.com.missio.semcrise.services;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

public class BootReceiver extends BroadcastReceiver {
    private AlarmReceiver alarm = new AlarmReceiver();

    @Override
    public void onReceive(Context context, Intent intent) {
        Log.w("boot_broadcast_poc", "starting boot receiving...");
        if("android.intent.action.BOOT_COMPLETED".equals(intent.getAction())
                || "android.intent.action.QUICKBOOT_POWERON".equals(intent.getAction())
                ) {
            Log.w("boot_broadcast_poc", "setting alarm...");
            alarm.setAlarm(context);
        }
    }
}
package br.com.missio.semcrise.utils;

import br.com.missio.semcrise.exceptions.InvalidBarcodeBillValueException;

public class BarcodeBillValueParser {

    public static final int DECIMAL_SEPARATOR_POSITION = 2;

    public static Double parseBillValue(String barcodeBillValue) throws InvalidBarcodeBillValueException {
        if(isValid(barcodeBillValue)) {
            char[] billValueDigits = barcodeBillValue.toCharArray();
            char[] decimalBillValueDigits = new char[billValueDigits.length + 1];
            int backToFrontDecimalBillValuePosition = 0;
            int stringBillValuePosition = billValueDigits.length - 1;
            for(int decimalBillValuePosition = billValueDigits.length;
                decimalBillValuePosition>=0;
                decimalBillValuePosition--) {
                if(backToFrontDecimalBillValuePosition == DECIMAL_SEPARATOR_POSITION) {
                    decimalBillValueDigits[decimalBillValuePosition] = '.';
                } else {
                    decimalBillValueDigits[decimalBillValuePosition] =
                            billValueDigits[stringBillValuePosition];
                    stringBillValuePosition--;
                }
                backToFrontDecimalBillValuePosition++;
            }
            String stringBillValueDigits = String.valueOf(decimalBillValueDigits);
            return Double.parseDouble(stringBillValueDigits);
        } else {
            throw new InvalidBarcodeBillValueException("Barcode Bill Value: "
                    + barcodeBillValue + " is invalid.");
        }
    }
    private static boolean isValid(String barcodeBillValue) {
        return barcodeBillValue != null && barcodeBillValue.length()>2;
    }
}

package br.com.missio.semcrise.exceptions;

public class BarcodeInvalidException extends Exception {

    public BarcodeInvalidException(String message) {
        super(message);
    }
}

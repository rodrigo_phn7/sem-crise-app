package br.com.missio.semcrise.utils;

import br.com.missio.semcrise.exceptions.BarcodeIsNotDealersException;

public class BarcodeChecker {
    private final CodeChecker codeChecker;
    private String barcode;

    public BarcodeChecker(String barcode) {
        this.barcode = barcode;
        codeChecker = new CodeChecker(barcode);
    }

    public String getBarcodeType() {
        if(barcode.length() == Constants.BARCODE_SIZE && barcodeHasOnlyNumbers()) {
            if(isBankSlipType()) {
                return BarcodeType.BANK_SLIP;
            } else if(isDealersType()) {
                 return BarcodeType.DEALERS;
            }
        }
        return BarcodeType.NULL;
    }

    private boolean barcodeHasOnlyNumbers() {
        char[] barcodeSequence = barcode.toCharArray();
        for (char digit : barcodeSequence) {
            try {
                Integer.parseInt(digit + "");
            } catch (NumberFormatException e) {
                return false;
            }
        }
        return true;
    }

    private boolean isBankSlipType() {
        int calculatedCodeChecker = codeChecker.calculateMod11BankSlipCodeChecker(Constants.BANK_SLIP_CODE_CHECKER_POSITION);
        int barcodeCodeChecker = codeChecker.getCodeChecker(Constants.BANK_SLIP_CODE_CHECKER_POSITION);
        return calculatedCodeChecker == barcodeCodeChecker;
    }

    private boolean isDealersType() {
        int calculatedCodeChecker;
        int barcodeCodeChecker = codeChecker.getCodeChecker(Constants.DEALERS_CODE_CHECKER_POSITION);
        try {
            calculatedCodeChecker = calculateDealersCodeChecker();
        } catch (BarcodeIsNotDealersException e) {
            e.printStackTrace();
            return false;
        }
        return calculatedCodeChecker == barcodeCodeChecker;
    }

    private int calculateDealersCodeChecker() throws BarcodeIsNotDealersException {
        int realValueOrReferenceIdentification = getRealValueOrReferenceIdentification();
        if(RealValueOrReference.CURRENCY_QUANTITY_MOD10.getIdentification() == realValueOrReferenceIdentification
                || RealValueOrReference.EFFECTIVE_MOD10.getIdentification() == realValueOrReferenceIdentification) {
            return codeChecker.calculateMod10CodeChecker(Constants.DEALERS_CODE_CHECKER_POSITION);
        } else if(RealValueOrReference.CURRENCY_QUANTITY_MOD11.getIdentification() == realValueOrReferenceIdentification
                || RealValueOrReference.EFFECTIVE_MOD11.getIdentification() == realValueOrReferenceIdentification) {
            return codeChecker.calculateMod11CodeChecker(Constants.DEALERS_CODE_CHECKER_POSITION);
        }
        throw new BarcodeIsNotDealersException("Barcode: " + barcode + " is not Dealers Type");
    }

    private int getRealValueOrReferenceIdentification() {
        return Integer.parseInt(barcode.substring(2,3));
    }
}
package br.com.missio.semcrise.presenters.fragments;

import android.content.res.Resources;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import br.com.missio.semcrise.R;
import br.com.missio.semcrise.model.DateStatus;

public class FullStatementFragment extends ShowEventsFragment implements AdapterView.OnItemSelectedListener {

    private Spinner dateStatusSpinner;
    private HashMap<String, String> dateStatusMap;
    private Spinner periodSpinner;
    private ArrayList<String> dateStatusItems;
    private List<String> periodItems;
    private DateStatus dateStatus;
    private int period;

    @Override
    public void onResume() {
        super.onResume();
        setDateStatusMap();
        setUpInputs();
    }

    private void setDateStatusMap() {
        dateStatusMap = new HashMap<>();
        dateStatusMap.put(DateStatus.FUTURE.name(), getString(R.string.future_date_status));
        dateStatusMap.put(DateStatus.PAST.name(), getString(R.string.past_date_status));
    }

    private void setUpInputs() {
        dateStatusSpinner = (Spinner) getShowExpensesView().findViewById(R.id.date_status_spinner);
        dateStatusItems = new ArrayList<>();
        dateStatusItems.add(dateStatusMap.get(DateStatus.PAST.name()));
        dateStatusItems.add(dateStatusMap.get(DateStatus.FUTURE.name()));
        ArrayAdapter<String> dateStatusAdapter = new ArrayAdapter<>(getSemCriseActivity(),android.R.layout.simple_spinner_item,dateStatusItems);
        dateStatusAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        dateStatusSpinner.setAdapter(dateStatusAdapter);
        dateStatusSpinner.setOnItemSelectedListener(this);
        keepSameDateStatus();

        periodItems = getPeriodItems();
        periodSpinner = (Spinner) getShowExpensesView().findViewById(R.id.period_spinner);
        ArrayAdapter<String> periodAdapter = new ArrayAdapter<>(getSemCriseActivity(),android.R.layout.simple_spinner_item,periodItems);
        periodAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        periodSpinner.setAdapter(periodAdapter);
        periodSpinner.setOnItemSelectedListener(this);
        keepSamePeriod();
    }

    private void keepSamePeriod() {
        if(period != -1) {
            periodSpinner.setSelection(getPosition(period));
        }
    }

    private int getPosition(int period) {
        int pos = 0;
        for (String item : periodItems) {
            if(item.startsWith(String.valueOf(period))) {
                return pos;
            }
            pos++;
        }
        return -1;
    }

    private void keepSameDateStatus() {
        if(dateStatus != null) {
            dateStatusSpinner.setSelection(getPosition(dateStatus));
        }
    }

    private int getPosition(DateStatus dateStatus) {
        int pos = 0;
        for (String item : dateStatusItems) {
            if(item.equals(dateStatusMap.get(dateStatus.name()))) {
                return pos;
            }
            pos ++;
        }
        return -1;
    }

    private List<String> getPeriodItems() {
        Resources res = getResources();
        String[] periodArray = res.getStringArray(R.array.string_array_period);
        List<String> periodItems = new ArrayList<>();
        String days = getString(R.string.days);
        for (String period : periodArray) {
            periodItems.add(period + " " + days);
        }
        return periodItems;
    }

    public void performFilterStatement() {
        DateStatus dateStatus = getDateStatusFromInput();
        int period = getPeriodFromInput();
        FullStatementEventListFragment fullStatementListFragment = (FullStatementEventListFragment) getListFragment();
        if(fullStatementListFragment != null) {
            fullStatementListFragment.fillList(dateStatus,period);
        }
    }

    @NonNull
    private DateStatus getDateStatusFromInput() {
        DateStatus dateStatus;
        String dateStatusString = dateStatusSpinner.getSelectedItem().toString();
        dateStatus = dateStatusString.equals(dateStatusMap.get(DateStatus.PAST.name())) ? DateStatus.PAST : DateStatus.FUTURE;
        return dateStatus;
    }

    private int getPeriodFromInput() {
        int period;
        String periodString = periodSpinner.getSelectedItem().toString();
        String[] periodArrayString = periodString.split(" ");
        period = Integer.valueOf(periodArrayString[0]);
        return period;
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        performFilterStatement();
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {
    }

    @Override
    public void onPause() {
        super.onPause();
        dateStatus = ((FullStatementEventListFragment)getListFragment()).getDateStatus();
        period = ((FullStatementEventListFragment)getListFragment()).getPeriod();
    }
}
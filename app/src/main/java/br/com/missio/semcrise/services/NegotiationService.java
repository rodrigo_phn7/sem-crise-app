package br.com.missio.semcrise.services;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;
import android.text.TextUtils;

import java.io.UnsupportedEncodingException;
import java.util.UUID;

import javax.mail.MessagingException;

import br.com.missio.semcrise.R;
import br.com.missio.semcrise.dao.CreditorDao;
import br.com.missio.semcrise.dao.ExpenseDao;
import br.com.missio.semcrise.model.Creditor;
import br.com.missio.semcrise.model.Expense;
import br.com.missio.semcrise.model.User;
import br.com.missio.semcrise.presenters.activities.SemCriseActivity;
import br.com.missio.semcrise.utils.DateHelper;
import br.com.missio.semcrise.utils.SessionManager;
import io.realm.Realm;

public class NegotiationService extends IntentService {

    public static final String NEGOTIATION_SUCCESS = "negotiation_success";
    public static final String NEGOTIATION_UNSUCCESSFUL = "negotiation_unsuccessful";
    private String userEmail;
    private Context context;

    public NegotiationService() {
        super("NegotiationService");
    }

    @Override
    public void onCreate() {
        super.onCreate();
        SessionManager session = new SessionManager(getApplicationContext());
        if(session.isUserLoggedIn()) {
            userEmail = session.getUserData().get(User.EMAIL_TAG);
        }
        context = getApplicationContext();
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        String expenseId = TextUtils.split(intent.getAction(), SemCriseActivity.HASHTAG_SEPARATOR)[1];
        User user = getUser();
        //TODO: Creditor here
        String receiverEmail = user.getEmail();
        String messageBody = composeMessageWithUserInformation(expenseId);
        String subject = getString(R.string.debt_by) + " " + user.getName();
        try {
            MailSender.sendMail(context, receiverEmail, subject, messageBody);
            sendSuccessBroadcast();
        } catch (MessagingException | UnsupportedEncodingException e) {
            e.printStackTrace();
            sendUnsuccessfulBroadcast();
        }
    }

    private void sendSuccessBroadcast() {
        Intent success = new Intent(NEGOTIATION_SUCCESS);
        LocalBroadcastManager.getInstance(this).sendBroadcast(success);
    }

    private void sendUnsuccessfulBroadcast() {
        Intent unsuccessful = new Intent(NEGOTIATION_UNSUCCESSFUL);
        LocalBroadcastManager.getInstance(this).sendBroadcast(unsuccessful);
    }

    private String composeMessageWithUserInformation(String expenseId) {
        Realm realmDatabase = Realm.getDefaultInstance();
        ExpenseDao expensesDao = new ExpenseDao(realmDatabase);
        Expense expense = expensesDao.findById(expenseId);
        CreditorDao creditorDao = new CreditorDao(realmDatabase);
        Creditor creditor =  creditorDao.getCreditorById(expense.getCreditorId());
        User user = getUser();
        String formattedExpectedDate = DateHelper.formatDate(expense.getExpectedDate(), context.getString(R.string.tiny_date_format));
        //TODO: ContratoID aqui
        String message = getString(R.string.negotiation_message_pt01) +
                getString(R.string.negotiation_message_pt02) +
                getString(R.string.negotiation_message_pt03) + user.getName() +
                getString(R.string.negotiation_message_pt04) + user.getCpf() +
                getString(R.string.negotiation_message_pt05) + UUID.randomUUID().toString() +
                getString(R.string.negotiation_message_pt06) + creditor.getAvailableName() +
                getString(R.string.negotiation_message_pt07) + user.getName() +
                getString(R.string.negotiation_message_pt08) + creditor.getAvailableName() +
                getString(R.string.negotiation_message_pt09) + user.getName() +
                getString(R.string.negotiation_message_pt10) + expense.getName() +
                getString(R.string.negotiation_message_pt11) + expense.getMoneyValue() +
                getString(R.string.negotiation_message_pt12) + expense.getCleanPaymentMethod() +
                getString(R.string.negotiation_message_pt13) + formattedExpectedDate +
                getString(R.string.negotiation_message_pt14);
        realmDatabase.close();
        return message;
    }

    private User getUser() {
        UserService userService = new UserService(context);
        return userService.getUserByEmail(userEmail);
    }

}

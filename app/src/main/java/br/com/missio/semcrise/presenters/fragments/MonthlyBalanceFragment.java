package br.com.missio.semcrise.presenters.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.text.DecimalFormat;
import java.util.Currency;

import br.com.missio.semcrise.R;
import br.com.missio.semcrise.model.BrazilianDecimalFormat;
import br.com.missio.semcrise.services.ExpensesService;
import br.com.missio.semcrise.services.ReceiptsService;

public class MonthlyBalanceFragment extends Fragment {

    private View view;
    private ReceiptsService receiptsService;
    private ExpensesService expensesService;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_monthly_balance,container,false);
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        setUpMonthlyBalance();
    }

    private void setUpMonthlyBalance() {
        Context context = getActivity().getApplicationContext();
        receiptsService = new ReceiptsService(context);
        Double receiptMonthlyAmount = receiptsService.getMonthlyAmountUntilNow();
        expensesService = new ExpensesService(context);
        Double expenseMonthlyAmount = expensesService.getMonthlyAmountUntilNow();
        TextView monthBalanceTextView = (TextView) view.findViewById(R.id.month_partial_balance_amount);
        Double monthlyBalance = receiptMonthlyAmount - expenseMonthlyAmount;
        DecimalFormat decimalFormat = BrazilianDecimalFormat.MONEY_REAL;
        monthBalanceTextView.setText(decimalFormat.format(monthlyBalance));
    }

    @Override
    public void onDestroy() {
        receiptsService.closeDatabase();
        expensesService.closeDatabase();
        super.onDestroy();
    }
}
package br.com.missio.semcrise.model;

import android.text.TextUtils;

import java.util.Date;
import java.util.HashMap;

import io.realm.RealmObject;
import io.realm.annotations.Required;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false, exclude = {"name","pass","birthDate","profession","postalCode","education","negotiationEnabled"})
public class User extends RealmObject{

    public static String EMAIL_TAG = "user_email";
    public static String FIRSTNAME_TAG = "user_firstname";

    @Required
    private String name;
    @Required
    private String email;
    @Required
    private String phone;
    @Required
    private String pass;

    private String cpf;
    private Date birthDate;
    private String profession;
    private String postalCode;
    private String education;
    private boolean negotiationEnabled = false;

    public String getFirstName() {
        String[] subNames = TextUtils.split(name," ");
        return subNames[0];
    }

    public HashMap<String, String> getUserSessionData(){
        HashMap<String,String> userSessionData = new HashMap<>();
        userSessionData.put(EMAIL_TAG, email);
        userSessionData.put(FIRSTNAME_TAG, getFirstName());
        return userSessionData;
    }
}

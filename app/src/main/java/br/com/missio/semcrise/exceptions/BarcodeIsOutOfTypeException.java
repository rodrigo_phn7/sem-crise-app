package br.com.missio.semcrise.exceptions;

public class BarcodeIsOutOfTypeException extends Exception {
    public BarcodeIsOutOfTypeException(String message) {
        super(message);
    }
}

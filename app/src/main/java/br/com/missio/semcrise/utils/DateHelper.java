package br.com.missio.semcrise.utils;


import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import br.com.missio.semcrise.exceptions.InvalidMonthNumberException;
import br.com.missio.semcrise.exceptions.MonthNumberOutOfBoundsException;
import br.com.missio.semcrise.model.DateStatus;
import br.com.missio.semcrise.presenters.activities.SemCriseActivity;

public class DateHelper {

    public static final String DATE_DIALOG_TAG = "date_dialog";
    public static final int HALF_TIME_INTERVAL = 30;

    public static boolean isFromCurrentMonth(Date date) {
        Calendar provenCalendar = Calendar.getInstance();
        provenCalendar.setTime(date);
        Calendar currentMonthCalendar = Calendar.getInstance();
        return provenCalendar.get(Calendar.MONTH) == currentMonthCalendar.get(Calendar.MONTH) && provenCalendar.get(Calendar.YEAR) == currentMonthCalendar.get(Calendar.YEAR);
    }

    public static String formatDate(Date date, String format) {
        DateFormat dateFormat = new SimpleDateFormat(format);
        return dateFormat.format(date);
    }

    public static boolean isFromCurrentDay(Date actualDate) {
        Calendar anotherMonthCalendar = Calendar.getInstance();
        anotherMonthCalendar.setTime(actualDate);
        Calendar currentMonthCalendar = Calendar.getInstance();
        return (anotherMonthCalendar.get(Calendar.DAY_OF_MONTH) == currentMonthCalendar.get(Calendar.DAY_OF_MONTH)) && isFromCurrentMonth(actualDate);
    }

    public static boolean sameDayOfMonth(Calendar calendar, Calendar otherCalendar) {
        return calendar.get(Calendar.YEAR) == otherCalendar.get(Calendar.YEAR) && calendar.get(Calendar.MONTH) == otherCalendar.get(Calendar.MONTH) && calendar.get(Calendar.DAY_OF_MONTH) == otherCalendar.get(Calendar.DAY_OF_MONTH);
    }

    public static boolean dateMatchesWith(Date date, int period, DateStatus dateStatus) {
        Calendar today = Calendar.getInstance();
        Calendar provenDate = Calendar.getInstance();
        provenDate.setTime(date);
        if(sameDayOfMonth(today,provenDate)){
            return true;
        }
        int signedPeriod = getSignedPeriod(period, dateStatus);
        Calendar periodCalendar = Calendar.getInstance();
        periodCalendar.add(Calendar.DAY_OF_MONTH,signedPeriod);
        if(dateStatus.equals(DateStatus.PAST)) {
            return provenDate.after(periodCalendar) && provenDate.before(today);
        } else {
            return provenDate.before(periodCalendar) && provenDate.after(today);
        }
    }

    private static int getSignedPeriod(int period, DateStatus dateStatus) {
        String signal = dateStatus.equals(DateStatus.PAST) ? "-" : "";
        String signedPeriodString = signal + period;
        return Integer.valueOf(signedPeriodString);
    }

    public static boolean isFromCurrentMonthUntilNow(Date date) {
        Calendar provenCalendar = Calendar.getInstance();
        provenCalendar.setTime(date);
        Calendar now = Calendar.getInstance();
        return isFromCurrentMonth(date) && provenCalendar.before(now);
    }

    public static Calendar generateExpirationTime(int interval) {
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.MINUTE,interval);
        return calendar;
    }

    public static void adjustTimeForWorkingDate(Calendar time, List<Calendar> holidays) {
        while(Calendar.SUNDAY == time.get(Calendar.DAY_OF_WEEK) || Calendar.SATURDAY == time.get(Calendar.DAY_OF_WEEK) || contains(holidays,time)) {
            time.add(Calendar.DAY_OF_MONTH,1);
        }
    }

    public static boolean contains(List<Calendar> holidays, Calendar time) {
        for (Calendar holiday : holidays) {
            if(sameDayOfMonth(holiday,time)) {
                return true;
            }
        }
        return false;
    }

    public static List<Calendar> formatDates(String[] datesStringArray) {
        List<Calendar> dates = new ArrayList<>();
        Calendar date = Calendar.getInstance();
        for (String dateString : datesStringArray) {
            String[] datesParts = dateString.split(SemCriseActivity.HASHTAG_SEPARATOR);
            date.set(Calendar.DAY_OF_MONTH,Integer.valueOf(datesParts[0]));
            try {
                date.set(Calendar.MONTH, parseMonth(datesParts[1]));
            } catch (MonthNumberOutOfBoundsException | InvalidMonthNumberException e) {
                e.printStackTrace();
                return null;
            }
            date.set(Calendar.YEAR,Integer.valueOf(datesParts[2]));
            dates.add(date);
        }
        return dates;
    }

    public static int parseMonth(String month) throws MonthNumberOutOfBoundsException, InvalidMonthNumberException {
        HashMap<String,Integer> monthsData = new HashMap<>();
        monthsData.put("01",Calendar.JANUARY);
        monthsData.put("02",Calendar.FEBRUARY);
        monthsData.put("03",Calendar.APRIL);
        monthsData.put("04",Calendar.APRIL);
        monthsData.put("05",Calendar.MAY);
        monthsData.put("06",Calendar.JUNE);
        monthsData.put("07",Calendar.JULY);
        monthsData.put("08",Calendar.AUGUST);
        monthsData.put("09",Calendar.SEPTEMBER);
        monthsData.put("10",Calendar.OCTOBER);
        monthsData.put("11",Calendar.NOVEMBER);
        monthsData.put("12",Calendar.DECEMBER);
        if(month == null || month.length() != 2) {
            throw new InvalidMonthNumberException("Invalid Month Number " + month + ", should have two digits");
        }
        int monthNumber = Integer.parseInt(month);
        if(monthNumber > 0 && monthNumber <13) {
            return monthsData.get(month);
        }
        throw new MonthNumberOutOfBoundsException("Month Number " + month + " is out of bounds");
    }
}

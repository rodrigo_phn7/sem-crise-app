package br.com.missio.semcrise.exceptions;

public class InvalidBarcodeBillValueException extends Exception {
    public InvalidBarcodeBillValueException(String message) {
        super(message);
    }
}

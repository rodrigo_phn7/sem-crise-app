package br.com.missio.semcrise.exceptions;

public class DayNumberOutOfBoundsException extends Exception{
    public DayNumberOutOfBoundsException(String message) {
        super(message);
    }
}

package br.com.missio.semcrise.model;

import android.content.Context;
import android.support.annotation.NonNull;

import java.util.Date;

import br.com.missio.semcrise.R;
import br.com.missio.semcrise.presenters.activities.SemCriseActivity;
import br.com.missio.semcrise.utils.DateHelper;
import br.com.missio.semcrise.utils.EventDateHelper;
import io.realm.RealmObject;
import io.realm.annotations.Ignore;
import io.realm.annotations.Required;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false, exclude = {"negotiationActive","negotiationDone","userEmail","paymentMethod","expectedDate","effectiveDate","name","monthlyRecurrent","prefixedValue"})
public class Expense extends RealmObject implements MonetaryEvent {

    @Required
    private String id;
    @Required
    private String name;
    @Required
    private String creditorId;
    @Required
    private Double value;
    @Required
    private Date expectedDate;
    @Required
    private String paymentMethod;
    @Required
    private String expenseType;
    @Required
    private Date dateItWasAdded;

    private String userEmail;
    private Date effectiveDate;
    private boolean monthlyRecurrent;
    private boolean prefixedValue;
    private boolean negotiationActive;
    private boolean negotiationDone;

    @Override
    public Money getMoneyValue() {
        return new Money(getValue());
    }

    @Override
    public String getFormattedActualDate(Context context) {
        return DateHelper.formatDate(getActualDate(), context.getString(R.string.tiny_date_format));
    }

    @Override
    public Date getActualDate() {
        Date effectiveDate = getEffectiveDate();
        if(effectiveDate==null){
            return getExpectedDate();
        }
        return effectiveDate;
    }

    @Override
    public boolean isLate() {
        return EventDateHelper.isLate(effectiveDate,expectedDate);
    }

    @Override
    public boolean isEffective() {
        return getEffectiveDate() != null;
    }

    @Override
    public String getEventStatus() {
        if(isEffective()) {
            return "(pago)";
        } else if(isNegotiationActive()) {
            return "(em negociação)";
        } else {
            return "(a pagar)";
        }
    }

    @Override
    public String getEventType() {
        return MonetaryEvent.EXPENSE;
    }

    @Override
    public int compareTo(@NonNull MonetaryEvent another) {
        return getDateItWasAdded().compareTo(another.getDateItWasAdded());
    }

    public String getCleanPaymentMethod() {
        return getPaymentMethod().split(SemCriseActivity.HASHTAG_SEPARATOR)[0];
    }

    public String getCleanExpenseType() {
        return getExpenseType().split(SemCriseActivity.HASHTAG_SEPARATOR)[0];
    }
}
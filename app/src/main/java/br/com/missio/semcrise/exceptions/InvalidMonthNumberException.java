package br.com.missio.semcrise.exceptions;

public class InvalidMonthNumberException extends Exception {
    public InvalidMonthNumberException(String message) {
        super(message);
    }
}

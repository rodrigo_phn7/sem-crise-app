package br.com.missio.semcrise.services;

import android.content.Context;
import android.content.res.Resources;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import br.com.missio.semcrise.R;
import br.com.missio.semcrise.dao.ReceiptDao;
import br.com.missio.semcrise.model.DateStatus;
import br.com.missio.semcrise.model.MonetaryEventsList;
import br.com.missio.semcrise.model.Receipt;
import br.com.missio.semcrise.model.User;
import br.com.missio.semcrise.utils.DateHelper;
import br.com.missio.semcrise.utils.SessionManager;
import io.realm.Realm;

public class ReceiptsService {

    private final List<Calendar> holidays;
    private  Realm realmDatabase;
    private String userEmail;
    private ReceiptDao receiptsDao;

    public ReceiptsService(Context context){
        realmDatabase = Realm.getDefaultInstance();
        receiptsDao = new ReceiptDao(realmDatabase);
        SessionManager session = new SessionManager(context);
        if(session.isUserLoggedIn()) {
            userEmail = session.getUserData().get(User.EMAIL_TAG);
        }
        Resources res = context.getResources();
        String[] holidaysStringArray = res.getStringArray(R.array.holidays);
        holidays = DateHelper.formatDates(holidaysStringArray);
    }

    public Double getMonthlyAmountUntilNow() {
        MonetaryEventsList allReceipts = receiptsDao.findByEmail(userEmail);
        return allReceipts.getMonthlyEventsUntilNow().getTotalAmount();
    }

    public void addReceiptWith(String name, String receiptSource, Double value, Boolean prefixedValue, Date expectedDate, Boolean monthlyRecurrent, String userEmail, Date effectiveDate) {
        if(holidays != null) {
            Calendar expectedCalendar = Calendar.getInstance();
            expectedCalendar.setTime(expectedDate);
            DateHelper.adjustTimeForWorkingDate(expectedCalendar,holidays);
            expectedDate = expectedCalendar.getTime();
            if(effectiveDate != null) {
                Calendar effectiveCalendar = Calendar.getInstance();
                effectiveCalendar.setTime(effectiveDate);
                DateHelper.adjustTimeForWorkingDate(effectiveCalendar,holidays);
                effectiveDate = effectiveCalendar.getTime();
            }
        }
        Receipt receipt = new Receipt();
        receipt.setName(name);
        receipt.setUserEmail(userEmail);
        receipt.setSource(receiptSource);
        receipt.setValue(value);
        receipt.setPrefixedValue(prefixedValue);
        receipt.setExpectedDate(expectedDate);
        receipt.setMonthlyRecurrent(monthlyRecurrent);
        receipt.setEffectiveDate(effectiveDate);
        receiptsDao.add(receipt);
    }

    public MonetaryEventsList getAllReceipts() {
        return receiptsDao.findByEmail(userEmail);
    }

    public Double getDailyTotalAmount() {
        MonetaryEventsList allReceipts = receiptsDao.findByEmail(userEmail);
        return allReceipts.getDailyEffectiveEvents().getTotalAmount();
    }

    public MonetaryEventsList getDailyReceipts() {
        MonetaryEventsList allReceipts = receiptsDao.findByEmail(userEmail);
        return allReceipts.getDailyEffectiveEvents();
    }

    public MonetaryEventsList getAllEventsIn(int period, DateStatus dateStatus) {
        MonetaryEventsList allReceipts = receiptsDao.findByEmail(userEmail);
        return allReceipts.getAllEventsIn(period, dateStatus);
    }

    public Receipt findById(String id) {
        return receiptsDao.findById(id);
    }

    public void closeDatabase() {
        realmDatabase.close();
    }

    public void update(Receipt receipt, String name, String sourceOrCreditor, Double value, boolean isPrefixedValue, Date expectedDate, boolean isMonthlyRecurrent, Date effectiveDate) {
        receiptsDao.update(receipt, name, sourceOrCreditor, value, isPrefixedValue, expectedDate, isMonthlyRecurrent, effectiveDate);
    }

    public void updateEffectiveDate(Receipt receipt, Date effectiveDate) {
        receiptsDao.update(receipt,effectiveDate);
    }

    public void delete(Receipt receiptToDelete) {
        receiptsDao.delete(receiptToDelete);
    }
}
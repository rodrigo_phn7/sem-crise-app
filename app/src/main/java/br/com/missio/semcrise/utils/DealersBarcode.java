package br.com.missio.semcrise.utils;

import java.util.Calendar;

import br.com.missio.semcrise.dao.CreditorDao;
import br.com.missio.semcrise.exceptions.InvalidBarcodeBillValueException;
import br.com.missio.semcrise.exceptions.InvalidIdentificationOfRealValueOrReferenceException;
import br.com.missio.semcrise.exceptions.InvalidLiteralDueDateException;
import br.com.missio.semcrise.exceptions.InvalidSegmentException;
import br.com.missio.semcrise.model.Creditor;
import io.realm.Realm;
import lombok.EqualsAndHashCode;
import lombok.Setter;
import lombok.ToString;

@ToString(exclude = {"parsedBillValue","companyOrAgency","realmDatabase","parsedDueDate"})
@EqualsAndHashCode(callSuper = false, exclude = {"parsedBillValue","companyOrAgency","realmDatabase","parsedDueDate"})
public class DealersBarcode implements BarcodeType {

    @Setter
    private String productIdentification;
    @Setter
    private String segmentIdentification;
    @Setter
    private String identificationOfRealValueOrReference;
    @Setter
    private String checkerDigit;
    @Setter
    private String billValue;
    @Setter
    private String companyOrAgencyIdentification;
    @Setter
    private String cnpj;
    @Setter
    private String freeField;
    @Setter
    private String dueDate;

    private Realm realmDatabase;
    private Creditor companyOrAgency;
    private Double parsedBillValue;
    private Calendar parsedDueDate;

    public DealersBarcode() {}

    public DealersBarcode(String barcode) {
        productIdentification = barcode.substring(0,1);
        segmentIdentification = barcode.substring(1,2);
        identificationOfRealValueOrReference = barcode.substring(2,3);
        checkerDigit = barcode.substring(3,4);
        billValue = barcode.substring(4,15);
        companyOrAgencyIdentification = barcode.substring(15,19);
        freeField = barcode.substring(19,44);
        dueDate = freeField.substring(0,8);
        realmDatabase = Realm.getDefaultInstance();
    }

    public boolean isProductIdentificationRight() {
        return "8".equals(productIdentification);
    }

    public Segment getSegment() throws InvalidSegmentException {
        for (Segment segment : Segment.values()) {
            if(segment.getIdentification() == Integer.parseInt(segmentIdentification)) {
                return segment;
            }
        }
        throw new InvalidSegmentException("Invalid Segment number: " + segmentIdentification);
    }

    public RealValueOrReference getRealValueOrReference() throws InvalidIdentificationOfRealValueOrReferenceException {
        for (RealValueOrReference realValueOrReference : RealValueOrReference.values()) {
            if(realValueOrReference.getIdentification() == Integer.parseInt(identificationOfRealValueOrReference)) {
                return realValueOrReference;
            }
        }
        throw new InvalidIdentificationOfRealValueOrReferenceException
                ("Invalid identification of real value or reference: " + identificationOfRealValueOrReference);
    }

    public Double getBillValue() {
        if(parsedBillValue == null) {
            try {
                parsedBillValue = BarcodeBillValueParser.parseBillValue(billValue);
            } catch (InvalidBarcodeBillValueException e) {
                e.printStackTrace();
            }
        }
        return parsedBillValue;
    }

    public Creditor getCompanyOrAgency() {
        if(companyOrAgency == null) {
            companyOrAgency = new CreditorDao(realmDatabase).getCreditorByCode(companyOrAgencyIdentification);
        }
        return companyOrAgency;
    }

    public Calendar getDueDate() {
        if(parsedDueDate == null) {
            try {
                parsedDueDate = BarcodeDueDateParser.getDueDate(dueDate);
            } catch (InvalidLiteralDueDateException e) {
                e.printStackTrace();
            }
        }
        return parsedDueDate;
    }
}
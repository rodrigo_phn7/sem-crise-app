package br.com.missio.semcrise.presenters.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.text.DecimalFormat;
import java.util.Currency;

import br.com.missio.semcrise.R;
import br.com.missio.semcrise.model.BrazilianDecimalFormat;
import br.com.missio.semcrise.services.ExpensesService;
import br.com.missio.semcrise.services.ReceiptsService;

public class DailyBalanceFragment extends Fragment {

    private View dailyBalanceView;
    private Activity activity;
    private ReceiptsService receiptsService;
    private ExpensesService expensesService;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        dailyBalanceView = inflater.inflate(R.layout.fragment_daily_balance_main, container, false);
        activity = getActivity();
        return dailyBalanceView;
    }

    @Override
    public void onResume() {
        super.onResume();
        setUpDailyBalance();
    }

    private void setUpDailyBalance() {
        receiptsService = new ReceiptsService(activity);
        expensesService = new ExpensesService(activity);
        TextView dailyBalanceTextView = (TextView) dailyBalanceView.findViewById(R.id.daily_balance);
        Double receiptDailyAmount = receiptsService.getDailyTotalAmount();
        Double expenseDailyAmount = expensesService.getDailyTotalAmount();
        Double dailyBalance = receiptDailyAmount - expenseDailyAmount;
        DecimalFormat decimalFormat = BrazilianDecimalFormat.MONEY_REAL;
        dailyBalanceTextView.setText(decimalFormat.format(dailyBalance));
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        receiptsService.closeDatabase();
        expensesService.closeDatabase();
    }
}

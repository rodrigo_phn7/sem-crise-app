package br.com.missio.semcrise.exceptions;

public class InvalidSegmentException extends Exception {
    public InvalidSegmentException(String message) {
        super(message);
    }
}

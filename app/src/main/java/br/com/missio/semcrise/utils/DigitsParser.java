package br.com.missio.semcrise.utils;

public class DigitsParser {

    private final String digits;

    public DigitsParser(String digits) {
        this.digits = digits;
    }

    public int[] getDigitsWithoutCodeChecker(int codeCheckerPosition) {
        int[] barcodeDigits = new int[Constants.BARCODE_SIZE -1];
        int index, position = 0;
        for (char digit : digits.toCharArray()) {
            if(position <= codeCheckerPosition){
                index = position;
            } else {
                index = position - 1;
            }
            barcodeDigits[index] = Integer.parseInt(digit+"");
            position++;
        }
        return barcodeDigits;
    }
}
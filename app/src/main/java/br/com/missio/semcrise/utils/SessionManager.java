package br.com.missio.semcrise.utils;

import android.content.Context;
import android.content.SharedPreferences;

import java.util.HashMap;

import br.com.missio.semcrise.R;
import br.com.missio.semcrise.model.User;

public class SessionManager {

    private static final int PRIVATE_MODE = 0;

    private final SharedPreferences sharedPreferences;

    public SessionManager(Context applicationContext) {
        String appName = applicationContext.getResources().getString(R.string.app_name);
        String appNameWithoutSpace = appName.replaceAll("\\s+","");
        sharedPreferences = applicationContext.getSharedPreferences(appNameWithoutSpace + "_preferences", PRIVATE_MODE);
    }

    public void createLoginSession(HashMap<String, String> userData) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(User.EMAIL_TAG,userData.get(User.EMAIL_TAG));
        editor.putString(User.FIRSTNAME_TAG,userData.get(User.FIRSTNAME_TAG));
        editor.apply();
    }

    public boolean isUserLoggedIn() {
        HashMap<String,String> userData = getUserData();
        return !userData.get(User.EMAIL_TAG).equals("") && !userData.get(User.FIRSTNAME_TAG).equals("");
    }

    public HashMap<String,String> getUserData() {
        HashMap<String,String> userData = new HashMap<>();
        userData.put(User.EMAIL_TAG,sharedPreferences.getString(User.EMAIL_TAG,""));
        userData.put(User.FIRSTNAME_TAG,sharedPreferences.getString(User.FIRSTNAME_TAG,""));
        return userData;
    }

    public void clearSession() {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.clear();
        editor.apply();
    }
}

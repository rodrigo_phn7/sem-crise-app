package br.com.missio.semcrise.presenters.fragments;

import android.app.DatePickerDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.PopupMenu;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.andreabaccega.widget.FormAutoCompleteTextView;
import com.andreabaccega.widget.FormEditText;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

import br.com.missio.semcrise.R;
import br.com.missio.semcrise.exceptions.NullCreditorException;
import br.com.missio.semcrise.model.Expense;
import br.com.missio.semcrise.model.ExpenseGenerator;
import br.com.missio.semcrise.model.User;
import br.com.missio.semcrise.presenters.ViewFinderAdapter;
import br.com.missio.semcrise.presenters.ViewManager;
import br.com.missio.semcrise.presenters.activities.SemCriseActivity;
import br.com.missio.semcrise.services.ExpensesService;
import br.com.missio.semcrise.utils.DateHelper;
import br.com.missio.semcrise.utils.SessionManager;

import static br.com.missio.semcrise.utils.ValidationHelper.validateEachField;

public class AddBarcodeExpenseFragment extends Fragment implements View.OnClickListener, CompoundButton.OnCheckedChangeListener, PopupMenu.OnMenuItemClickListener, DatePickerDialog.OnDateSetListener {
    private SemCriseActivity activity;
    private ExpensesService expensesService;
    private Expense expense;
    private TextView expenseNameTextView;
    private FormEditText expenseNameEditText;
    private ImageButton actionEditNameButton;
    private ViewManager viewManager;
    private CheckBox effectOnThatDateCheckbox;
    private Spinner paymentMethodSpinner;
    private Spinner expenseTypeSpinner;
    private CheckBox isPrefixedValueCheckbox;
    private CheckBox isMonthlyRecurrentCheckbox;
    private Button saveButton;
    private String name;
    private boolean isPrefixedValue;
    private boolean isMonthlyRecurrent;
    private String expenseType;
    private String paymentMethod;
    private Date effectiveDate;
    private Date expectedDate;
    private View view;
    private LinearLayout effectiveDateLinearLayout;
    private TextView eventEffectiveDateTextView;
    private String creditorId;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_add_barcode_expense, container, false);
        activity = (SemCriseActivity) getActivity();
        expensesService = new ExpensesService(activity);
        String barcode = activity.getIntent().getStringExtra(SemCriseActivity.BARCODE);
        try {
            expense = ExpenseGenerator.generateExpenseThrough(barcode);
            setUpInputs();
        } catch (Exception e) {
            e.printStackTrace();
            Toast.makeText(
                    activity,
                    R.string.invalid_barcode_message,
                    Toast.LENGTH_LONG)
                    .show();
            activity.finish();
        }
        return view;
    }

    private void setUpInputs() {
        viewManager = new ViewManager(new ViewFinderAdapter(view),activity,expense);
        setUpExpenseNameTextView();
        setUpMonetaryValueEditText();
        setUpDueDateEditTextOrTextView();
        setUpEffectOnThatDateCheckbox();
        viewManager.setUpCreditorOrSourceTextViewOrEditText(false);
        setUpExpenseTypeSpinner();
        setUpPaymentMethodSpinner();
        isPrefixedValueCheckbox = (CheckBox) view.findViewById(R.id.is_prefixed_value_checkbox);
        isMonthlyRecurrentCheckbox = (CheckBox) view.findViewById(R.id.is_monthly_recurrent_checkbox);
        saveButton = (Button) view.findViewById(R.id.save_button);
        saveButton.setOnClickListener(this);
        if(effectiveDateLinearLayout == null) {
            effectiveDateLinearLayout = (LinearLayout) view.findViewById(R.id.effective_date_linear_layout);
        }
        if(eventEffectiveDateTextView == null) {
            eventEffectiveDateTextView = (TextView) view.findViewById(R.id.effective_date_text_view);
        }
    }

    private void setUpExpenseTypeSpinner() {
        boolean isExpenseTypeSet = expense.getExpenseType() != null;
        expenseTypeSpinner = viewManager.createExpenseTypeSpinner(isExpenseTypeSet);
    }

    private void setUpPaymentMethodSpinner() {
        paymentMethodSpinner = viewManager.createPaymentMethodSpinner(false);
    }

    private void setUpEffectOnThatDateCheckbox() {
        effectOnThatDateCheckbox = (CheckBox) view.findViewById(R.id.effect_on_that_date);
        effectOnThatDateCheckbox.setOnCheckedChangeListener(this);
    }

    private void setUpDueDateEditTextOrTextView() {
        prepareExpectedDate();
        if(expense.getActualDate() != null) {
            viewManager.createActualDateTextView();
        } else {
            viewManager.hideView(R.id.actual_date_text_view);
            viewManager.showView(R.id.expected_date_edit_text);
            viewManager.createExpectedDateEditText();
        }
    }

    private void setUpMonetaryValueEditText() {
        viewManager.createMonetaryValueTextView();
    }

    private void setUpExpenseNameTextView() {
        expenseNameTextView = (TextView) view.findViewById(R.id.expense_name_text_view);
        actionEditNameButton = (ImageButton) view.findViewById(R.id.edit_expense_name_image_button);
        expenseNameEditText = (FormEditText) view.findViewById(R.id.expense_name_edit_text);
        String expenseName = expense.getName();
        if(expenseName != null) {
            actionEditNameButton.setOnClickListener(this);
            expenseNameTextView.setText(expenseName);
        } else {
            expenseNameTextView.setVisibility(View.GONE);
            expenseNameEditText.setVisibility(View.VISIBLE);
            actionEditNameButton.setVisibility(View.GONE);
        }
    }

    @Override
    public void onClick(View v) {
        if(v == actionEditNameButton) {
            expenseNameEditText.setVisibility(View.VISIBLE);
            expenseNameTextView.setVisibility(View.GONE);
            actionEditNameButton.setVisibility(View.GONE);
            expenseNameEditText.setText(expenseNameEditText.getText().toString());
            expenseNameEditText.requestFocus();
        } else if(v == saveButton) {
            boolean formIsValid = validateForm();
            if(formIsValid) {
                try {
                    prepareVariables();
                    save();
                    Toast.makeText(activity, R.string.add_success, Toast.LENGTH_SHORT).show();
                } catch(Exception e) {
                    Toast.makeText(activity, R.string.cant_add_item_error, Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }
                activity.finish();
            }
        }
    }

    private boolean validateForm() {
        ArrayList<FormEditText> fieldsToValidate = new ArrayList<>();
        if(isExpenseNameEdited()) {
            fieldsToValidate.add(expenseNameEditText);
        }
        if(fieldsToValidate.isEmpty()) {
            return true;
        }
        FormEditText[] allFields = new FormEditText[fieldsToValidate.size()];
        int position = 0;
        for (FormEditText field : fieldsToValidate) {
            allFields[position] = field;
            position++;
        }
        return validateEachField(allFields) && creditorTextViewIsValid();
    }

    private boolean creditorTextViewIsValid() {
        FormAutoCompleteTextView[] creditorTextView = {viewManager.getCreditorOrSourceEditText()};
        return !viewManager.isCreditorEdited() || validateEachField(creditorTextView);
    }

    private void prepareVariables() throws NullCreditorException {
        prepareExpenseName();
        prepareCreditor();
        isPrefixedValue = isPrefixedValueCheckbox.isChecked();
        isMonthlyRecurrent = isMonthlyRecurrentCheckbox.isChecked();
        prepareExpenseParameters();
    }

    private void prepareExpectedDate() {
        if(expense.getActualDate() == null) {
            expectedDate = viewManager.getEventDate();
        } else {
            expectedDate = expense.getExpectedDate();
        }
    }

    private void prepareExpenseName() {
        if(isExpenseNameEdited()) {
            name = expenseNameEditText.getText().toString();
        } else {
            name = expense.getName();
        }
    }

    private void prepareCreditor() throws NullCreditorException {
        if(viewManager.isCreditorEdited()) {
            expense.setCreditorId(viewManager.getCreditorId());
        }
    }

    private void prepareExpenseParameters() {
        String expenseTypeValue = expenseTypeSpinner.getSelectedItem().toString();
        Integer expenseTypeIndex = expenseTypeSpinner.getSelectedItemPosition();
        expenseType = expenseTypeValue + SemCriseActivity.HASHTAG_SEPARATOR + expenseTypeIndex;
        String paymentMethodValue = paymentMethodSpinner.getSelectedItem().toString();
        Integer paymentMethodIndex = paymentMethodSpinner.getSelectedItemPosition();
        paymentMethod = paymentMethodValue + SemCriseActivity.HASHTAG_SEPARATOR + paymentMethodIndex;
    }

    private boolean isExpenseNameEdited() {
        return actionEditNameButton.getVisibility() == View.GONE;
    }

    private void save() {
        SessionManager session = new SessionManager(activity.getApplicationContext());
        HashMap<String,String> sessionUserData = session.getUserData();
        String userEmail = sessionUserData.get(User.EMAIL_TAG);
        expense.setName(name);
        expense.setExpectedDate(expectedDate);
        expense.setPrefixedValue(isPrefixedValue);
        expense.setMonthlyRecurrent(isMonthlyRecurrent);
        expense.setExpenseType(expenseType);
        expense.setPaymentMethod(paymentMethod);
        expense.setEffectiveDate(effectiveDate);
        expense.setUserEmail(userEmail);
        expensesService.add(expense);
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        if(buttonView == effectOnThatDateCheckbox) {
            if(isChecked) {
                showPopupMenu(buttonView);
            } else {
                hideDateTextView();
            }
        }
    }

    private void showPopupMenu(CompoundButton buttonView) {
        PopupMenu popup = new PopupMenu(activity, buttonView);
        MenuInflater inflater = popup.getMenuInflater();
        popup.setOnMenuItemClickListener(this);
        inflater.inflate(R.menu.effect_on_that_date_actions_menu, popup.getMenu());
        popup.show();
    }

    private void hideDateTextView() {
        effectiveDate = null;
        effectiveDateLinearLayout.setVisibility(View.GONE);
    }

    @Override
    public boolean onMenuItemClick(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.choose_date:
                showDateDialog();
                return true;
            case R.id.maintain_date:
                effectiveDate = expectedDate;
                showDateTextView(expectedDate);
                return true;
            default:
                return false;
        }
    }

    public void showDateDialog() {
        DatePickerDialogFragment dialogFragment = new DatePickerDialogFragment();
        dialogFragment.setDateSetListener(this);
        dialogFragment.show(activity.getSupportFragmentManager(), DateHelper.DATE_DIALOG_TAG);
    }

    @Override
    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
        effectiveDate = getDate(year,monthOfYear,dayOfMonth);
        showDateTextView(effectiveDate);
    }

    private void showDateTextView(Date eventEffectiveDate) {
        effectiveDateLinearLayout.setVisibility(View.VISIBLE);
        String format = activity.getString(R.string.picker_date_format);
        String formattedDate = DateHelper.formatDate(eventEffectiveDate, format);
        eventEffectiveDateTextView.setText(formattedDate);
    }

    private Date getDate(int year, int monthOfYear, int dayOfMonth) {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.DAY_OF_MONTH,dayOfMonth);
        calendar.set(Calendar.MONTH,monthOfYear);
        calendar.set(Calendar.YEAR,year);
        return calendar.getTime();
    }
}
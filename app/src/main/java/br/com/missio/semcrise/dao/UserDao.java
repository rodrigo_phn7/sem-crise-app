package br.com.missio.semcrise.dao;

import java.util.Date;

import br.com.missio.semcrise.model.User;
import io.realm.Realm;
import io.realm.RealmQuery;
import io.realm.RealmResults;

public class UserDao {

    private Realm realmDatabase;

    public UserDao(Realm realmDatabase) {
        this.realmDatabase = realmDatabase;
    }

    public void add(User newUser) {
        realmDatabase.beginTransaction();
        realmDatabase.copyToRealm(newUser);
        realmDatabase.commitTransaction();
    }

    public User getUserDataBy(String email) {
        return realmDatabase.where(User.class)
                .equalTo("email",email)
                .findFirst();
    }

    public RealmResults<User> findAll(){
        return realmDatabase.where(User.class)
                .findAll();
    }

    public void update(User actualUser) {
        realmDatabase.beginTransaction();
        realmDatabase.copyToRealmOrUpdate(actualUser);
        realmDatabase.commitTransaction();
    }

    public void delete(User userToDelete) {
        realmDatabase.beginTransaction();
        RealmQuery<User> query = realmDatabase.where(User.class);
        query = query.equalTo("email", userToDelete.getEmail());
        User user = query.findFirst();
        user.removeFromRealm();
        realmDatabase.commitTransaction();
    }

    public void deleteAll(){
        realmDatabase.beginTransaction();
        realmDatabase.where(User.class)
                .findAll()
                .clear();
    }

    public void updateUserWith(User user, String cpf, String postalCode, Date birthDate, String profession, String education, Boolean negotiationEnabled) {
        realmDatabase.beginTransaction();
        user.setCpf(cpf);
        user.setPostalCode(postalCode);
        user.setBirthDate(birthDate);
        user.setProfession(profession);
        user.setEducation(education);
        user.setNegotiationEnabled(negotiationEnabled);
        realmDatabase.copyToRealm(user);
        realmDatabase.commitTransaction();
    }

    public void update(User user, String pass) {
        realmDatabase.beginTransaction();
        user.setPass(pass);
        realmDatabase.copyToRealm(user);
        realmDatabase.commitTransaction();
    }
}
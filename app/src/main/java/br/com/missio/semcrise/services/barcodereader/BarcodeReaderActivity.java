package br.com.missio.semcrise.services.barcodereader;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.andreabaccega.widget.FormEditText;

import br.com.missio.semcrise.R;
import br.com.missio.semcrise.exceptions.BarcodeInvalidException;
import br.com.missio.semcrise.exceptions.DigitableLineWrongSizeException;
import br.com.missio.semcrise.presenters.activities.SemCriseActivity;

public class BarcodeReaderActivity extends SemCriseActivity implements TextWatcher {
    private static final int BANK_SLIP_FIRST_FIELD_SIZE = DigitableLineVerifier.BANK_SLIP_FIRST_FIELD_SIZE;
    private static final int BANK_SLIP_SECOND_FIELD_SIZE = DigitableLineVerifier.BANK_SLIP_FIELD_SIZE;
    private static final int BANK_SLIP_THIRD_FIELD_SIZE = DigitableLineVerifier.BANK_SLIP_FIELD_SIZE;
    private static final int DEALER_FIELD_SIZE = DigitableLineVerifier.DEALER_FIELD_SIZE;
    public static final String TAG = "BarcodeReaderActivity";
    private FormEditText barcodeEditText;
    private String digitableLine;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_read_barcode);
        barcodeEditText = (FormEditText) findViewById(R.id.barcode_edit_text);
        if(barcodeEditText != null) {
            barcodeEditText.addTextChangedListener(this);
        } else {
            Log.e(TAG,"BarcodeEditText is null.");
        }
    }

    public void continueBarcodeRead(View view) {
        try {
            String barcode = DigitableLineParser.extractBarcode(digitableLine);
            Intent intent = new Intent();
            intent.putExtra(BarcodeCaptureActivity.BARCODE_VALUE, barcode);
            setResult(BarcodeCaptureActivity.BARCODE_SUCCESS, intent);
            finish();
        } catch (BarcodeInvalidException | DigitableLineWrongSizeException e) {
            e.printStackTrace();
            showMessageError();
        }
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    @Override
    public void afterTextChanged(Editable s) {
        digitableLine = s.toString();
        DigitableLineVerifier digitableLineVerifier = new DigitableLineVerifier(digitableLine);
        if(digitableLineVerifier.isValid()) {
            barcodeEditText.setTextColor(ContextCompat.getColor(this, R.color.textColorPrimary));
            barcodeEditText.setTypeface(null, Typeface.NORMAL);
        } else {
            if(digitableLineSizeFitsToFieldsSize()) {
                showMessageError();
            }
            barcodeEditText.setTextColor(ContextCompat.getColor(this, R.color.red));
            barcodeEditText.setTypeface(null, Typeface.BOLD);
        }
    }

    private boolean digitableLineSizeFitsToFieldsSize() {
        int size = digitableLine.length();
        return size == BANK_SLIP_FIRST_FIELD_SIZE
                || size == BANK_SLIP_FIRST_FIELD_SIZE + BANK_SLIP_SECOND_FIELD_SIZE
                || size == BANK_SLIP_FIRST_FIELD_SIZE + BANK_SLIP_SECOND_FIELD_SIZE + BANK_SLIP_THIRD_FIELD_SIZE
                || size == DEALER_FIELD_SIZE || size == 2*DEALER_FIELD_SIZE
                || size == 3*DEALER_FIELD_SIZE || size == 4*DEALER_FIELD_SIZE;
    }

    private void showMessageError() {
        Toast.makeText(BarcodeReaderActivity.this, R.string.msg_error_invalid_digitable_line,
                Toast.LENGTH_LONG).show();

    }
}
package br.com.missio.semcrise.exceptions;

public class NullCreditorException extends Exception {
    public NullCreditorException(String message) {
        super(message);
    }
}

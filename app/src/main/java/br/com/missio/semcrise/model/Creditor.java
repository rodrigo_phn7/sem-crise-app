package br.com.missio.semcrise.model;

import io.realm.RealmObject;
import io.realm.annotations.Required;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

//TODO: Fazer Autocomplete nas telas.

@ToString
@EqualsAndHashCode(callSuper = false)
public class Creditor extends RealmObject {
    @Setter
    private String id;
    @Required
    @Getter
    @Setter
    private String code;
    @Required
    @Getter
    @Setter
    private String name;
    @Getter
    @Setter
    private String cnpj;
    @Getter
    @Setter
    private String contract;
    @Getter
    @Setter
    private String knownName;
    @Getter
    private String creditorType;

    public String getId() {
        return (id != null) ? id : code;
    }

    public void saveCreditorType(CreditorType creditorType) {
        this.creditorType = creditorType.toString();
    }

    public String getAvailableName() {
        return (knownName != null) ? knownName : name;
    }
}
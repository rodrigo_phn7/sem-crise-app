package br.com.missio.semcrise.services;

import android.content.Context;
import android.content.res.Resources;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import br.com.missio.semcrise.R;
import br.com.missio.semcrise.dao.ExpenseDao;
import br.com.missio.semcrise.model.DateStatus;
import br.com.missio.semcrise.model.Expense;
import br.com.missio.semcrise.model.MonetaryEventsList;
import br.com.missio.semcrise.model.User;
import br.com.missio.semcrise.utils.DateHelper;
import br.com.missio.semcrise.utils.SessionManager;
import io.realm.Realm;

public class ExpensesService {

    private List<Calendar> holidays;
    private Realm realmDatabase;
    private String userEmail;
    private ExpenseDao expensesDao;

    public ExpensesService(Context context){
        realmDatabase = Realm.getDefaultInstance();
        expensesDao = new ExpenseDao(realmDatabase);
        SessionManager session = new SessionManager(context);
        if(session.isUserLoggedIn()) {
            userEmail = session.getUserData().get(User.EMAIL_TAG);
        }
        Resources res = context.getResources();
        String[] holidaysStringArray = res.getStringArray(R.array.holidays);
        holidays = DateHelper.formatDates(holidaysStringArray);
    }

    public void addExpenseWith(String name, String sourceOrCreditor, Double value, Boolean isPrefixedValue, Date expectedDate, Boolean isMonthlyRecurrent, String paymentMethod, String expenseType, String userEmail, Date effectiveDate) {
        expectedDate = adjustDate(expectedDate);
        Expense expense = new Expense();
        expense.setName(name);
        expense.setCreditorId(sourceOrCreditor);
        expense.setValue(value);
        expense.setPrefixedValue(isPrefixedValue);
        expense.setExpectedDate(expectedDate);
        expense.setMonthlyRecurrent(isMonthlyRecurrent);
        expense.setPaymentMethod(paymentMethod);
        expense.setExpenseType(expenseType);
        expense.setUserEmail(userEmail);
        effectiveDate = adjustEffectiveDate(effectiveDate);
        expense.setEffectiveDate(effectiveDate);
        expensesDao.add(expense);
    }

    private Date adjustDate(Date date) {
        if(holidays != null) {
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(date);
            DateHelper.adjustTimeForWorkingDate(calendar,holidays);
            date = calendar.getTime();
        }
        return date;
    }

    private Date adjustEffectiveDate(Date effectiveDate) {
        if(effectiveDate != null) {
            effectiveDate = adjustDate(effectiveDate);
        }
        return effectiveDate;
    }

    public MonetaryEventsList getDailyExpenses() {
        MonetaryEventsList expenses = expensesDao.findByEmail(userEmail);
        return expenses.getDailyEffectiveEvents();
    }

    public MonetaryEventsList getLateExpenses() {
        MonetaryEventsList expenses = expensesDao.findByEmail(userEmail);
        return expenses.getLateEvents();
    }

    public MonetaryEventsList getAllExpenses() {
        return expensesDao.findByEmail(userEmail);
    }

    public MonetaryEventsList getAllEventsIn(int period, DateStatus dateStatus) {
        MonetaryEventsList expenses = expensesDao.findByEmail(userEmail);
        return expenses.getAllEventsIn(period, dateStatus);
    }

    public Double getMonthlyAmountUntilNow() {
        MonetaryEventsList allExpenses = expensesDao.findByEmail(userEmail);
        return allExpenses.getMonthlyEventsUntilNow().getTotalAmount();
    }

    public Double getDailyTotalAmount() {
        MonetaryEventsList allExpenses = expensesDao.findByEmail(userEmail);
        return allExpenses.getDailyEffectiveEvents().getTotalAmount();
    }

    public Expense findById(String id) {
        return expensesDao.findById(id);
    }

    public void closeDatabase() {
        realmDatabase.close();
    }

    public MonetaryEventsList getDailyNonEffectiveExpenses() {
        MonetaryEventsList allExpenses = expensesDao.findByEmail(userEmail);
        return allExpenses.getDailyNonEffectiveEvents();
    }

    public void update(Expense oldExpense, String name, String sourceOrCreditor, Double value, boolean isPrefixedValue, Date expectedDate, boolean isMonthlyRecurrent, String paymentMethod, String expenseType, Date effectiveDate) {
        expectedDate = adjustDate(expectedDate);
        effectiveDate = adjustEffectiveDate(effectiveDate);
        expensesDao.update(oldExpense, name, sourceOrCreditor, value, isPrefixedValue, expectedDate, isMonthlyRecurrent, paymentMethod, expenseType,effectiveDate);
    }

    public void updateExpenseNegotiationStatus(Expense expense, boolean negotiationActive) {
        expensesDao.update(expense,negotiationActive);
    }

    public void updateEffectiveDate(Expense expense, Date effectiveDate) {
        effectiveDate = adjustEffectiveDate(effectiveDate);
        expensesDao.update(expense,effectiveDate);
    }

    public void delete(Expense expenseToDelete) {
        expensesDao.delete(expenseToDelete);
    }

    public void add(Expense expense) {
        expense.setExpectedDate(adjustDate(expense.getExpectedDate()));
        expense.setEffectiveDate(adjustEffectiveDate(expense.getEffectiveDate()));
        expensesDao.add(expense);
    }
}
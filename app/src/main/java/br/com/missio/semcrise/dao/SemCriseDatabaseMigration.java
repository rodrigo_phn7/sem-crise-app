package br.com.missio.semcrise.dao;

import br.com.missio.semcrise.model.CreditorType;
import io.realm.DynamicRealm;
import io.realm.DynamicRealmObject;
import io.realm.FieldAttribute;
import io.realm.RealmMigration;
import io.realm.RealmObjectSchema;
import io.realm.RealmSchema;
import io.realm.annotations.Required;

public class SemCriseDatabaseMigration implements RealmMigration {
    @Override
    public void migrate(DynamicRealm realm, long oldVersion, long newVersion) {
        RealmSchema schema = realm.getSchema();

        if(oldVersion == 0) {
            schema.get("ExpenseEventData")
            .renameField("source","creditor");
            oldVersion++;
        }

        if(oldVersion == 1) {
            schema.get("ExpenseEventData")
                    .renameField("creditor","creditorTest");
            oldVersion++;
        }

        if(oldVersion == 2) {
            schema.get("ExpenseEventData")
                    .renameField("creditorTest","creditor");
            oldVersion++;
        }

        if(oldVersion == 3) {
            schema.rename("ExpenseEventData","Expense");

            RealmObjectSchema expenseSchema = schema.get("Expense");
            expenseSchema
                .addField("monthlyRecurrent",boolean.class)
                .addField("prefixedValue",boolean.class)
                .addField("tempNegotiationActive",boolean.class)
                .addField("tempNegotiationDone",boolean.class)

                .setNullable("negotiationActive",false)
                .setNullable("negotiationDone",false)

                .setRequired("negotiationActive",false)
                .setRequired("negotiationDone",false)

                .transform(new RealmObjectSchema.Function() {
                    @Override
                    public void apply(DynamicRealmObject obj) {
                        obj.set("monthlyRecurrent", obj.getBoolean("isMonthlyRecurrent"));
                        obj.set("prefixedValue", obj.getBoolean("isPrefixedValue"));
                        obj.set("tempNegotiationActive",obj.getBoolean("negotiationActive"));
                        obj.set("tempNegotiationDone",obj.getBoolean("negotiationDone"));
                    }
                })

                .removeField("isMonthlyRecurrent")
                .removeField("isPrefixedValue")
                .removeField("negotiationActive")
                .removeField("negotiationDone")

                .renameField("tempNegotiationActive","negotiationActive")
                .renameField("tempNegotiationDone","negotiationDone");

            oldVersion++;
        }

        if(oldVersion == 4) {
            schema.rename("ReceiptEventData","Receipt");

            RealmObjectSchema receiptSchema = schema.get("Receipt");
            receiptSchema
                .addField("monthlyRecurrent",boolean.class)
                .addField("prefixedValue",boolean.class)

                .transform(new RealmObjectSchema.Function() {
                    @Override
                    public void apply(DynamicRealmObject obj) {
                        obj.set("monthlyRecurrent", obj.getBoolean("isMonthlyRecurrent"));
                        obj.set("prefixedValue", obj.getBoolean("isPrefixedValue"));
                    }
                })

                .removeField("isMonthlyRecurrent")
                .removeField("isPrefixedValue");
            oldVersion++;
        }

        if(oldVersion == 5) {
            schema.rename("UserData","User");

            RealmObjectSchema userSchema = schema.get("User");
            userSchema
                .addField("tempNegotiationEnabled",boolean.class)

                .setNullable("negotiationEnabled",false)

                .transform(new RealmObjectSchema.Function() {
                    @Override
                    public void apply(DynamicRealmObject obj) {
                        obj.set("tempNegotiationEnabled", obj.getBoolean("negotiationEnabled"));
                    }
                })

                .removeField("negotiationEnabled")

                .renameField("tempNegotiationEnabled","negotiationEnabled");
            oldVersion++;
        }

        if(oldVersion == 6) {
            RealmObjectSchema bankSchema = schema.create("Bank");
                    bankSchema
                    .addField("code", String.class, FieldAttribute.REQUIRED)
                    .addField("name", String.class, FieldAttribute.REQUIRED)
                    .addField("cnpj", String.class)
                    .addField("contract", String.class)
                    .addField("knownName", String.class);

            RealmObjectSchema receiptSchema = schema.get("Receipt");
            receiptSchema.addField("currency",String.class);

            RealmObjectSchema expenseSchema = schema.get("Expense");
            expenseSchema
                    .addField("currency",String.class)
                    .addField("creditorId",String.class,FieldAttribute.REQUIRED)

                    .transform(new RealmObjectSchema.Function() {
                        @Override
                        public void apply(DynamicRealmObject obj) {
                            obj.set("creditorId",obj.getString("creditor"));
                        }
                    })

                    .removeField("creditor");
            oldVersion++;
        }

        if(oldVersion == 7) {
            RealmObjectSchema expenseSchema = schema.get("Expense");
            expenseSchema
                    .addField("creditorTypedName",String.class)
                    .transform(new RealmObjectSchema.Function() {
                        @Override
                        public void apply(DynamicRealmObject obj) {
                            obj.set("creditorTypedName",obj.getString("creditorId"));
                            obj.set("creditorId","");
                        }
                    });
            oldVersion++;
        }

        if(oldVersion == 8) {
            schema.rename("Bank","Creditor");

            RealmObjectSchema creditorSchema = schema.get("Creditor");
            creditorSchema
                    .addField("creditorType",String.class)
                    .transform(new RealmObjectSchema.Function() {
                        @Override
                        public void apply(DynamicRealmObject obj) {
                            obj.set("creditorType", CreditorType.BANK.toString());
                        }
                    });
            oldVersion++;
        }

        if(oldVersion == 9) {
            RealmObjectSchema creditorSchema = schema.get("Creditor");
            creditorSchema
                    .addField("id",String.class);
            oldVersion++;
        }

        if(oldVersion == 10) {
            RealmObjectSchema expenseSchema = schema.get("Expense");
            expenseSchema.removeField("creditorTypedName");
            oldVersion++;
        }

        if(oldVersion == 11) {
            RealmObjectSchema expenseSchema = schema.get("Expense");
            expenseSchema.removeField("currency");

            RealmObjectSchema receiptSchema = schema.get("Receipt");
            receiptSchema.removeField("currency");

            oldVersion++;
        }
    }
}
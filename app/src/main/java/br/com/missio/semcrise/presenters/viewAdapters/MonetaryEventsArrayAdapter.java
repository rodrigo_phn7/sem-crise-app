package br.com.missio.semcrise.presenters.viewAdapters;

import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import br.com.missio.semcrise.R;
import br.com.missio.semcrise.model.MonetaryEvent;
import br.com.missio.semcrise.presenters.activities.SemCriseActivity;

public class MonetaryEventsArrayAdapter extends ArrayAdapter<MonetaryEvent> {

    private static final int MAX_ELEMENTS = 3;
    private final SemCriseActivity context;
    private final List<MonetaryEvent> monetaryEvents;

    public MonetaryEventsArrayAdapter(SemCriseActivity context, List<MonetaryEvent> monetaryEvents) {
        super(context, R.layout.big_events_list, monetaryEvents);
        this.context = context;
        this.monetaryEvents = monetaryEvents;
    }

    static class ViewHolder {
        protected LinearLayout bigEventsListLinearLayout;
        protected TextView nameItemText;
        protected TextView statusItemText;
        protected TextView valueItemText;
        protected TextView dateItemText;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view;
        if(convertView == null) {
            LayoutInflater inflator = context.getLayoutInflater();
            view = inflator.inflate(R.layout.big_events_list, null);
            final ViewHolder viewHolder = new ViewHolder();
            viewHolder.bigEventsListLinearLayout = (LinearLayout) view.findViewById(R.id.big_events_list_linear_layout);
            viewHolder.nameItemText = (TextView) view.findViewById(R.id.event_name_item);
            viewHolder.statusItemText = (TextView) view.findViewById(R.id.event_status_item);
            viewHolder.valueItemText = (TextView) view.findViewById(R.id.event_value_item);
            viewHolder.dateItemText = (TextView) view.findViewById(R.id.event_date_item);
            view.setTag(viewHolder);
        } else {
            view = convertView;
        }
        ViewHolder holder = (ViewHolder) view.getTag();
        MonetaryEvent event = monetaryEvents.get(position);
        holder.nameItemText.setText(event.getName());
        holder.statusItemText.setText(event.getEventStatus());
        holder.valueItemText.setText(event.getMoneyValue().toString());
        holder.dateItemText.setText(event.getFormattedActualDate(context));
        if(thisFragmentShowsMainDailyEvents()) {
            if(MonetaryEvent.EXPENSE.equals(event.getEventType())) {
                holder.bigEventsListLinearLayout.setBackgroundColor(view.getResources().getColor(R.color.paymentsBackground));
            } else {
                holder.bigEventsListLinearLayout.setBackgroundColor(view.getResources().getColor(R.color.receivablesBackground));
            }
        }
        return view;
    }

    private boolean thisFragmentShowsMainDailyEvents() {
        if(context != null) {
            Fragment fragment = context.getSupportFragmentManager().findFragmentById(R.id.container_body);
            if(fragment != null) {
                String action = fragment.getArguments().getString(SemCriseActivity.EVENT_ACTION);
                return SemCriseActivity.SHOW_MAIN_DAILY_EVENTS.equals(action);
            }
        }
        return false;
    }

    @Override
    public int getCount() {
        if(monetaryEvents.size() >= MAX_ELEMENTS && thisFragmentShowsMainDailyEvents()){
            return MAX_ELEMENTS;
        }
        else {
            return super.getCount();
        }
    }
}

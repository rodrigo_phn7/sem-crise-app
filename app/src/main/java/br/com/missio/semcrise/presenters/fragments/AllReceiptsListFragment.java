package br.com.missio.semcrise.presenters.fragments;

import android.widget.ArrayAdapter;
import br.com.missio.semcrise.model.MonetaryEvent;
import br.com.missio.semcrise.model.MonetaryEventsList;
import br.com.missio.semcrise.services.ReceiptsService;
import br.com.missio.semcrise.presenters.viewAdapters.MonetaryEventsArrayAdapter;

public class AllReceiptsListFragment extends SemCriseListFragment {

    @Override
    public void onResume() {
        super.onResume();
        fillList();
    }

    private void fillList() {
        ReceiptsService receiptsService = new ReceiptsService(getSemCriseActivity().getApplicationContext());
        MonetaryEventsList receipts = receiptsService.getAllReceipts();
        setEvents(receipts);
        setReceiptsService(receiptsService);
        ArrayAdapter<MonetaryEvent> adapter = new MonetaryEventsArrayAdapter(getSemCriseActivity(), receipts);
        setListAdapter(adapter);
    }
}

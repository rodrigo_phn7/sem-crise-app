package br.com.missio.semcrise.utils;

public class BarcodeValidator {
    public static boolean validate(String barcode) {
        try {
            return !BarcodeType.NULL.equals(new BarcodeChecker(barcode).getBarcodeType());
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }
}

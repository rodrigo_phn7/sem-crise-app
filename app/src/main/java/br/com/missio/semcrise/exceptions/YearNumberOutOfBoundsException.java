package br.com.missio.semcrise.exceptions;

public class YearNumberOutOfBoundsException extends Exception {
    public YearNumberOutOfBoundsException(String message) {
        super(message);
    }
}
package br.com.missio.semcrise.exceptions;

public class BarcodeIsNotDealersException extends Exception {
    public BarcodeIsNotDealersException(String message) {
        super(message);
    }
}
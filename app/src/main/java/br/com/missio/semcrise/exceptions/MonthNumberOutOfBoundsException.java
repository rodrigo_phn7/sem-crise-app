package br.com.missio.semcrise.exceptions;

public class MonthNumberOutOfBoundsException extends Exception {
    public MonthNumberOutOfBoundsException(String message) {
        super(message);
    }
}
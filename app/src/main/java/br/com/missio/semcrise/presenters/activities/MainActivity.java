package br.com.missio.semcrise.presenters.activities;

import android.os.Build;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.Toolbar;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;


import java.util.Timer;
import java.util.TimerTask;

import br.com.missio.semcrise.R;
import br.com.missio.semcrise.model.MonetaryEvent;
import br.com.missio.semcrise.presenters.fragments.FragmentDrawer;
import br.com.missio.semcrise.presenters.fragments.FullStatementFragment;
import br.com.missio.semcrise.presenters.fragments.HelpFragment;
import br.com.missio.semcrise.presenters.fragments.MainFragment;
import br.com.missio.semcrise.presenters.fragments.ShowEventsFragment;
import br.com.missio.semcrise.services.barcodereader.BarcodeReaderActivity;
import br.com.missio.semcrise.utils.Constants;
import br.com.missio.semcrise.services.barcodereader.BarcodeCaptureActivity;

import static br.com.missio.semcrise.presenters.fragments.FragmentDrawer.*;

public class MainActivity extends SemCriseActivity implements FragmentDrawerListener, PopupMenu.OnMenuItemClickListener {

    private static final int MAIN_POSITION = 0;
    private static final int SHOW_PROFILE_POSITION = 1;
    private static final int SHOW_EXPENSES_POSITION = 2;
    private static final int SHOW_RECEIPTS_POSITION = 3;
    private static final int SHOW_SETTINGS_POSITION = 4;
    private static final int HELP_POSITION = 5;
    private static final int EXIT_POSITION = 6;
    private static final int SHOW_LATE_EXPENSES_POSITION = 99;
    private static final int SHOW_NON_EFFECTIVE_DAILY_EXPENSES_POSITION = 100;
    private static final int SHOW_FULL_STATEMENT_POSITION = 101;
    private static final int SHOW_MAIN_DAILY_EVENTS_POSITION = 102;
    private static final int RC_BARCODE_CAPTURE = 9001;
    private Fragment fragment;
    private int position = 100 * 999;
    private Intent intent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        assert getSupportActionBar() != null;
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        showTips();

        FragmentDrawer drawerFragment = (FragmentDrawer)
                getSupportFragmentManager().findFragmentById(R.id.fragment_navigation_drawer);
        drawerFragment.setUp(R.id.fragment_navigation_drawer, (DrawerLayout) findViewById(R.id.drawer_layout), toolbar);
        drawerFragment.setDrawerListener(this);

        intent = getIntent();

        String action = intent.getAction();
        if(SHOW_LATE_EXPENSES_LIST.equals(action)) {
            displayView(SHOW_LATE_EXPENSES_POSITION,true);
        } else if(SHOW_NON_EFFECTIVE_DAILY_EXPENSES.equals(action)) {
            displayView(SHOW_NON_EFFECTIVE_DAILY_EXPENSES_POSITION,true);
        } else {
            displayView(MAIN_POSITION, true);
        }
    }

    private void showTips() {
        Timer timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                showPresentationAppDialog();
            }
        }, Constants.ONE_SECOND);
    }

    private void showPresentationAppDialog() {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        boolean showPresentation = sharedPreferences.getBoolean(getString(R.string.show_presentation), true);
        if (showPresentation) {
            goTo(PresentationAppDialogActivity.class);
        }
    }

    private void displayView(int position, boolean firstRun) {
        if (!fragmentAlreadyShown(position)) {
            fragment = null;
            this.position = position;
            String title = getString(R.string.app_name);
            switch (position) {
                case MAIN_POSITION:
                    fragment = new MainFragment();
                    title = getString(R.string.title_sem_crise_app);
                    break;
                case SHOW_EXPENSES_POSITION:
                    fragment = new ShowEventsFragment();
                    intent.putExtra(EVENT_ACTION, SHOW_EXPENSES);
                    title = getString(R.string.title_activity_all_expenses);
                    break;
                case SHOW_RECEIPTS_POSITION:
                    fragment = new ShowEventsFragment();
                    intent.putExtra(EVENT_ACTION, SHOW_RECEIPTS);
                    title = getString(R.string.title_activity_all_receipts);
                    break;
                case SHOW_LATE_EXPENSES_POSITION:
                    fragment = new ShowEventsFragment();
                    setEventType(MonetaryEvent.EXPENSE);
                    intent.putExtra(EVENT_ACTION,SHOW_LATE_EXPENSES_LIST);
                    title = getString(R.string.title_activity_late_expenses);
                    break;
                case SHOW_NON_EFFECTIVE_DAILY_EXPENSES_POSITION:
                    fragment = new ShowEventsFragment();
                    setEventType(MonetaryEvent.EXPENSE);
                    intent.putExtra(EVENT_ACTION,SHOW_NON_EFFECTIVE_DAILY_EXPENSES);
                    title = getString(R.string.title_activity_non_effective_daily_expenses);
                    break;
                case SHOW_FULL_STATEMENT_POSITION:
                    fragment = new FullStatementFragment();
                    intent.putExtra(EVENT_ACTION, SHOW_FULL_STATEMENT);
                    title = getString(R.string.title_activity_full_statement);
                    break;
                case SHOW_MAIN_DAILY_EVENTS_POSITION:
                    fragment = new ShowEventsFragment();
                    intent.putExtra(EVENT_ACTION, SHOW_MAIN_DAILY_EVENTS);
                    title = getString(R.string.today);
                    break;
                case SHOW_SETTINGS_POSITION:
                    showSettings();
                    break;
                case SHOW_PROFILE_POSITION:
                    showProfile();
                    break;
                case HELP_POSITION:
                    fragment = new HelpFragment();
                    title = getString(R.string.help);
                case EXIT_POSITION:
                    exitApp();
                    break;
                default:
                    break;
            }
            if (fragment != null) {
                showFragment(firstRun, intent, title);
            }
        }
    }

    private boolean fragmentAlreadyShown(int position) {
        return this.position == position;
    }

    public void showCompleteRegisterScreen(View view) {
        goTo(CompleteRegisterActivity.class);
    }

    public void showDailyEventsScreen(View view) {
        displayView(SHOW_MAIN_DAILY_EVENTS_POSITION,false);
    }

    public void showLateExpensesScreen(View view) {
        displayView(SHOW_LATE_EXPENSES_POSITION,false);
    }

    public void showFullStatementScreen(View view) {
        displayView(SHOW_FULL_STATEMENT_POSITION,false);
    }

    private void showFragment(boolean firstRun, Intent intent, String title) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        intent.putExtra(APP_BAR_TITLE, title);
        fragment.setArguments(intent.getExtras());
        fragmentTransaction.replace(R.id.container_body, fragment);
        if (!firstRun) {
            fragmentTransaction.addToBackStack(null);
        }
        fragmentTransaction.commit();
    }

    private void showProfile() {
        Toast.makeText(this, "Soon... You profile.", Toast.LENGTH_SHORT).show();
    }

    private void showSettings() {
        goTo(SettingsActivity.class);
    }

    private void exitApp() {
        finish();
    }

    @Override
    public boolean onMenuItemClick(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.via_form_item:
                goToAddEventScreen(MonetaryEvent.EXPENSE);
                return true;
            case R.id.via_barcode_item:
                readBarcode();
                return true;
            case R.id.add_receipt:
                goToAddEventScreen(MonetaryEvent.RECEIPT);
                return true;
            default:
                return false;
        }
    }

    private void goToAddEventScreen(String eventType) {
        Intent intent = new Intent(this, AddOrUpdateEventActivity.class);
        intent.setAction(ADD_EVENT);
        intent.putExtra(EVENT_TYPE_TAG, eventType);
        startActivity(intent);
    }

    @Override
    public void onDrawerItemSelected(View view, int position) {
        displayView(position, false);
    }

    public void showAddChoicePopupMenu(View view) {
        PopupMenu popup = new PopupMenu(this, view);
        popup.setOnMenuItemClickListener(this);
        MenuInflater inflater = popup.getMenuInflater();
        inflater.inflate(R.menu.add_actions_menu, popup.getMenu());
        popup.show();
    }

    public void reloadFragment() {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.container_body, fragment);
        fragmentTransaction.commit();
    }

    public void resetPositionReference() {
        this.position = 100 * 999;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        resetPositionReference();
    }

    public void readBarcode() {
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2) {
            Intent intent = new Intent(this, BarcodeCaptureActivity.class);
            startActivityForResult(intent, RC_BARCODE_CAPTURE);
        } else {
            Intent intent = new Intent(this, BarcodeReaderActivity.class);
            startActivityForResult(intent, RC_BARCODE_CAPTURE);
            Toast.makeText(MainActivity.this, R.string.your_device_cant_read_barcode_through_camera, Toast.LENGTH_LONG).show();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(resultCode == BarcodeCaptureActivity.BARCODE_SUCCESS) {
            String barcode = data.getStringExtra(BarcodeCaptureActivity.BARCODE_VALUE);
            addExpenseThroughBarcode(barcode);
        }
    }

    private void addExpenseThroughBarcode(String barcode) {
        Intent intent = new Intent(this, AddOrUpdateEventActivity.class);
        intent.setAction(ADD_EXPENSE_THROUGH_READ_BARCODE);
        intent.putExtra(EVENT_TYPE_TAG,MonetaryEvent.EXPENSE);
        intent.putExtra(BARCODE,barcode);
        startActivity(intent);
    }
}
package br.com.missio.semcrise.services;

import android.content.Context;

import java.io.IOException;
import java.io.InputStream;
import java.util.UUID;

import br.com.missio.semcrise.SemCriseApplication;
import br.com.missio.semcrise.dao.CreditorDao;
import br.com.missio.semcrise.model.Creditor;
import br.com.missio.semcrise.model.CreditorType;
import io.realm.Realm;
import io.realm.RealmResults;

public class CreditorsService {

    private Realm realmDatabase;
    private Context context;
    private CreditorDao creditorDao;
    private Creditor creditor;

    public CreditorsService() {
        realmDatabase = Realm.getDefaultInstance();
        creditorDao = new CreditorDao(realmDatabase);
        context = SemCriseApplication.getContext();
    }


    public void loadCreditors() throws IOException {
        if(creditorDao.thereIsNotCreditorRegisteredFor(CreditorType.BANK)) {
            load("banks.json");
        }
        if(creditorDao.thereIsNotCreditorRegisteredFor(CreditorType.COMPANY)) {
            load("company_dealers.json");
        }
        if(creditorDao.thereIsNotCreditorRegisteredFor(CreditorType.PUBLIC_AGENCY)) {
            load("public_agency_dealers.json");
        }
    }

    private void load(String jsonFile) throws IOException {
        InputStream creditorsStream = context.getAssets().open(jsonFile);
        realmDatabase.beginTransaction();
        try {
            realmDatabase.createAllFromJson(Creditor.class,creditorsStream);
            realmDatabase.commitTransaction();
        } catch (IOException e) {
            e.printStackTrace();
            realmDatabase.cancelTransaction();
        } finally {
            if(creditorsStream != null) {
                creditorsStream.close();
            }
        }
    }

    public Creditor getCreditorById(String creditorId) {
        if(creditor == null) {
            creditor = creditorDao.getCreditorByCode(creditorId);
        }
        return creditor;
    }

    public void closeDatabase() {
        realmDatabase.close();
    }

    public RealmResults<Creditor> findAll() {
        return realmDatabase.where(Creditor.class).findAll();
    }

    public String createNewCreditorAndReturnId(String creditorName) {
        String id = UUID.randomUUID().toString();
        Creditor creditor = new Creditor();
        creditor.setId(id);
        creditor.setCode(id);
        creditor.setName(creditorName);
        try {
            realmDatabase.beginTransaction();
            realmDatabase.copyToRealm(creditor);
        } catch (Exception e) {
            e.printStackTrace();
            realmDatabase.cancelTransaction();
            throw e;
        } finally {
            realmDatabase.commitTransaction();
        }
        return id;
    }
}
package br.com.missio.semcrise.presenters.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.AppCompatTextView;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.Toast;

import com.andreabaccega.widget.FormEditText;

import br.com.missio.semcrise.R;
import br.com.missio.semcrise.model.Expense;
import br.com.missio.semcrise.model.MonetaryEvent;
import br.com.missio.semcrise.model.User;
import br.com.missio.semcrise.services.UserService;
import br.com.missio.semcrise.utils.PasswordNumberValidator;
import br.com.missio.semcrise.utils.ValidationHelper;

public class LoginActivity extends SemCriseActivity {

    private FormEditText mPasswordView;
    private User user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        setUpUser();
        setUpViewAndInputs();
    }

    private void setUpUser() {
        user = getUser(this);
    }

    private void setUpViewAndInputs() {
        mPasswordView = (FormEditText) findViewById(R.id.login_pass);
        mPasswordView.addValidator(new PasswordNumberValidator(getString(R.string.error_minimum_digits)));

        AppCompatTextView welcomeView = (AppCompatTextView) findViewById(R.id.text_view_welcome);
        welcomeView.setText(welcomeText());

        Button mEmailSignInButton = (Button) findViewById(R.id.email_sign_in_button);
        mEmailSignInButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                    attemptLogin();
            }
        });
    }

    private String welcomeText() {
        return getText(R.string.welcome) + ", " + user.getFirstName();
    }

    private void attemptLogin() {
        boolean formIsValid = validateForm();
        if(formIsValid) {
            String password = mPasswordView.getText().toString();
            boolean loginSuccessful = new UserService(this).login(user.getEmail(), password);
            if (loginSuccessful) {
                directAction();
                finish();
            } else {
                Toast.makeText(getApplicationContext(),R.string.cannot_login, Toast.LENGTH_SHORT).show();
            }
        }
    }

    private boolean validateForm() {
        FormEditText[] allFields = {mPasswordView};
        return ValidationHelper.validateEachField(allFields);
    }

    private void directAction() {
        Intent loginIntent = getIntent();
        String action = loginIntent.getAction();
        if(action != null) {
            if(action.startsWith(Expense.EXPENSE_ID_TAG)) {
                Intent intent = new Intent(this,ShowEventActivity.class);
                String expenseID = TextUtils.split(action,SemCriseActivity.HASHTAG_SEPARATOR)[1];
                intent.setAction(expenseID);
                intent.putExtra(SemCriseActivity.EVENT_TYPE_TAG, MonetaryEvent.EXPENSE);
                intent.putExtra(ESPECIAL_EXPENSE_TYPE,loginIntent.getStringExtra(ESPECIAL_EXPENSE_TYPE));
                startActivity(intent);
            } else {
                Intent intent = new Intent(this,MainActivity.class);
                intent.setAction(action);
                startActivity(intent);
            }
        } else {
            goTo(MainActivity.class);
        }
    }

    public void showResetPasswordScreen(View view) {
        goTo(ResetPasswordActivity.class);
    }
}
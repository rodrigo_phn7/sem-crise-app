package br.com.missio.semcrise.dao;

import android.util.Log;

import java.util.Calendar;
import java.util.Date;
import java.util.UUID;

import br.com.missio.semcrise.model.Expense;
import br.com.missio.semcrise.model.MonetaryEventsList;
import io.realm.Realm;
import io.realm.RealmResults;

public class ExpenseDao {

    private Realm realmDatabase;

    public ExpenseDao(Realm realmDatabase){
        this.realmDatabase = realmDatabase;
    }
    public void add(Expense newExpense) {
        try {
            newExpense.setId(UUID.randomUUID().toString());
            newExpense.setNegotiationActive(false);
            newExpense.setNegotiationDone(false);
            newExpense.setDateItWasAdded(Calendar.getInstance().getTime());
            realmDatabase.beginTransaction();
            realmDatabase.copyToRealm(newExpense);
        } catch (Exception e) {
            e.printStackTrace();
            realmDatabase.cancelTransaction();
            throw e;
        } finally {
            realmDatabase.commitTransaction();
        }

    }

    public void update(Expense expense, boolean negotiationActive) {
        realmDatabase.beginTransaction();
        expense.setNegotiationActive(negotiationActive);
        realmDatabase.copyToRealm(expense);
        realmDatabase.commitTransaction();
    }

    public void update(Expense expense, Date effectiveDate) {
        realmDatabase.beginTransaction();
        expense.setEffectiveDate(effectiveDate);
        realmDatabase.copyToRealm(expense);
        realmDatabase.commitTransaction();
    }

    public MonetaryEventsList findByEmail(String email) {
        RealmResults<Expense> expensesResults = realmDatabase.where(Expense.class).equalTo("userEmail",email).findAll();
        MonetaryEventsList expenses = new MonetaryEventsList();
        for (Expense result : expensesResults) {
            expenses.add(result);
        }
        return expenses;
    }

    public Expense findById(String id) {
        return realmDatabase.where(Expense.class).equalTo("id",id).findFirst();
    }

    public void delete(Expense expenseToDelete){
        realmDatabase.beginTransaction();
        try {
            expenseToDelete.removeFromRealm();
        } catch (Exception e) {
            e.printStackTrace();
            Log.i("TryDelete", e.getMessage());
        }
        realmDatabase.commitTransaction();
    }

    public void update(Expense oldExpense, String name, String sourceOrCreditor, Double value, boolean prefixedValue, Date expectedDate, boolean monthlyRecurrent, String paymentMethod, String expenseType, Date effectiveDate) {
        realmDatabase.beginTransaction();
        oldExpense.setName(name);
        oldExpense.setCreditorId(sourceOrCreditor);
        oldExpense.setValue(value);
        oldExpense.setPrefixedValue(prefixedValue);
        oldExpense.setExpectedDate(expectedDate);
        oldExpense.setMonthlyRecurrent(monthlyRecurrent);
        oldExpense.setPaymentMethod(paymentMethod);
        oldExpense.setExpenseType(expenseType);
        oldExpense.setEffectiveDate(effectiveDate);
        realmDatabase.copyToRealm(oldExpense);
        realmDatabase.commitTransaction();
    }
}

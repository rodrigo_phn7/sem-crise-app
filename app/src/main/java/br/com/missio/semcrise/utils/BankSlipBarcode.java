package br.com.missio.semcrise.utils;

import java.util.Calendar;

import br.com.missio.semcrise.dao.CreditorDao;
import br.com.missio.semcrise.exceptions.InvalidBarcodeBillValueException;
import br.com.missio.semcrise.model.Creditor;
import io.realm.Realm;
import lombok.EqualsAndHashCode;
import lombok.Setter;
import lombok.ToString;

@ToString(exclude = {"realmDatabase","creditor","dueDate","parsedBillValue"})
@EqualsAndHashCode(callSuper = false,exclude = {"realmDatabase","creditor","dueDate","parsedBillValue"})
public class BankSlipBarcode implements BarcodeType {
    @Setter
    private String bankCode;
    @Setter
    private String codeChecker;
    @Setter
    private String dueDateFactor;
    @Setter
    private String billValue;
    @Setter
    private String freeField;

    private Realm realmDatabase;
    private Creditor creditor;
    private Calendar dueDate;
    private Double parsedBillValue;

    public BankSlipBarcode() {}

    public BankSlipBarcode(String barcode) {
        bankCode = barcode.substring(0,3);
        codeChecker = barcode.substring(4,5);
        dueDateFactor = barcode.substring(5,9);
        billValue = barcode.substring(9,19);
        freeField = barcode.substring(19,44);
        realmDatabase = Realm.getDefaultInstance();
    }

    public Creditor getBank() {
        if(creditor == null) {
            creditor = new CreditorDao(realmDatabase).getCreditorByCode(bankCode);
        }
        return creditor;
    }

    public Calendar getDueDate() {
        if(dueDate == null) {
            int parsedDueDateFactor = Integer.parseInt(dueDateFactor);
            dueDate = BarcodeDueDateParser.getDueDate(Calendar.getInstance(),parsedDueDateFactor);
        }
        return dueDate;
    }

    public Double getBillValue() {
        if(parsedBillValue == null) {
            try {
                parsedBillValue = BarcodeBillValueParser.parseBillValue(billValue);
            } catch (InvalidBarcodeBillValueException e) {
                e.printStackTrace();
            }
        }
        return parsedBillValue;
    }
}
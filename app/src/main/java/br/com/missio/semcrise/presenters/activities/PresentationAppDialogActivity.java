package br.com.missio.semcrise.presenters.activities;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;

import static android.view.View.*;

import br.com.missio.semcrise.R;

public class PresentationAppDialogActivity extends Activity implements OnClickListener {

    private Button understandButton;
    private CheckBox notShowAgainCheckBox;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_presentation_app);
        understandButton = (Button) findViewById(R.id.understand_button);
        understandButton.setOnClickListener(this);
        notShowAgainCheckBox = (CheckBox) findViewById(R.id.not_show_again_check_box);
    }

    @Override
    public void onClick(View v) {
        if(v == understandButton) {
            savePreferenceNotShowAgain(notShowAgainCheckBox.isChecked());
            finish();
        }
    }

    private void savePreferenceNotShowAgain(boolean showAgain) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean(getString(R.string.show_presentation),!showAgain);
        editor.apply();
    }
}

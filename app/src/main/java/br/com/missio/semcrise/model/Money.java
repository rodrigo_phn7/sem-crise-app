package br.com.missio.semcrise.model;

import java.text.DecimalFormat;

public class Money {

    private final DecimalFormat decimalFormat;
    private Double value;

    public Money(Double value) {
        decimalFormat = BrazilianDecimalFormat.MONEY_REAL;
        if(value == null) {
            this.value = 0d;
        } else{
            this.value = value;
        }
    }

    private String formatMonetaryValue() {
        return decimalFormat.format(value);
    }

    @Override
    public String toString() {
        return formatMonetaryValue();
    }
}
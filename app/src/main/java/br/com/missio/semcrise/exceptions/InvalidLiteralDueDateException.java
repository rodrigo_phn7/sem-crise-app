package br.com.missio.semcrise.exceptions;

public class InvalidLiteralDueDateException extends Exception {
    public InvalidLiteralDueDateException(String message) {
        super(message);
    }
}
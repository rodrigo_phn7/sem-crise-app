package br.com.missio.semcrise.model;

public enum DateStatus {
    FUTURE, PAST;
}

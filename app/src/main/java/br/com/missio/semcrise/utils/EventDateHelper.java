package br.com.missio.semcrise.utils;

import java.util.Calendar;
import java.util.Date;

public class EventDateHelper {
    public static boolean isLate(Date effectiveDate, Date expectedDate) {
        Calendar calendarExpectedDate = Calendar.getInstance();
        calendarExpectedDate.setTime(expectedDate);

        Calendar now = Calendar.getInstance();

        return effectiveDate == null && now.after(calendarExpectedDate) && !DateHelper.sameDayOfMonth(calendarExpectedDate,now);

    }
}

package br.com.missio.semcrise.presenters.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import br.com.missio.semcrise.R;
import br.com.missio.semcrise.model.MonetaryEvent;
import br.com.missio.semcrise.presenters.activities.SemCriseActivity;
import br.com.missio.semcrise.services.ExpensesService;

public class ShowEventsFragment extends SemCriseFragment {

    private View showExpensesView;
    private SemCriseActivity activity;
    private SemCriseListFragment listFragment;
    private String action;
    private ExpensesService expensesService;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        showExpensesView = inflater.inflate(R.layout.fragment_show_events, container, false);
        activity = (SemCriseActivity) getActivity();
        action = getArguments().getString(SemCriseActivity.EVENT_ACTION);
        expensesService = new ExpensesService(activity);
        if(!SemCriseActivity.SHOW_FULL_STATEMENT.equals(action)) {
            removeDateFilter();
        }
        return showExpensesView;
    }

    private void removeDateFilter() {
        showExpensesView.findViewById(R.id.filter_full_statement).setVisibility(View.GONE);
    }

    @Override
    public void onResume() {
        super.onResume();
        setUpMonthlyBalanceFragment();
        setUpEventsListFragment();
        setUpAppBarTitle(activity);
        Button lateExpensesButton = (Button) showExpensesView.findViewById(R.id.late_expenses_button);
        setEvents(expensesService.getAllExpenses());
        if(!hasLateExpenses()) {
            lateExpensesButton.setVisibility(View.GONE);
        }
    }

    private void setUpMonthlyBalanceFragment() {
        MonthlyBalanceFragment fragment = new MonthlyBalanceFragment();
        FragmentManager fragmentManager = activity.getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.add(R.id.container_show_events_monthly_balance, fragment);
        fragmentTransaction.commit();
    }

    private void setUpEventsListFragment() {
        if(action != null) {
            switch (action) {
                case SemCriseActivity.SHOW_EXPENSES:
                    listFragment = new AllExpensesListFragment();
                    activity.setEventType(MonetaryEvent.EXPENSE);
                    break;
                case SemCriseActivity.SHOW_RECEIPTS:
                    listFragment = new AllReceiptsListFragment();
                    activity.setEventType(MonetaryEvent.RECEIPT);
                    break;
                case SemCriseActivity.SHOW_MAIN_DAILY_EVENTS:
                    listFragment = new DailyEventsListFragment();
                    removeShowLateExpensesAndShowFullStatementButtons();
                    break;
                case SemCriseActivity.SHOW_FULL_STATEMENT:
                    listFragment = new FullStatementEventListFragment();
                    removeShowLateExpensesAndShowFullStatementButtons();
                    break;
                case SemCriseActivity.SHOW_LATE_EXPENSES_LIST:
                    listFragment = new EspecialExpensesListFragment();
                    listFragment.setArguments(getArguments());
                    removeShowLateExpensesAndShowFullStatementButtons();
                    break;
                case SemCriseActivity.SHOW_NON_EFFECTIVE_DAILY_EXPENSES:
                    listFragment = new EspecialExpensesListFragment();
                    listFragment.setArguments(getArguments());
                    removeShowLateExpensesAndShowFullStatementButtons();
                default:
                    break;
            }
            if(listFragment != null) {
                FragmentManager fragmentManager = activity.getSupportFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.add(R.id.container_events_list, listFragment);
                fragmentTransaction.commit();
            }
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        removeFragment(activity,listFragment);
    }

    public SemCriseActivity getSemCriseActivity() {
        return activity;
    }

    public SemCriseListFragment getListFragment() {
        return listFragment;
    }

    public View getShowExpensesView() {
        return showExpensesView;
    }

    private void removeShowLateExpensesAndShowFullStatementButtons() {
        showExpensesView.findViewById(R.id.full_statement_button).setVisibility(View.GONE);
        showExpensesView.findViewById(R.id.late_expenses_button).setVisibility(View.GONE);
    }
}
package br.com.missio.semcrise.presenters.activities;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import br.com.missio.semcrise.R;

public class BeginningAppActivity extends SemCriseActivity implements View.OnClickListener {

    private Button buttonAcceptAndContinue;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_beginning_app);
        buttonAcceptAndContinue = (Button) findViewById(R.id.button_accept_and_continue);
        buttonAcceptAndContinue.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if(v == buttonAcceptAndContinue){
            goTo(RegisterUserActivity.class);
            finish();
        }
    }
}
package br.com.missio.semcrise.services.barcodereader;

import android.support.annotation.NonNull;

import br.com.missio.semcrise.exceptions.BarcodeInvalidException;
import br.com.missio.semcrise.exceptions.DigitableLineWrongSizeException;
import br.com.missio.semcrise.utils.BarcodeValidator;

public class DigitableLineParser {

    public static final int BANK_SLIP_DIGITABLE_LINE_SIZE = 47;
    private static final int DEALER_DIGITABLE_LINE_SIZE = 48;

    public static String extractBarcode(String digitableLine) throws
            DigitableLineWrongSizeException, BarcodeInvalidException {

        String barcode;

        if(BANK_SLIP_DIGITABLE_LINE_SIZE == digitableLine.length()) {
            barcode = parseBankSlipBarcode(digitableLine);
        } else if(DEALER_DIGITABLE_LINE_SIZE == digitableLine.length()) {
            barcode = parseDealerBarcode(digitableLine);
        } else {
            throw new DigitableLineWrongSizeException("Digitable line " + digitableLine
                    + " has length " + digitableLine.length() + ". Wrong size!!!");
        }

        if(BarcodeValidator.validate(barcode)) {
            return barcode;
        } else {
            throw new BarcodeInvalidException("Invalid Barcode " + barcode);
        }
    }

    @NonNull
    private static String parseBankSlipBarcode(String digitableLine) {
        String barcode;
        String bankCodeAndCurrencyCode = digitableLine.substring(0,4);
        String codeChecker = digitableLine.substring(32,33);
        String dueDateFactorAndBillValue = digitableLine.substring(33);
        String barcodePositions20To24 = digitableLine.substring(4,9);
        String barcodePositions25To34 = digitableLine.substring(10,20);
        String barcodePositions35To44 = digitableLine.substring(21,31);
        barcode = bankCodeAndCurrencyCode + codeChecker
                + dueDateFactorAndBillValue + barcodePositions20To24
                + barcodePositions25To34 + barcodePositions35To44;
        return barcode;
    }

    @NonNull
    private static String parseDealerBarcode(String digitableLine) {
        String barcode;
        String firstField = digitableLine.substring(0,11);
        String secondField = digitableLine.substring(12,23);
        String thirdField = digitableLine.substring(24,35);
        String fourthField = digitableLine.substring(36,47);
        barcode = firstField + secondField + thirdField + fourthField;
        return barcode;
    }
}
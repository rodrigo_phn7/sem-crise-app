package br.com.missio.semcrise.presenters;

import android.support.annotation.IdRes;
import android.view.View;

public class ViewFinderAdapter implements ViewFinder {

    private View view;

    public ViewFinderAdapter(View view) {
        this.view = view;

    }

    @Override
    public View findViewById(@IdRes int id) {
        return view.findViewById(id);
    }
}

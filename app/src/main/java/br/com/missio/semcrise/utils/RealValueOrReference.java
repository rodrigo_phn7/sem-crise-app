package br.com.missio.semcrise.utils;

import lombok.Getter;

public enum RealValueOrReference {
    EFFECTIVE_MOD10(6),
    CURRENCY_QUANTITY_MOD10(7),
    EFFECTIVE_MOD11(8),
    CURRENCY_QUANTITY_MOD11(9);

    @Getter
    private final int identification;

    RealValueOrReference(int identification) {
        this.identification = identification;
    }
}

package br.com.missio.semcrise.presenters.fragments;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;

import br.com.missio.semcrise.R;

public class ConfirmNegotiationDialogFragment extends DialogFragment {

    public static final String CONFIRM_DIALOG_TAG = "confirm_dialog";
    private OnConfirmNegotiationListener confirmNegotiationListener;

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage(R.string.negotiation_terms_message)
                .setTitle(R.string.negotiation_terms_title)
                .setPositiveButton(R.string.negotiate, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        confirmNegotiationListener.onConfirmNegotiation(true);
                    }
                })
                .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        confirmNegotiationListener.onConfirmNegotiation(false);
                    }
                });
        return builder.create();
    }

    public void setConfirmNegotiationListener(OnConfirmNegotiationListener callback) {
        this.confirmNegotiationListener = callback;
    }

    public interface OnConfirmNegotiationListener {
        void onConfirmNegotiation(boolean confirmResponse);
    }

}

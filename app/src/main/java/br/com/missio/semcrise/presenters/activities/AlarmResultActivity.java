package br.com.missio.semcrise.presenters.activities;

import android.content.Intent;
import android.os.Bundle;

import br.com.missio.semcrise.model.Expense;

public class AlarmResultActivity extends SemCriseActivity{

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        startLoginActivity(getIntent());
    }

    private void startLoginActivity(Intent intent) {
        Intent loginIntent = new Intent(this,LoginActivity.class);
        loginIntent.setAction(intent.getAction());
        loginIntent.putExtra(ESPECIAL_EXPENSE_TYPE,intent.getStringExtra(ESPECIAL_EXPENSE_TYPE));
        startActivity(loginIntent);
    }

    @Override
    protected void onResume() {
        super.onResume();
        super.onBackPressed();
    }
}

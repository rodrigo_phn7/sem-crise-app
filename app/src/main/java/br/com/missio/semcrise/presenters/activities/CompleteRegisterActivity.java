package br.com.missio.semcrise.presenters.activities;

import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.widget.Button;
import android.widget.DatePicker;

import com.andreabaccega.widget.FormEditText;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import br.com.missio.semcrise.R;
import br.com.missio.semcrise.model.User;
import br.com.missio.semcrise.presenters.fragments.DatePickerDialogFragment;
import br.com.missio.semcrise.services.UserService;
import br.com.missio.semcrise.utils.DateHelper;
import br.com.missio.semcrise.utils.ValidationHelper;

import static android.app.DatePickerDialog.*;

public class CompleteRegisterActivity extends SemCriseActivity implements OnClickListener, OnFocusChangeListener, OnDateSetListener {

    public static final boolean NEGOTIATION_ENABLED = true;
    private FormEditText userCpfEditText;
    private User user;
    private Button completeRegisterButton;
    private FormEditText cepEditText;
    private FormEditText birthDateEditText;
    private FormEditText professionEditText;
    private FormEditText educationEditText;
    private Date birthDate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_complete_register);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        user = getUser(this);
        setUpUserInputData();
    }

    private void setUpUserInputData() {
        userCpfEditText = (FormEditText) findViewById(R.id.user_cpf_edit_text);
        cepEditText = (FormEditText) findViewById(R.id.cep_edit_text);
        setUpExpectedDateEditText();
        professionEditText = (FormEditText) findViewById(R.id.profession_edit_text);
        educationEditText = (FormEditText) findViewById(R.id.education_edit_text);
        completeRegisterButton = (Button) findViewById(R.id.complete_register_button);
        completeRegisterButton.setOnClickListener(this);
    }

    private void setUpExpectedDateEditText() {
        birthDateEditText = (FormEditText) findViewById(R.id.birth_date_edit_text);
        birthDateEditText.setOnClickListener(this);
        birthDateEditText.setOnFocusChangeListener(this);
    }

    @Override
    public void onFocusChange(View v, boolean hasFocus) {
        if(v == birthDateEditText && hasFocus) {
            showDateDialog();
        }
    }

    @Override
    public void onClick(View v) {
        if(v == birthDateEditText) {
            showDateDialog();
        } else  if((completeRegisterButton == v)){
            boolean formIsValid = validateForm();
            if(formIsValid) {
                updateUser();
                finish();
            }
        }
    }

    private void showDateDialog() {
        DatePickerDialogFragment dialogFragment = new DatePickerDialogFragment();
        dialogFragment.setDateSetListener(this);
        dialogFragment.show(getSupportFragmentManager(), DateHelper.DATE_DIALOG_TAG);
    }

    @Override
    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
        showSettedDateInEditText(year, monthOfYear, dayOfMonth);
        updateBirthDate(year, monthOfYear, dayOfMonth);
    }

    private void showSettedDateInEditText(int year, int monthOfYear, int dayOfMonth) {
        Calendar cal = new GregorianCalendar(year, monthOfYear, dayOfMonth);
        String format = getString(R.string.picker_date_format);
        String formattedDate = DateHelper.formatDate(cal.getTime(), format);
        birthDateEditText.setText(formattedDate);
    }

    private void updateBirthDate(int year, int monthOfYear, int dayOfMonth) {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.DAY_OF_MONTH,dayOfMonth);
        calendar.set(Calendar.MONTH,monthOfYear);
        calendar.set(Calendar.YEAR,year);
        birthDate = calendar.getTime();
    }

    private void updateUser() {
        String cpf = userCpfEditText.getText().toString();
        String postalCode = cepEditText.getText().toString();
        String profession = professionEditText.getText().toString();
        String education = educationEditText.getText().toString();
        new UserService(this).updateUserWith(user, cpf, postalCode, birthDate, profession, education, NEGOTIATION_ENABLED);
    }

    private boolean validateForm() {
        FormEditText[] allFields = {userCpfEditText, cepEditText, birthDateEditText,
                professionEditText, educationEditText};
        return ValidationHelper.validateEachField(allFields);
    }

}
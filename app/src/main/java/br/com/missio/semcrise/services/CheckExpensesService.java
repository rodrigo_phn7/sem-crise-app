package br.com.missio.semcrise.services;

import android.app.IntentService;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;
import android.util.Log;

import br.com.missio.semcrise.R;
import br.com.missio.semcrise.dao.ExpenseDao;
import br.com.missio.semcrise.model.Expense;
import br.com.missio.semcrise.model.MonetaryEventsList;
import br.com.missio.semcrise.model.User;
import br.com.missio.semcrise.presenters.activities.AlarmResultActivity;
import br.com.missio.semcrise.presenters.activities.SemCriseActivity;
import br.com.missio.semcrise.utils.DateHelper;
import br.com.missio.semcrise.utils.SessionManager;
import io.realm.Realm;

public class CheckExpensesService extends IntentService {

    public static final int LATE_NOTIFICATION_ID = 2;
    public static final int DAILY_NOTIFICATION_ID = 3;
    private static final String TAG = "CheckExpensesService";
    private String userEmail;
    private MonetaryEventsList expenses;
    private String especialExpenseType;

    public CheckExpensesService() {
        super(TAG);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        Realm realmDatabase = Realm.getDefaultInstance();
        ExpenseDao expensesDao = new ExpenseDao(realmDatabase);
        expenses =  expensesDao.findByEmail(userEmail);
        checkDailyExpenses();
        checkLateExpenses();
        realmDatabase.close();
        AlarmReceiver.completeWakefulIntent(intent);
    }

    private void checkDailyExpenses() {
        MonetaryEventsList dailyExpenses = expenses.getDailyNonEffectiveEvents();
        int dailyExpensesSize = dailyExpenses.size();
        if(dailyExpensesSize > 1) {
            sendDailyExpensesNotification(dailyExpensesSize);
            Log.i(TAG, dailyExpensesSize + getString(R.string.expenses_found_part_message));
        }else if(dailyExpensesSize == 1) {
            Expense expense = (Expense) dailyExpenses.get(0);
            String msg = composeExpenseMessage(expense);
            String title = getString(R.string.title_today_one_expense_notification_message);
            especialExpenseType = SemCriseActivity.SHOW_NON_EFFECTIVE_DAILY_EXPENSES;
            sendNotification(title, msg, expense, DAILY_NOTIFICATION_ID);
            Log.i(TAG, getString(R.string.one_daily_expense_found));
        } else {
            Log.i(TAG, getString(R.string.daily_expenses_not_found));
        }
    }

    private void sendDailyExpensesNotification(int dailyExpensesSize) {
        String msg = getString(R.string.more_than_one_today_expenses_message_pt01)
                + dailyExpensesSize +
                getString(R.string.more_than_one_today_expenses_message_pt02);
        String title = getString(R.string.title_more_than_one_today_expenses);
        Intent resultIntent = new Intent(this, AlarmResultActivity.class);
        resultIntent.setAction(SemCriseActivity.SHOW_NON_EFFECTIVE_DAILY_EXPENSES);
        notify(dailyExpensesSize, msg, title, resultIntent, DAILY_NOTIFICATION_ID);
    }

    private void checkLateExpenses() {
        MonetaryEventsList lateExpenses = expenses.getLateEvents();
        int lateExpensesSize = lateExpenses.size();
        if(lateExpensesSize > 1) {
            sendLateExpensesNotification(lateExpensesSize);
            Log.i(TAG, lateExpensesSize + getString(R.string.late_expenses_found));
        }else if(lateExpensesSize == 1) {
            Expense expense = (Expense) lateExpenses.get(0);
            String msg = composeExpenseMessage(expense);
            especialExpenseType = SemCriseActivity.SHOW_LATE_EXPENSES_LIST;
            String title = getString(R.string.title_one_late_expense_notification);
            sendNotification(title, msg, expense, LATE_NOTIFICATION_ID);
            Log.i(TAG, getString(R.string.log_one_late_expense_found));
        } else {
            Log.i(TAG, getString(R.string.log_no_late_expenses_found));
        }
    }

    private void sendLateExpensesNotification(int lateExpensesSize) {
        String msg = getString(R.string.more_than_one_late_expenses_message_pt01) +
                lateExpensesSize +
                getString(R.string.more_than_one_late_expenses_message_pt02);
        String title = getString(R.string.title_late_expenses_notification);
        Intent resultIntent = new Intent(this, AlarmResultActivity.class);
        resultIntent.setAction(SemCriseActivity.SHOW_LATE_EXPENSES_LIST);
        notify(lateExpensesSize, msg, title, resultIntent, LATE_NOTIFICATION_ID);
    }

    private void sendNotification(String title, String msg, Expense expense, int notificationID) {
        Intent resultIntent = new Intent(this, AlarmResultActivity.class);
        resultIntent.setAction(Expense.EXPENSE_ID_TAG + SemCriseActivity.HASHTAG_SEPARATOR + expense.getId());
        resultIntent.putExtra(SemCriseActivity.ESPECIAL_EXPENSE_TYPE,especialExpenseType);
        notify(1, msg, title, resultIntent, notificationID);
    }

    private void notify(int lateExpensesSize, String msg, String title, Intent resultIntent, int notificationID) {
        resultIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
        stackBuilder.addParentStack(AlarmResultActivity.class);
        stackBuilder.addNextIntent(resultIntent);

        PendingIntent contentIntent =
                stackBuilder.getPendingIntent(0,PendingIntent.FLAG_UPDATE_CURRENT);

        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(this)
                        .setSmallIcon(R.mipmap.ic_launcher)
                        .setContentTitle(title)
                        .setStyle(new NotificationCompat.BigTextStyle()
                                .bigText(msg))
                        .setContentText(msg)
                        .setAutoCancel(true);

        if(lateExpensesSize > 1) {
            mBuilder.setNumber(lateExpensesSize);
        }
        mBuilder.setContentIntent(contentIntent);

        NotificationManager notificationManager = (NotificationManager)
                this.getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(notificationID, mBuilder.build());
    }

    private String composeExpenseMessage(Expense expense) {
        String format = getApplicationContext().getString(R.string.tiny_date_format);
        String formattedDate = DateHelper.formatDate(expense.getExpectedDate(),format);
        return getString(R.string.one_expense_message_pt01)
                + expense.getName()
                + getString(R.string.one_expense_message_pt02)
                + formattedDate;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        SessionManager session = new SessionManager(getApplicationContext());
        if(session.isUserLoggedIn()) {
            userEmail = session.getUserData().get(User.EMAIL_TAG);
        }
    }
}
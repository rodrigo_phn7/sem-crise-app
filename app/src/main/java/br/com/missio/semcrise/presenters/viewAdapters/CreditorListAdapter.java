package br.com.missio.semcrise.presenters.viewAdapters;

import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.v7.widget.AppCompatTextView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import br.com.missio.semcrise.model.Creditor;
import br.com.missio.semcrise.presenters.activities.SemCriseActivity;
import io.realm.Case;
import io.realm.RealmResults;

public class CreditorListAdapter extends FilterableRealmBaseAdapter<Creditor> {

    private SemCriseActivity context;

    public CreditorListAdapter(SemCriseActivity context, @LayoutRes int layout, RealmResults<Creditor> realmObjectList) {
        super(context, layout, realmObjectList);
        this.context = context;
    }

    @Override
    protected List<Creditor> performRealmFiltering(@NonNull CharSequence constraint, RealmResults<Creditor> results) {
        return results.where().contains("name", constraint.toString(), Case.INSENSITIVE).findAll();
    }

    private static class CreditorViewHolder {
        AppCompatTextView creditorNameEditText;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        CreditorViewHolder viewHolder;
        if (convertView == null) {
            LayoutInflater inflater = context.getLayoutInflater();
            convertView = inflater.inflate(android.R.layout.simple_list_item_1, parent, false);
            viewHolder = new CreditorViewHolder();
            viewHolder.creditorNameEditText = (
                    AppCompatTextView) convertView.findViewById(android.R.id.text1);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (CreditorViewHolder) convertView.getTag();
        }
        Creditor item = mResults.get(position);
        viewHolder.creditorNameEditText.setText(item.getAvailableName());
        return convertView;
    }
}

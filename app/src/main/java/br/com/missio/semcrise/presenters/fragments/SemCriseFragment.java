package br.com.missio.semcrise.presenters.fragments;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;

import br.com.missio.semcrise.model.MonetaryEventsList;
import br.com.missio.semcrise.presenters.activities.SemCriseActivity;

public class SemCriseFragment extends Fragment {

    private MonetaryEventsList events;

    protected void setUpAppBarTitle(SemCriseActivity activity) {
        String title = getArguments().getString(SemCriseActivity.APP_BAR_TITLE);
        assert activity.getSupportActionBar() != null;
        activity.getSupportActionBar().setTitle(title);
    }

    protected void removeFragment(SemCriseActivity activity, SemCriseListFragment fragment) {
        FragmentManager fragmentManager = activity.getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.remove(fragment);
        fragmentTransaction.commit();
    }

    protected boolean hasLateExpenses() {
        return events.hasLateExpenses();
    }

    protected int getLateExpensesQuantity() {
        return events.getLateEvents().size();
    }

    public void setEvents(MonetaryEventsList events) {
        this.events = events;
    }
}

package br.com.missio.semcrise.model;

import android.content.Context;
import java.util.Date;

public interface MonetaryEvent extends Comparable<MonetaryEvent> {
    String RECEIPT = "receipt";
    String EXPENSE = "expense";
    String NONE = "none";
    String EXPENSE_ID_TAG = "expenseId";

    String getName();
    Money getMoneyValue();
    String getFormattedActualDate(Context context);
    Date getActualDate();
    Date getEffectiveDate();
    Double getValue();
    boolean isLate();
    String getEventType();
    Date getExpectedDate();
    boolean isEffective();
    boolean isMonthlyRecurrent();
    boolean isPrefixedValue();
    void setName(String name);
    void setValue(Double value);
    void setExpectedDate(Date date);
    void setEffectiveDate(Date date);
    String getId();
    boolean isNegotiationActive();
    String getEventStatus();
    Date getDateItWasAdded();
    @Override
    int compareTo(MonetaryEvent another);
}

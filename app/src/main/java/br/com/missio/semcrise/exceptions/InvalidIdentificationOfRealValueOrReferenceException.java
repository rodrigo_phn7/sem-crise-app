package br.com.missio.semcrise.exceptions;

/**
 * Created by Rodrigo on 11/04/2016.
 */
public class InvalidIdentificationOfRealValueOrReferenceException extends Throwable {
    public InvalidIdentificationOfRealValueOrReferenceException(String message) {
        super(message);
    }
}

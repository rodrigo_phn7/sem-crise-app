package br.com.missio.semcrise.presenters.fragments;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.support.v4.content.LocalBroadcastManager;
import android.view.ContextMenu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.DatePicker;
import android.widget.ListView;
import android.widget.Toast;

import java.util.Calendar;
import java.util.Date;

import br.com.missio.semcrise.R;
import br.com.missio.semcrise.model.Expense;
import br.com.missio.semcrise.model.MonetaryEvent;
import br.com.missio.semcrise.model.MonetaryEventsList;
import br.com.missio.semcrise.model.Receipt;
import br.com.missio.semcrise.presenters.activities.MainActivity;
import br.com.missio.semcrise.presenters.activities.SemCriseActivity;
import br.com.missio.semcrise.presenters.activities.ShowEventActivity;
import br.com.missio.semcrise.services.ExpensesService;
import br.com.missio.semcrise.services.NegotiationService;
import br.com.missio.semcrise.services.ReceiptsService;
import br.com.missio.semcrise.utils.DateHelper;

import static android.app.DatePickerDialog.OnDateSetListener;
import static br.com.missio.semcrise.presenters.fragments.ConfirmNegotiationDialogFragment.CONFIRM_DIALOG_TAG;
import static br.com.missio.semcrise.presenters.fragments.ConfirmNegotiationDialogFragment.OnConfirmNegotiationListener;

public class SemCriseListFragment extends ListFragment implements OnConfirmNegotiationListener, OnDateSetListener {

    private ExpensesService expensesService;
    private SemCriseActivity activity;
    private ProgressDialog progressDialog;
    private int position;
    private MonetaryEventsList events;
    private ReceiptsService receiptsService;

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        activity = (SemCriseActivity) getActivity();
        registerForContextMenu(getListView());
    }

    @Override
    public void onResume() {
        super.onResume();
        IntentFilter filter = new IntentFilter();
        filter.addAction(NegotiationService.NEGOTIATION_SUCCESS);
        filter.addAction(NegotiationService.NEGOTIATION_UNSUCCESSFUL);
        LocalBroadcastManager.getInstance(activity).registerReceiver(negotiationSuccessBroadcast, filter);
    }

    @Override
    public void onPause() {
        LocalBroadcastManager.getInstance(activity).unregisterReceiver(negotiationSuccessBroadcast);
        super.onPause();
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        MenuInflater inflater = activity.getMenuInflater();
        inflater.inflate(R.menu.event_action_menu, menu);
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) menuInfo;
        int pos = info.position;
        MonetaryEvent event = events.get(pos);
        if(event.isEffective()) {
            menu.removeItem(R.id.effect_event);
        } else {
            MenuItem item = menu.findItem(R.id.effect_event);
            if(MonetaryEvent.EXPENSE.equals(activity.getEventType())) {
                item.setTitle(R.string.make_payment);
            } else {
                item.setTitle(R.string.confirm_receipt);
            }
        }
        if(event.isNegotiationActive() || event.isEffective() || MonetaryEvent.RECEIPT.equals(event.getEventType())) {
            menu.removeItem(R.id.negotiate);
        }
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        activity.dismissAllNotifications();
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        this.position = info.position;
        switch (item.getItemId()) {
            case R.id.negotiate:
                showDialogToConfirmNegotiation();
                return true;
            case R.id.effect_event:
                showDateDialog();
                return true;
            case R.id.delete:
                doDelete();
                return true;
            default:
                return super.onContextItemSelected(item);
        }
    }

    private void showDialogToConfirmNegotiation() {
        ConfirmNegotiationDialogFragment dialogFragment = new ConfirmNegotiationDialogFragment();
        dialogFragment.setConfirmNegotiationListener(this);
        dialogFragment.show(getFragmentManager(), CONFIRM_DIALOG_TAG);
    }

    @Override
    public void onConfirmNegotiation(boolean confirmResponse) {
        if(confirmResponse) {
            doNegotiation();
        } else {
            doNothingAndShowCancelMessage();
        }
    }

    private void doNothingAndShowCancelMessage() {
        Toast.makeText(activity, R.string.cancel_negotiation_message, Toast.LENGTH_SHORT).show();
    }

    private void doNegotiation() {
        progressDialog = ProgressDialog.show(activity, activity.getString(R.string.please_wait), activity.getString(R.string.sending_request), true, false);
        Intent service = new Intent(activity, NegotiationService.class);
        service.setAction(Expense.EXPENSE_ID_TAG + SemCriseActivity.HASHTAG_SEPARATOR + events.get(position).getId());
        activity.startService(service);
    }

    private BroadcastReceiver negotiationSuccessBroadcast = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if(NegotiationService.NEGOTIATION_SUCCESS.equals(action)) {
                progressDialog.dismiss();
                registerNegotiation();
                Toast.makeText(activity, R.string.negotiation_done_message, Toast.LENGTH_SHORT).show();
                ((MainActivity)activity).reloadFragment();
            } else {
                progressDialog.dismiss();
                Toast.makeText(activity, R.string.action_network_error, Toast.LENGTH_LONG).show();
            }
        }
    };

    private void registerNegotiation() {
        Expense expense = ((Expense) events.get(position));
        expensesService.updateExpenseNegotiationStatus(expense, true);
    }

    private void showDateDialog() {
        DatePickerDialogFragment dialogFragment = new DatePickerDialogFragment();
        dialogFragment.setDateSetListener(this);
        dialogFragment.show(getFragmentManager(), DateHelper.DATE_DIALOG_TAG);
    }

    @Override
    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
        updateEffectiveDate(year, monthOfYear, dayOfMonth);
        int message;
        if(MonetaryEvent.EXPENSE.equals(activity.getEventType())) {
            message = R.string.expense_effective_message;
        } else {
            message = R.string.receipt_effective_message;
        }
        Toast.makeText(activity, message, Toast.LENGTH_SHORT).show();
    }

    private void updateEffectiveDate(int year, int monthOfYear, int dayOfMonth) {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.DAY_OF_MONTH,dayOfMonth);
        calendar.set(Calendar.MONTH,monthOfYear);
        calendar.set(Calendar.YEAR, year);
        Date effectiveDate = calendar.getTime();
        if(MonetaryEvent.EXPENSE.equals(activity.getEventType())) {
            Expense expense = ((Expense) events.get(position));
            expensesService.updateEffectiveDate(expense, effectiveDate);
        } else {
            Receipt receipt = ((Receipt) events.get(position));
            receiptsService.updateEffectiveDate(receipt,effectiveDate);
        }
    }

    private void doDelete() {
        MonetaryEvent event = events.get(position);
        if(MonetaryEvent.EXPENSE.equals(event.getEventType())) {
            Expense expenseToDelete =  ((Expense) event );
            String expenseName = expenseToDelete.getName();
            expensesService.delete(expenseToDelete);
            Toast.makeText(activity, activity.getString(R.string.expense) + " " + expenseName + " " + activity.getString(R.string.delete_success), Toast.LENGTH_SHORT).show();
        } else {
            Receipt receiptToDelete =  ((Receipt) event);
            String expenseName = receiptToDelete.getName();
            receiptsService.delete(receiptToDelete);
            Toast.makeText(activity, activity.getString(R.string.receipt) + " " + expenseName + " " + activity.getString(R.string.delete_success), Toast.LENGTH_SHORT).show();
        }
        activity.recreate();
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        Intent intent = new Intent(activity,ShowEventActivity.class);
        MonetaryEvent event = events.get(position);
        if(MonetaryEvent.EXPENSE.equals(event.getEventType())) {
            intent.putExtra(SemCriseActivity.EVENT_TYPE_TAG, MonetaryEvent.EXPENSE);
        } else {
            intent.putExtra(SemCriseActivity.EVENT_TYPE_TAG, MonetaryEvent.RECEIPT);
        }
        intent.setAction(event.getId());
        startActivity(intent);
    }

    public void setExpensesService(ExpensesService expensesService) {
        this.expensesService = expensesService;
    }

    public void setReceiptsService(ReceiptsService receiptsService) {
        this.receiptsService = receiptsService;
    }

    public void setEvents(MonetaryEventsList events) {
        this.events = events;
    }

    public SemCriseActivity getSemCriseActivity() {
        return activity;
    }

}


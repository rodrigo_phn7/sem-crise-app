package br.com.missio.semcrise.presenters.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.Toolbar;
import br.com.missio.semcrise.R;
import br.com.missio.semcrise.model.MonetaryEvent;
import br.com.missio.semcrise.presenters.fragments.AddBarcodeExpenseFragment;
import br.com.missio.semcrise.presenters.fragments.AddOrUpdateMonetaryEventFragment;

public class AddOrUpdateEventActivity extends SemCriseActivity {
    private Fragment fragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_or_update_event);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        Intent intent = getIntent();
        String action = intent.getAction();
        String eventType = intent.getStringExtra(SemCriseActivity.EVENT_TYPE_TAG);
        String title;

        if(ADD_EVENT.equals(action) || ADD_EXPENSE_THROUGH_READ_BARCODE.equals(action)) {
            title = getString(R.string.add);
        } else {
            title = getString(R.string.edit);
        }

        if(MonetaryEvent.EXPENSE.equals(eventType)) {
            title += " " + getString(R.string.expense);
        } else {
            title += " " + getString(R.string.receipt);
        }
        setTitle(title);

        if(ADD_EXPENSE_THROUGH_READ_BARCODE.equals(action)) {
            fragment = new AddBarcodeExpenseFragment();
        } else {
            fragment = new AddOrUpdateMonetaryEventFragment();
        }
        showFragment();
    }

    private void showFragment() {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.container_add_or_update_body, fragment);
        fragmentTransaction.commit();
    }

}
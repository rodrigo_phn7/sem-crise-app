package br.com.missio.semcrise;

import android.app.Application;
import android.content.Context;

import br.com.missio.semcrise.dao.SemCriseDatabaseMigration;
import br.com.missio.semcrise.services.AlarmReceiver;
import br.com.missio.semcrise.utils.ConfigurationHelper;
import io.realm.Realm;
import io.realm.RealmConfiguration;

public class SemCriseApplication extends Application {
    private AlarmReceiver alarm = new AlarmReceiver();
    private static Context context;

    @Override
    public void onCreate() {
        super.onCreate();
        configRealmDatabase(this);
        ConfigurationHelper.setUpAlarm(this,alarm);
        context = getApplicationContext();
    }

    protected void configRealmDatabase(Context context) {
        RealmConfiguration config = new RealmConfiguration.Builder(context)
                .name("semcrise.realm")
                .schemaVersion(12)
                .migration(new SemCriseDatabaseMigration())
                .build();
        Realm.setDefaultConfiguration(config);
    }

    public static Context getContext() {
        return context;
    }
}

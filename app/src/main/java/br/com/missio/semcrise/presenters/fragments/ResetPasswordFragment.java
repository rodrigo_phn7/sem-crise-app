package br.com.missio.semcrise.presenters.fragments;

import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.andreabaccega.widget.FormEditText;

import br.com.missio.semcrise.R;
import br.com.missio.semcrise.services.UserService;
import br.com.missio.semcrise.utils.PasswordNumberValidator;
import br.com.missio.semcrise.utils.ValidationHelper;

public class ResetPasswordFragment extends Fragment {

    private FormEditText editTextUserPassword;
    private FormEditText editTextUserConfirmPassword;
    private String pass;
    private Activity activity;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_change_pass_final_step,container,false);
        editTextUserPassword = (FormEditText) view.findViewById(R.id.editTextRegisterPassword);
        editTextUserPassword.addValidator(new PasswordNumberValidator(getString(R.string.error_minimum_digits)));
        editTextUserConfirmPassword = (FormEditText) view.findViewById(R.id.editTextRegisterConfirmPassword);
        editTextUserConfirmPassword.addValidator(new PasswordNumberValidator(getString(R.string.error_minimum_digits)));
        activity = getActivity();
        return view;
    }

    public void resetPassword() {
        if(formIsValid()) {
            UserService userService = new UserService(activity);
            userService.changePassword(pass);
            Toast.makeText(activity, "Senha trocada com sucesso.", Toast.LENGTH_SHORT).show();
            activity.finish();
        }
    }

    private boolean formIsValid() {
        FormEditText[] allFields = {editTextUserPassword,editTextUserConfirmPassword};
        boolean allFieldsAreValid = ValidationHelper.validateEachField(allFields);
        return allFieldsAreValid && passConfirmed();
    }

    private boolean passConfirmed() {
        pass = editTextUserPassword.getText().toString();
        String confirmPass = editTextUserConfirmPassword.getText().toString();
        boolean passConfirmed = pass.equals(confirmPass);
        if(!passConfirmed){
            CharSequence notConfirmedPassMessage = getText(R.string.passwords_doesnt_match);
            editTextUserPassword.setError(notConfirmedPassMessage);
            editTextUserConfirmPassword.setError(notConfirmedPassMessage);
        }
        return passConfirmed;
    }

}

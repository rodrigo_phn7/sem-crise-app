package br.com.missio.semcrise.exceptions;

public class DigitableLineWrongSizeException extends Exception{
    public DigitableLineWrongSizeException(String message) {
        super(message);
    }
}

package br.com.missio.semcrise.presenters.fragments;

import android.app.NotificationManager;
import android.content.Context;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import br.com.missio.semcrise.presenters.activities.SemCriseActivity;
import br.com.missio.semcrise.model.MonetaryEvent;
import br.com.missio.semcrise.model.MonetaryEventsList;
import br.com.missio.semcrise.services.CheckExpensesService;
import br.com.missio.semcrise.services.ExpensesService;
import br.com.missio.semcrise.presenters.viewAdapters.MonetaryEventsArrayAdapter;

public class EspecialExpensesListFragment extends SemCriseListFragment {

    private SemCriseActivity activity;
    private String action;

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        activity = (SemCriseActivity) getActivity();
        registerForContextMenu(getListView());
        action = getArguments().getString(SemCriseActivity.EVENT_ACTION);
    }

    @Override
    public void onResume() {
        super.onResume();
        setUpList();
        dismissNotifications();
    }

    private void dismissNotifications() {
        NotificationManager manager = (NotificationManager) activity.getSystemService(Context.NOTIFICATION_SERVICE);
        if(SemCriseActivity.SHOW_NON_EFFECTIVE_DAILY_EXPENSES.equals(action)) {
            manager.cancel(CheckExpensesService.DAILY_NOTIFICATION_ID);
        } else {
            manager.cancel(CheckExpensesService.LATE_NOTIFICATION_ID);
        }
    }

    private void setUpList() {
        ExpensesService expensesService = new ExpensesService(activity.getApplicationContext());
        MonetaryEventsList expenses;
        if(SemCriseActivity.SHOW_NON_EFFECTIVE_DAILY_EXPENSES.equals(action)) {
            expenses = expensesService.getDailyNonEffectiveExpenses();
        } else {
            expenses = expensesService.getLateExpenses();
        }
        setExpensesService(expensesService);
        setEvents(expenses);
        ArrayAdapter<MonetaryEvent> adapter = new MonetaryEventsArrayAdapter(activity, expenses);
        setListAdapter(adapter);
    }
}
package br.com.missio.semcrise.services;

import android.content.Context;

import java.io.UnsupportedEncodingException;
import java.util.Properties;

import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import br.com.missio.semcrise.R;

public class MailSender {

    private static String senderUsername;
    private static String senderPassword;

    public static void sendMail(Context context, String email, String subject, String messageBody) throws MessagingException, UnsupportedEncodingException {
        senderUsername = context.getString(R.string.senderUsername);
        senderPassword = context.getString(R.string.senderPassword);
        Session session = createSessionObject(context);
        Message message = createMessage(email, subject, messageBody, session);
        Transport.send(message);
    }

    private static Session createSessionObject(Context context) {
        Properties properties = new Properties();
        properties.put("mail.smtp.auth", "true");
        properties.put("mail.smtp.starttls.enable", "true");
        properties.put("mail.smtp.host", context.getString(R.string.email_smtp));
        properties.put("mail.smtp.port", context.getString(R.string.email_port));
        return Session.getInstance(properties, new Authenticator() {
            @Override
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(senderUsername, senderPassword);
            }
        });
    }

    private static Message createMessage(String email, String subject, String messageBody, Session session) throws UnsupportedEncodingException, MessagingException {
        Message message = new MimeMessage(session);
        message.setFrom(new InternetAddress(senderUsername, senderUsername));
        message.setRecipient(Message.RecipientType.TO, new InternetAddress(email, email));
        message.setSubject(subject);
        message.setText(messageBody);
        return message;
    }
}

package br.com.missio.semcrise.services;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;

import br.com.missio.semcrise.R;
import br.com.missio.semcrise.model.User;
import br.com.missio.semcrise.utils.RandomStringGenerator;
import io.realm.Realm;

public class VerificationCodeSenderService extends IntentService {

    public static final String SEND_VERIFICATION_CODE_SUCCESS = "send_verification_code_success";
    public static final String VERIFICATION_CODE = "verification_code";
    public static final String SEND_VERIFICATION_CODE_UNSUCCESS = "send_verification_code_unsuccess";
    private Context context;
    private String verificationCode;
    private String userEmail;

    public VerificationCodeSenderService() {
        super("VerificationCodeSenderService");
    }

    @Override
    public void onCreate() {
        super.onCreate();
        context = getApplicationContext();
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        userEmail = intent.getAction();
        String messageBody;
        try {
            messageBody = composeMessage();
            String subject = getString(R.string.send_verification_code_subject);
            MailSender.sendMail(context, userEmail, subject, messageBody);
            sendSuccessBroadcast();
        } catch (Exception e) {
            e.printStackTrace();
            sendUnsuccessfulBroadcast();
        }
    }

    private void sendSuccessBroadcast() {
        Intent success = new Intent(SEND_VERIFICATION_CODE_SUCCESS);
        success.putExtra(VERIFICATION_CODE, verificationCode);
        LocalBroadcastManager.getInstance(this).sendBroadcast(success);
    }

    private void sendUnsuccessfulBroadcast() {
        Intent unsuccessful = new Intent(SEND_VERIFICATION_CODE_UNSUCCESS);
        LocalBroadcastManager.getInstance(this).sendBroadcast(unsuccessful);
    }

    private String composeMessage() throws Exception {
        Realm realmDatabase = Realm.getDefaultInstance();
        User user = getUser();
        int verificationCodeLength = Integer.parseInt(getString(R.string.verification_code_lenth));
        verificationCode = RandomStringGenerator.generateRandomString(verificationCodeLength, RandomStringGenerator.Mode.NUMERIC);
        String message = getString(R.string.change_pass_message_pt01) + user.getName() +
                         getString(R.string.change_pass_message_pt02) + verificationCode +
                         getString(R.string.change_pass_message_pt03);
        realmDatabase.close();
        return message;
    }

    private User getUser() {
        UserService userService = new UserService(context);
        return userService.getUserByEmail(userEmail);
    }

}

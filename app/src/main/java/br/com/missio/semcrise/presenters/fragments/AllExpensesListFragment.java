package br.com.missio.semcrise.presenters.fragments;

import android.widget.ArrayAdapter;
import br.com.missio.semcrise.model.MonetaryEvent;
import br.com.missio.semcrise.model.MonetaryEventsList;
import br.com.missio.semcrise.services.ExpensesService;
import br.com.missio.semcrise.presenters.viewAdapters.MonetaryEventsArrayAdapter;

public class AllExpensesListFragment extends SemCriseListFragment {

    @Override
    public void onResume() {
        super.onResume();
        fillList();
    }

    private void fillList() {
        ExpensesService expensesService = new ExpensesService(getSemCriseActivity().getApplicationContext());
        MonetaryEventsList expenses = expensesService.getAllExpenses();
        setEvents(expenses);
        setExpensesService(expensesService);
        ArrayAdapter<MonetaryEvent> adapter = new MonetaryEventsArrayAdapter(getSemCriseActivity(), expenses);
        setListAdapter(adapter);
    }
}
package br.com.missio.semcrise.presenters.activities;

import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;

import br.com.missio.semcrise.R;
import br.com.missio.semcrise.model.MonetaryEvent;
import br.com.missio.semcrise.model.User;
import br.com.missio.semcrise.services.CheckExpensesService;
import br.com.missio.semcrise.services.UserService;

public class SemCriseActivity extends AppCompatActivity {
    public static final String ADD_EXPENSE_THROUGH_READ_BARCODE = "add_expense_read_barcode";
    public static final String BARCODE = "barcode";
    public static final String ESPECIAL_EXPENSE_TYPE = "especial_expense_type";
    public static final String APP_BAR_TITLE = "app_bar_title";
    public static final String SHOW_MAIN_DAILY_EVENTS = "show_daily_events";
    public static final String ADD_EVENT = "add_event";
    public static final String EVENT_ACTION = "event_action";
    public static final String SHOW_EXPENSES = "show_expenses";
    public static final String SHOW_RECEIPTS = "show_receipts";
    public static final String SHOW_LATE_EXPENSES_LIST = "show_late_expenses";
    public static final String SHOW_NON_EFFECTIVE_DAILY_EXPENSES = "show_daily_expenses";
    public static final String SHOW_FULL_STATEMENT = "show_full_statement";
    public static final String HASHTAG_SEPARATOR = "#";
    public static String EVENT_TYPE_TAG = "event_type";
    private String eventType = MonetaryEvent.NONE;

    public void goTo(Class<?> activityClass) {
        Intent intent = new Intent(this,activityClass);
        intent.putExtra(EVENT_TYPE_TAG, getEventType());
        startActivity(intent);
    }

    public String getEventType() {
        return eventType;
    }

    public void setEventType(String eventType) {
        this.eventType = eventType;
    }

    public void dismissNotification(String especialExpenseType) {
        NotificationManager manager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        if(SHOW_NON_EFFECTIVE_DAILY_EXPENSES.equals(especialExpenseType)) {
            manager.cancel(CheckExpensesService.DAILY_NOTIFICATION_ID);
        } else {
            manager.cancel(CheckExpensesService.LATE_NOTIFICATION_ID);
        }
    }

    public static User getUser(Context context) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        String userEmail = sharedPreferences.getString(context.getString(R.string.key_pref_user_email),"");
        return new UserService(context).getUserByEmail(userEmail);
    }

    public void dismissAllNotifications() {
        NotificationManager manager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        manager.cancel(CheckExpensesService.DAILY_NOTIFICATION_ID);
        manager.cancel(CheckExpensesService.LATE_NOTIFICATION_ID);
    }
}

package br.com.missio.semcrise.model;

import br.com.missio.semcrise.R;
import br.com.missio.semcrise.SemCriseApplication;

public enum PaymentMethod {
    CASH(R.string.cash),
    BANK_SLIP(R.string.bank_slip),
    CREDIT(R.string.credit),
    DEBIT(R.string.debit);

    private final int resourceId;

    PaymentMethod(int id) {
        this.resourceId = id;
    }

    @Override
    public String toString() {
        return SemCriseApplication.getContext().getString(resourceId);
    }
}

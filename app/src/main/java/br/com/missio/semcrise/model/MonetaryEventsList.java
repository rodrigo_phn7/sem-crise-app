package br.com.missio.semcrise.model;

import java.util.ArrayList;

import br.com.missio.semcrise.utils.DateHelper;

public class MonetaryEventsList extends ArrayList<MonetaryEvent> {

    public MonetaryEventsList getMonthlyEventsUntilNow() {
        MonetaryEventsList eventsByCurrentMonthUntilNow = new MonetaryEventsList();
        for(MonetaryEvent monetaryEvent : this) {
           if(monetaryEvent.isEffective()){
               if(DateHelper.isFromCurrentMonthUntilNow(monetaryEvent.getEffectiveDate())){
                    eventsByCurrentMonthUntilNow.add(monetaryEvent);
               }
           }
        }
        return eventsByCurrentMonthUntilNow;
    }

    public Double getTotalAmount(){
        Double balance = (double) 0;
        for (MonetaryEvent monetaryEvent : this) {
            balance += monetaryEvent.getValue();
        }
        return balance;
    }

    public MonetaryEventsList getDailyEffectiveEvents() {
        MonetaryEventsList dailyEvents = new MonetaryEventsList();
        for (MonetaryEvent monetaryEvent : this) {
            if(monetaryEvent.isEffective() && DateHelper.isFromCurrentDay(monetaryEvent.getActualDate())){
                dailyEvents.add(monetaryEvent);
            }
        }
        return dailyEvents;
    }

    public MonetaryEventsList getLateEvents() {
        MonetaryEventsList lateEvents = new MonetaryEventsList();
        for(MonetaryEvent monetaryEvent : this) {
            if(monetaryEvent.isLate() && !monetaryEvent.isNegotiationActive()){
                lateEvents.add(monetaryEvent);
            }
        }
        return lateEvents;
    }

    public MonetaryEventsList getAllEventsIn(int period, DateStatus dateStatus) {
        MonetaryEventsList events = new MonetaryEventsList();
        for(MonetaryEvent monetaryEvent : this) {
            if(DateHelper.dateMatchesWith(monetaryEvent.getExpectedDate(), period, dateStatus)) {
                events.add(monetaryEvent);
            }
        }
        return events;
    }

    public MonetaryEventsList getDailyNonEffectiveEvents() {
        MonetaryEventsList dailyEvents = new MonetaryEventsList();
        for (MonetaryEvent monetaryEvent : this) {
            if(!monetaryEvent.isEffective() && !monetaryEvent.isNegotiationActive() && DateHelper.isFromCurrentDay(monetaryEvent.getActualDate())){
                dailyEvents.add(monetaryEvent);
            }
        }
        return dailyEvents;
    }

    public boolean hasLateExpenses() {
        for(MonetaryEvent monetaryEvent : this) {
            if(monetaryEvent.isLate() && !monetaryEvent.isNegotiationActive()){
                return true;
            }
        }
        return false;
    }
}

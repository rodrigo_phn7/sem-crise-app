package br.com.missio.semcrise.presenters.activities;

import android.os.Bundle;

import br.com.missio.semcrise.presenters.fragments.SettingsFragment;

public class SettingsActivity extends SemCriseActivity {
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getFragmentManager().beginTransaction()
                .replace(android.R.id.content, new SettingsFragment())
                .commit();
    }
}

package br.com.missio.semcrise.utils;

import android.support.annotation.NonNull;

import java.util.Calendar;

import br.com.missio.semcrise.exceptions.DayNumberOutOfBoundsException;
import br.com.missio.semcrise.exceptions.InvalidLiteralDueDateException;
import br.com.missio.semcrise.exceptions.YearNumberOutOfBoundsException;

public class BarcodeDueDateParser {
    public static final int MIN_DATE_BASIS_FACTOR = 1000;
    public static final int MAX_DATE_BASIS_FACTOR = 9999;

    public static Calendar getDueDate(Calendar actualDate,int dueDateFactor) {
        if(dueDateFactor > MAX_DATE_BASIS_FACTOR || dueDateFactor < MIN_DATE_BASIS_FACTOR) {
            return null;
        }
        Calendar dateBasis = getDateBasis(actualDate,dueDateFactor);
        dateBasis.add(Calendar.DATE,dueDateFactor - MIN_DATE_BASIS_FACTOR);
        return dateBasis;
    }

    @NonNull
    private static Calendar getDateBasis(Calendar calendar,int dueDateFactor) {
        int year,month,day;
        if(calendar.get(Calendar.YEAR) >= 2020 && dueDateFactor <5000 ) {
            year = 2025;
            month = Calendar.FEBRUARY;
            day = 22;
        } else {
            year = 2000;
            month = Calendar.JULY;
            day = 3;
        }
        Calendar dateBasis = Calendar.getInstance();
        dateBasis.set(Calendar.YEAR,year);
        dateBasis.set(Calendar.MONTH,month);
        dateBasis.set(Calendar.DAY_OF_MONTH,day);
        dateBasis.set(Calendar.HOUR_OF_DAY,0);
        dateBasis.set(Calendar.MINUTE,0);
        dateBasis.set(Calendar.SECOND, 1);
        dateBasis.set(Calendar.MILLISECOND,0);
        return dateBasis;
    }

    public static Calendar getDueDate(String literalDueDate) throws InvalidLiteralDueDateException {
        if(literalDueDate != null && literalDueDate.length() == 8) {
            try {
                String day = literalDueDate.substring(6,8);
                String month = literalDueDate.substring(4,6);
                String year = literalDueDate.substring(0,4);
                Calendar dueDate = Calendar.getInstance();
                dueDate.set(Calendar.HOUR_OF_DAY,0);
                dueDate.set(Calendar.MINUTE,0);
                dueDate.set(Calendar.SECOND, 1);
                dueDate.set(Calendar.MILLISECOND,0);
                dueDate.set(Calendar.DAY_OF_MONTH, parseDay(day));
                dueDate.set(Calendar.MONTH,DateHelper.parseMonth(month));
                dueDate.set(Calendar.YEAR, parseYear(year));
                return dueDate;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        throw new InvalidLiteralDueDateException("Invalid Literal Due Date: " + literalDueDate);
    }

    private static int parseYear(String year) throws YearNumberOutOfBoundsException {
        int currentYear = Calendar.getInstance().get(Calendar.YEAR);
        int parsedYear = Integer.parseInt(year);
        if(parsedYear > (currentYear - 100) && parsedYear < (currentYear + 100)) {
            return parsedYear;
        }
        throw new YearNumberOutOfBoundsException("Year Number " + year + "is out of bounds.");
    }

    private static int parseDay(String day) throws DayNumberOutOfBoundsException {
        int parsedDay = Integer.parseInt(day);
        if(parsedDay > 0 && parsedDay <32) {
            return parsedDay;
        }
        throw new DayNumberOutOfBoundsException("Day Number " + day + " is out of bounds.");
    }
}
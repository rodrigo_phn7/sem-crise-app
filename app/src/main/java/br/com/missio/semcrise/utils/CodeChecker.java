package br.com.missio.semcrise.utils;

public class CodeChecker {
    public static final int[] MOD_11_WEIGHTS = {2, 3, 4, 5, 6, 7, 8, 9};
    public static final int[] MOD_10_WEIGHTS = {2, 1};
    private final String digits;
    private final DigitsParser digitsParser;

    public CodeChecker(String digits) {
        this.digits = digits;
        digitsParser = new DigitsParser(digits);
    }

    public int calculateMod11BankSlipCodeChecker(int codeCheckerPosition) {
        int[] digitsWithoutCodeChecker = digitsParser.getDigitsWithoutCodeChecker(codeCheckerPosition);
        int rest = sumDigits(multiplyDigits(digitsWithoutCodeChecker, MOD_11_WEIGHTS)) % 11;
        int elevenModule = 11 - rest;
        elevenModule = Math.abs(elevenModule);
        int calculatedCodeChecker;
        if(elevenModule == 0 || elevenModule == 10 || elevenModule == 11) {
            calculatedCodeChecker = 1;
        } else {
            calculatedCodeChecker = elevenModule;
        }
        return calculatedCodeChecker;
    }

    public int calculateMod11CodeChecker(int codeCheckerPosition) {
        int[] digitsWithoutCodeChecker = digitsParser.getDigitsWithoutCodeChecker(codeCheckerPosition);
        int rest = sumDigits(multiplyDigits(digitsWithoutCodeChecker, MOD_11_WEIGHTS)) % 11;
        int elevenModule = rest - 11;
        elevenModule = Math.abs(elevenModule);
        int calculatedCodeChecker;
        if(rest == 0 || rest == 1) {
            calculatedCodeChecker = 0;
        } else if(rest == 10) {
            calculatedCodeChecker = 1;
        } else {
            calculatedCodeChecker = elevenModule;
        }
        return calculatedCodeChecker;
    }

    public int calculateMod10CodeChecker(int codeCheckerPosition) {
        int[] digitsWithoutCodeChecker = digitsParser.getDigitsWithoutCodeChecker(codeCheckerPosition);
        int rest = sumMod10Digits(multiplyDigits(digitsWithoutCodeChecker, MOD_10_WEIGHTS)) % 10;
        int tenModule = 10 - rest;
        tenModule = Math.abs(tenModule);
        int calculatedCodeChecker;
        if(rest == 0) {
            calculatedCodeChecker = 0;
        } else {
            calculatedCodeChecker = tenModule;
        }
        return calculatedCodeChecker;
    }

    private int sumMod10Digits(int[] digitsWithoutCodeChecker) {
        int sumOfMultipliedDigit = 0;
        for (int multipliedDigit : digitsWithoutCodeChecker) {
            sumOfMultipliedDigit += partitionAndSumIfNeeded(multipliedDigit);
        }
        return sumOfMultipliedDigit;
    }

    private int partitionAndSumIfNeeded(int multipliedDigit) {
        if(multipliedDigit < 10) {
            return multipliedDigit;
        } else {
            int sum = 0;
            char[] charDigits = Integer.toString(multipliedDigit).toCharArray();
            for (char charDigit : charDigits) {
                String digit = charDigit + "";
                sum += Integer.parseInt(digit);
            }
            return sum;
        }
    }

    private int sumDigits(int[] digitsWithoutCodeChecker) {
        int sumOfMultipliedDigit = 0;
        for (int multipliedDigit : digitsWithoutCodeChecker) {
            sumOfMultipliedDigit += multipliedDigit;
        }
        return sumOfMultipliedDigit;
    }

    private int[] multiplyDigits(int[] digitsWithoutCodeChecker, int[] weights) {
        int[] multipliedDigits = new int[digitsWithoutCodeChecker.length];
        int weightsIndex = 0;
        for(int i = (digits.length() - 2); i>=0; i--) {
            multipliedDigits[i] = digitsWithoutCodeChecker[i] * weights[weightsIndex];
            weightsIndex ++;
            if(weightsIndex == weights.length) {
                weightsIndex = 0;
            }
        }
        return multipliedDigits;
    }

    public int getCodeChecker(int codeCheckerPosition) {
        return Integer.parseInt(digits.substring(codeCheckerPosition,codeCheckerPosition+1));
    }
}
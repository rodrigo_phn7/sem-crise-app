package br.com.missio.semcrise.services.barcodereader;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.hardware.Camera;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.vision.MultiProcessor;
import com.google.android.gms.vision.barcode.Barcode;
import com.google.android.gms.vision.barcode.BarcodeDetector;

import java.io.IOException;

import br.com.missio.semcrise.R;
import br.com.missio.semcrise.presenters.activities.SemCriseActivity;
import br.com.missio.semcrise.services.barcodereader.ui.camera.CameraSource;
import br.com.missio.semcrise.services.barcodereader.ui.camera.CameraSourcePreview;
import br.com.missio.semcrise.services.barcodereader.ui.camera.GraphicOverlay;
import br.com.missio.semcrise.utils.BarcodeTestCase;

//Qualquer dúvida, ler documentação oficial do google gms
public class BarcodeCaptureActivity extends SemCriseActivity implements BarcodeGraphicTracker.BarcodeDetectListener {

    private static final int RC_HANDLE_CAMERA_PERM = 2;
    private static final String TAG = "Barcode-reader";
    private static final int RC_HANDLE_GMS = 9001;
    public static final String BARCODE_VALUE = "barcode_value";
    public static final int BARCODE_SUCCESS = 1234;
    public static final int BARCODE_SIZE = 44;
    private CameraSourcePreview mPreview;
    private GraphicOverlay<BarcodeGraphic> mGraphicOverlay;
    private CameraSource mCameraSource;
    private boolean useFlash = false;
    private boolean autoFocus = true;

    @Override
    public void onCreate(Bundle icicle) {
        super.onCreate(icicle);
        setContentView(R.layout.barcode_scanner);

        mPreview = (CameraSourcePreview) findViewById(R.id.preview);
        mGraphicOverlay = (GraphicOverlay<BarcodeGraphic>) findViewById(R.id.graphicOverlay);
        checkPermissionAndCreateCameraSource();
    }

    private void checkPermissionAndCreateCameraSource() {
        int rc = ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA);
        if (rc == PackageManager.PERMISSION_GRANTED) {
            createCameraSource(autoFocus, useFlash);
        } else {
            requestCameraPermission();
        }
    }

    @SuppressLint("InlinedApi")
    private void createCameraSource(boolean autoFocus, boolean useFlash) {
        Context context = getApplicationContext();
        BarcodeDetector barcodeDetector = new BarcodeDetector.Builder(context).build();
        BarcodeTrackerFactory barcodeFactory = new BarcodeTrackerFactory(mGraphicOverlay);
        barcodeFactory.setBarcodeDetectListener(this);
        barcodeDetector.setProcessor(
                new MultiProcessor.Builder<>(barcodeFactory).build());

        if (!barcodeDetector.isOperational()) {
            Log.w(TAG, "Detector dependencies are not yet available.");
            IntentFilter lowstorageFilter = new IntentFilter(Intent.ACTION_DEVICE_STORAGE_LOW);
            boolean hasLowStorage = registerReceiver(null, lowstorageFilter) != null;

            if (hasLowStorage) {
                Toast.makeText(this, R.string.low_storage_error, Toast.LENGTH_LONG).show();
                Log.w(TAG, getString(R.string.low_storage_error));
            }
        }

        CameraSource.Builder builder = new CameraSource.Builder(getApplicationContext(), barcodeDetector)
                .setFacing(CameraSource.CAMERA_FACING_BACK)
                .setRequestedPreviewSize(1600, 1024)
                .setRequestedFps(15.0f);

        // make sure that auto focus is an available option
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.ICE_CREAM_SANDWICH) {
            builder = builder.setFocusMode(
                    autoFocus ? Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE : null);
        }

        mCameraSource = builder
                .setFlashMode(useFlash ? Camera.Parameters.FLASH_MODE_TORCH : null)
                .build();

        Toast.makeText(BarcodeCaptureActivity.this, R.string.wait_camera_message, Toast.LENGTH_LONG).show();
    }


    private void requestCameraPermission() {
        Log.w(TAG, "Camera permission is not granted. Requesting permission");

        final String[] permissions = new String[]{Manifest.permission.CAMERA};

        if (!ActivityCompat.shouldShowRequestPermissionRationale(this,
                Manifest.permission.CAMERA)) {
            ActivityCompat.requestPermissions(this, permissions, RC_HANDLE_CAMERA_PERM);
            return;
        }

        final Activity thisActivity = this;

        View.OnClickListener listener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ActivityCompat.requestPermissions(thisActivity, permissions,
                        RC_HANDLE_CAMERA_PERM);
            }
        };

        Snackbar.make(mGraphicOverlay, R.string.permission_camera_rationale,
                Snackbar.LENGTH_INDEFINITE)
                .setAction(R.string.ok, listener)
                .show();
    }

    @Override
    protected void onResume() {
        super.onResume();
        startCameraSource();
    }

    private void startCameraSource() throws SecurityException {
        int code = GoogleApiAvailability.getInstance().isGooglePlayServicesAvailable(
                getApplicationContext());
        if (code != ConnectionResult.SUCCESS) {
            Dialog dlg =
                    GoogleApiAvailability.getInstance().getErrorDialog(this, code, RC_HANDLE_GMS);
            dlg.show();
        }

        if (mCameraSource != null) {
            try {
                mPreview.start(mCameraSource, mGraphicOverlay);
            } catch (IOException e) {
                Log.e(TAG, "Unable to start camera source.", e);
                mCameraSource.release();
                mCameraSource = null;
            }
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        stopCamera();
    }

    private void stopCamera() {
        if (mPreview != null) {
            mPreview.stop();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mPreview != null) {
            mPreview.release();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        if (requestCode != RC_HANDLE_CAMERA_PERM) {
            Log.d(TAG, "Got unexpected permission result: " + requestCode);
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
            return;
        }

        if (grantResults.length != 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            Log.d(TAG, "Camera permission granted - initialize the camera source");
            createCameraSource(autoFocus, useFlash);
            return;
        }

        Log.e(TAG, "Permission not granted: results len = " + grantResults.length +
                " Result code = " + (grantResults.length > 0 ? grantResults[0] : "(empty)"));

        DialogInterface.OnClickListener listener = new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                finish();
            }
        };

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Detector de Código de Barras")
                .setMessage(R.string.no_camera_permission)
                .setPositiveButton(R.string.ok, listener)
                .show();
    }

    @Override
    public void onBarcodeDetect(Barcode barcode) {
        String barcodeValue = barcode.rawValue;
        Log.i(TAG, "Barcode detected: " + barcodeValue);
        Log.i(TAG, "Size: " + barcodeValue.length());
        if (barcodeValue.length() == BARCODE_SIZE) {
            Intent intent = new Intent();
            intent.putExtra(BARCODE_VALUE, barcodeValue);
            setResult(BARCODE_SUCCESS, intent);
            finish();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.barcode_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.use_flash:
                changeFlashStatus();
                restartCamera();
                return true;
            case R.id.type_barcode:
                showTypeBarcodeScreen();
                return true;
            case R.id.simulate_bank_slip_read:
                simulateBankSlipRead();
                return true;
            case R.id.simulate_company_dealer_read:
                simulateCompanyDealerRead();
                return true;
            case R.id.simulate_public_agency_dealer_read:
                simulatePublicAgencyDealerRead();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void showTypeBarcodeScreen() {
        goTo(BarcodeReaderActivity.class);
    }

    private void simulatePublicAgencyDealerRead() {
        simulateRead(BarcodeTestCase.DEALER_PUBLIC_AGENCY_WITHOUT_CNPJ_BARCODE);
    }

    private void simulateCompanyDealerRead() {
        simulateRead(BarcodeTestCase.DEALER_COMPANY_WITHOUT_CNPJ_BARCODE);
    }

    private void simulateBankSlipRead() {
        simulateRead(BarcodeTestCase.BANK_SLIP_BARCODE);
    }

    private void simulateRead(String barcode) {
        Log.i(TAG,"Simulating read with barcode: " + barcode);
        Intent intent = new Intent();
        intent.putExtra(BARCODE_VALUE, barcode);
        setResult(BARCODE_SUCCESS, intent);
        finish();
    }

    private void changeFlashStatus() {
        useFlash = !useFlash;
    }

    private void restartCamera() {
        stopCamera();
        checkPermissionAndCreateCameraSource();
        startCameraSource();
    }
}
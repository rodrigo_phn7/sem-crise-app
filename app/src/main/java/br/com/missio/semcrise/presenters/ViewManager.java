package br.com.missio.semcrise.presenters;

import android.app.DatePickerDialog;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.TextView;

import com.andreabaccega.widget.FormAutoCompleteTextView;
import com.andreabaccega.widget.FormEditText;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import br.com.missio.semcrise.R;
import br.com.missio.semcrise.exceptions.NullCreditorException;
import br.com.missio.semcrise.model.Creditor;
import br.com.missio.semcrise.model.Expense;
import br.com.missio.semcrise.model.MonetaryEvent;
import br.com.missio.semcrise.model.Receipt;
import br.com.missio.semcrise.presenters.activities.SemCriseActivity;
import br.com.missio.semcrise.presenters.fragments.DatePickerDialogFragment;
import br.com.missio.semcrise.presenters.viewAdapters.CreditorListAdapter;
import br.com.missio.semcrise.services.CreditorsService;
import br.com.missio.semcrise.utils.DateHelper;
import lombok.Getter;
import lombok.Setter;

public class ViewManager implements View.OnClickListener, View.OnFocusChangeListener, DatePickerDialog.OnDateSetListener {

    private MonetaryEvent event;
    private ViewFinder viewFinder;
    private SemCriseActivity activity;
    private FormEditText dateEditText;
    private TextView creditorOrSourceTextView;
    @Getter
    private ImageButton editCreditorActionButton;
    private boolean isReceipt;
    @Getter
    private FormAutoCompleteTextView creditorOrSourceEditText;
    private CreditorsService creditorsService;
    @Getter
    @Setter
    private Date eventDate = Calendar.getInstance().getTime();
    private Creditor selectedCreditor;
    private Creditor preSelectedCreditor;

    public ViewManager(ViewFinder viewFinder, SemCriseActivity activity, MonetaryEvent event) {
        this.viewFinder = viewFinder;
        this.activity = activity;
        this.event = event;
        this.isReceipt = (event != null) && (event instanceof Receipt);
        creditorsService = new CreditorsService();
    }

    public void setUpCreditorOrSourceTextViewOrEditText(boolean isEditMode) {
        creditorOrSourceTextView = (TextView) viewFinder.findViewById(R.id.creditor_or_source_text_view);
        creditorOrSourceEditText = (FormAutoCompleteTextView) viewFinder.findViewById(R.id.creditor_or_source_edit_text);
        CreditorListAdapter creditorListAdapter = new CreditorListAdapter(activity,android.R.layout.simple_list_item_1,creditorsService.findAll());
        creditorOrSourceEditText.setAdapter(creditorListAdapter);
        creditorOrSourceEditText.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Creditor creditor = (Creditor) parent.getItemAtPosition(position);
                creditorOrSourceEditText.setText(creditor.getAvailableName());
                selectedCreditor = creditor;
            }
        });
        editCreditorActionButton = (ImageButton) viewFinder.findViewById(R.id.edit_creditor_or_source_image_button);
        editCreditorActionButton.setOnClickListener(this);

        TextView creditorOrSourceLabelTextView = (TextView) viewFinder.findViewById(R.id.creditor_or_source_label_text_view);

        int hint = isReceipt ? R.string.source_label : R.string.creditor_label;
        creditorOrSourceLabelTextView.setText(hint);

        String creditorOrSourceName = isReceipt ? ((Receipt)event).getSource() : getCreditorName();

        if(isEditMode || creditorOrSourceName == null) {
            creditorOrSourceEditText.setVisibility(View.VISIBLE);
            creditorOrSourceTextView.setVisibility(View.GONE);
            if(creditorOrSourceName != null) {
                creditorOrSourceEditText.setText(creditorOrSourceName);
            }
        } else {
            creditorOrSourceTextView.setText(creditorOrSourceName);
            editCreditorActionButton.setVisibility(View.VISIBLE);
        }
    }

    public Spinner createExpenseTypeSpinner(boolean isEditMode) {
        Spinner expenseTypeSpinner = (Spinner) viewFinder.findViewById(R.id.expense_type_spinner);
        ArrayAdapter<CharSequence> expenseTypeAdapter = ArrayAdapter.createFromResource(activity,R.array.string_array_expense_types,android.R.layout.simple_spinner_item);
        expenseTypeAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        expenseTypeSpinner.setAdapter(expenseTypeAdapter);
        if(isEditMode) {
            String positionString = ((Expense) event).getExpenseType().split(SemCriseActivity.HASHTAG_SEPARATOR)[1];
            expenseTypeSpinner.setSelection(Integer.valueOf(positionString));
        }
        return expenseTypeSpinner;
    }

    public Spinner createPaymentMethodSpinner(boolean isEditMode) {
        Spinner paymentMethodSpinner = (Spinner) viewFinder.findViewById(R.id.payment_method_spinner);
        ArrayAdapter<CharSequence> paymentMethodAdapter = ArrayAdapter.createFromResource(activity, R.array.string_array_payment_methods, android.R.layout.simple_spinner_item);
        paymentMethodAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        paymentMethodSpinner.setAdapter(paymentMethodAdapter);
        if(isEditMode) {
            String positionString = ((Expense) event).getPaymentMethod().split(SemCriseActivity.HASHTAG_SEPARATOR)[1];
            paymentMethodSpinner.setSelection(Integer.valueOf(positionString));
        }
        return  paymentMethodSpinner;
    }

    public TextView createMonetaryValueTextView() {
        TextView monetaryValueTextView = (TextView) viewFinder.findViewById(R.id.event_value_text_view);
        String moneyValueString = event.getMoneyValue().toString();
        monetaryValueTextView.setText(moneyValueString);
        return monetaryValueTextView;
    }

    public TextView createActualDateTextView() {
        TextView actualDateTextView = (TextView) viewFinder.findViewById(R.id.actual_date_text_view);
        Calendar cal = Calendar.getInstance();
        cal.setTime(event.getActualDate());
        String format = activity.getString(R.string.picker_date_format);
        String formattedDate = DateHelper.formatDate(cal.getTime(), format);
        actualDateTextView.setText(formattedDate);
        return actualDateTextView;
    }

    public FormEditText createExpectedDateEditText() {
        Calendar cal = getPreparedExpectedDate();
        String format = activity.getString(R.string.picker_date_format);
        String formattedDate = DateHelper.formatDate(cal.getTime(), format);
        dateEditText = (FormEditText) viewFinder.findViewById(R.id.expected_date_edit_text);
        dateEditText.setText(formattedDate);
        dateEditText.setOnClickListener(this);
        dateEditText.setOnFocusChangeListener(this);
        return dateEditText;
    }

    @NonNull
    private Calendar getPreparedExpectedDate() {
        Calendar cal = Calendar.getInstance();
        if(event != null) {
            eventDate = event.getExpectedDate();
            if(eventDate != null) {
                cal.setTime(eventDate);
            }
        }
        return cal;
    }

    @Override
    public void onClick(View v) {
        if(v == editCreditorActionButton) {
            creditorOrSourceEditText.setVisibility(View.VISIBLE);
            creditorOrSourceTextView.setVisibility(View.GONE);
            editCreditorActionButton.setVisibility(View.GONE);
            creditorOrSourceEditText.setText(creditorOrSourceTextView.getText().toString());
            creditorOrSourceEditText.requestFocus();
        } else if(v == dateEditText) {
            showDateDialog();
        }
    }

    @Override
    public void onFocusChange(View v, boolean hasFocus) {
        if((v == dateEditText) && hasFocus) {
            showDateDialog();
        }
    }

    private void showDateDialog() {
        DatePickerDialogFragment dialogFragment = new DatePickerDialogFragment();
        dialogFragment.setDateSetListener(this);
        dialogFragment.show(activity.getSupportFragmentManager(), DateHelper.DATE_DIALOG_TAG);
    }

    @Override
    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
        showSettedDateInEditText(year, monthOfYear, dayOfMonth);
        updateDate(year, monthOfYear, dayOfMonth);
    }

    private void showSettedDateInEditText(int year, int monthOfYear, int dayOfMonth) {
        Calendar cal = new GregorianCalendar(year, monthOfYear, dayOfMonth);
        String format = activity.getString(R.string.picker_date_format);
        String formattedDate = DateHelper.formatDate(cal.getTime(), format);
        dateEditText.setText(formattedDate);
    }

    private void updateDate(int year, int monthOfYear, int dayOfMonth) {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.DAY_OF_MONTH,dayOfMonth);
        calendar.set(Calendar.MONTH,monthOfYear);
        calendar.set(Calendar.YEAR,year);
        eventDate = calendar.getTime();
    }

    public void hideView(int viewId) {
        viewFinder.findViewById(viewId).setVisibility(View.GONE);
    }

    public void showView(int viewId) {
        viewFinder.findViewById(viewId).setVisibility(View.VISIBLE);
    }

    public String getCreditorName() {
        if(event != null) {
            preSelectedCreditor = creditorsService.getCreditorById(((Expense)event).getCreditorId());
            if(preSelectedCreditor != null) {
                return preSelectedCreditor.getAvailableName();
            }
        }
        return null;
    }

    public void closeDatabase() {
        creditorsService.closeDatabase();
    }

    public boolean isCreditorEdited() {
        return (editCreditorActionButton != null) && editCreditorActionButton.getVisibility() == View.GONE;
    }

    public String getTypedCreditorOrSourceName() {
        return (creditorOrSourceEditText != null) ? creditorOrSourceEditText.getText().toString() : null;
    }

    public String getCreditorId() throws NullCreditorException {
        String typedCreditorName = getTypedCreditorOrSourceName();
        if(preSelectedCreditor != null && typedCreditorName == null) {
            return preSelectedCreditor.getId();
        } else if(preSelectedCreditor != null){
            if(preSelectedCreditor.getAvailableName().equals(typedCreditorName)) {
                return preSelectedCreditor.getId();
            } else {
                return getNewCreditorIdFor(typedCreditorName);
            }
        } else {
            if(selectedCreditor == null && typedCreditorName != null) {
                return getNewCreditorIdFor(typedCreditorName);
            } else if(selectedCreditor != null && typedCreditorName != null) {
                if(selectedCreditor.getAvailableName().equals(typedCreditorName)) {
                    return selectedCreditor.getId();
                } else {
                    return getNewCreditorIdFor(typedCreditorName);
                }
            }
        }
        throw new NullCreditorException("Creditor is null on add new Expense. Verify it!");
    }

    private String getNewCreditorIdFor(String creditorName) {
        return creditorsService.createNewCreditorAndReturnId(creditorName);
    }
}
package br.com.missio.semcrise.utils;

public interface BarcodeType {
    String BANK_SLIP = "0";
    String DEALERS = "1";
    String NULL = "999";
}

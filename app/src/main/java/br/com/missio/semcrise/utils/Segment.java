package br.com.missio.semcrise.utils;

import lombok.Getter;

public enum Segment {
    CITY_HALL(1),
    SANITATION(2),
    GAS_AND_ELECTRIC_ENERGY(3),
    TELCOM(4),
    GOVERNMENT_AGENCIES(5),
    CNPJ_AGENCIES(6),
    TRAFFIC_TICKETS(7),
    BANK_EXCLUSIVE_USE(9);

    @Getter
    private int identification;

    Segment(int identification) {
        this.identification = identification;
    }
}

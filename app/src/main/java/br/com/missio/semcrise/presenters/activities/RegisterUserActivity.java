package br.com.missio.semcrise.presenters.activities;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;

import com.andreabaccega.widget.FormEditText;

import br.com.missio.semcrise.R;
import br.com.missio.semcrise.services.AlarmReceiver;
import br.com.missio.semcrise.services.UserService;
import br.com.missio.semcrise.utils.ConfigurationHelper;
import br.com.missio.semcrise.utils.PasswordNumberValidator;
import br.com.missio.semcrise.utils.ValidationHelper;

public class RegisterUserActivity extends SemCriseActivity implements View.OnClickListener {

    private Button registerButton;
    private FormEditText editTextUserName;
    private FormEditText editTextUserEmail;
    private FormEditText editTextUserPhone;
    private FormEditText editTextUserPassword;
    private FormEditText editTextUserConfirmPassword;
    private UserService userSevice;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_user);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        userSevice = new UserService(getApplicationContext());
        setSupportActionBar(toolbar);
        setUpUserInputData();
    }

    private void setUpUserInputData() {
        editTextUserName = (FormEditText) findViewById(R.id.editTextRegisterName);
        editTextUserEmail = (FormEditText) findViewById(R.id.editTextRegisterEmail);
        editTextUserPhone = (FormEditText) findViewById(R.id.editTextRegisterPhone);
        editTextUserPassword = (FormEditText) findViewById(R.id.editTextRegisterPassword);
        editTextUserPassword.addValidator(new PasswordNumberValidator(getString(R.string.error_minimum_digits)));
        editTextUserConfirmPassword = (FormEditText) findViewById(R.id.editTextRegisterConfirmPassword);
        editTextUserConfirmPassword.addValidator(new PasswordNumberValidator(getString(R.string.error_minimum_digits)));
        registerButton = (Button) findViewById(R.id.button_register);
        registerButton.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if((registerButton == v)){
            boolean formIsValid = validateForm();
            if(formIsValid) {
                registerUser();
                goTo(LoginActivity.class);
                finish();
            }
        }
    }

    private boolean validateForm() {
        FormEditText[] allFields = {editTextUserName, editTextUserEmail,editTextUserPhone,editTextUserPassword,editTextUserConfirmPassword};
        boolean allFieldsAreValid = ValidationHelper.validateEachField(allFields);
        boolean passConfirmed = confirmPass();
        return allFieldsAreValid && passConfirmed;
    }

    private boolean confirmPass() {
        String pass = editTextUserPassword.getText().toString();
        String confirmPass = editTextUserConfirmPassword.getText().toString();
        boolean passConfirmed = pass.equals(confirmPass);
        if(!passConfirmed){
            CharSequence notConfirmedPassMessage = getText(R.string.passwords_doesnt_match);
            editTextUserPassword.setError(notConfirmedPassMessage);
            editTextUserConfirmPassword.setError(notConfirmedPassMessage);
        }
        return passConfirmed;
    }

    private void registerUser() {
        String userName = editTextUserName.getText().toString();
        String userEmail = editTextUserEmail.getText().toString();
        String userPhone = editTextUserPhone.getText().toString();
        String userPassword = editTextUserPassword.getText().toString();
        putUserEmailInSharedPreferences(userEmail);
        userSevice.addUserWith(userName, userEmail, userPhone, userPassword);
    }

    private void putUserEmailInSharedPreferences(String userEmail) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(getString(R.string.key_pref_user_email),userEmail);
        editor.apply();
    }

}
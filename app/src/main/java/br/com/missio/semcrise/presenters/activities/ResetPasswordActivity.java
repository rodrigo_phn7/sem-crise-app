package br.com.missio.semcrise.presenters.activities;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.content.LocalBroadcastManager;
import android.view.View;
import android.widget.Toast;

import com.andreabaccega.widget.FormEditText;

import java.util.Calendar;

import br.com.missio.semcrise.R;
import br.com.missio.semcrise.presenters.fragments.ConfirmVerificationCodeFragment;
import br.com.missio.semcrise.presenters.fragments.ResetPasswordFragment;
import br.com.missio.semcrise.presenters.fragments.SendVerificationCodeFragment;
import br.com.missio.semcrise.services.VerificationCodeSenderService;
import br.com.missio.semcrise.utils.DateHelper;
import br.com.missio.semcrise.utils.ValidationHelper;

import static br.com.missio.semcrise.presenters.fragments.ConfirmVerificationCodeFragment.*;

public class ResetPasswordActivity extends SemCriseActivity implements OnVerificationCodeValidListener {

    private FormEditText loginEmailEditText;
    private String userEmail;
    private ProgressDialog progressDialog;
    private Calendar verificationCodeExpiration;
    private String verificationCode;
    private ConfirmVerificationCodeFragment secondStepFragment;
    private ResetPasswordFragment finalStepFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reset_password);
        setFirstStepFragment(savedInstanceState);
    }

    private void setFirstStepFragment(Bundle savedInstanceState) {
        if(hasFragmentContainerFrameLayout()) {
            if(savedInstanceState != null) {
                return;
            }
            SendVerificationCodeFragment firstStepFragment = new SendVerificationCodeFragment();
            getFragmentManager().beginTransaction()
                    .add(R.id.fragment_reset_pass_container,firstStepFragment).commit();
        }
    }

    private boolean hasFragmentContainerFrameLayout() {
        return findViewById(R.id.fragment_reset_pass_container) != null;
    }

    @Override
    protected void onResume() {
        super.onResume();
        setUpUser();
        setUpInputs();
        IntentFilter filter = new IntentFilter();
        filter.addAction(VerificationCodeSenderService.SEND_VERIFICATION_CODE_SUCCESS);
        filter.addAction(VerificationCodeSenderService.SEND_VERIFICATION_CODE_UNSUCCESS);
        LocalBroadcastManager.getInstance(this).registerReceiver(sendVerificationCodeBroadcast, filter);
    }

    @Override
    protected void onPause() {
        super.onPause();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(sendVerificationCodeBroadcast);
    }

    private void setUpUser() {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        userEmail = sharedPreferences.getString(getString(R.string.key_pref_user_email),"");
    }

    private void setUpInputs() {
        loginEmailEditText = (FormEditText) findViewById(R.id.login_email);

    }

    public void sendVerificationCode(View view) {
        boolean formIsValid = validateForm();
        if(formIsValid) {
            doSendVerificationCode();
        }
    }

    private boolean validateForm() {
        FormEditText[] allFields = {loginEmailEditText};
        boolean allFieldsAreValid = ValidationHelper.validateEachField(allFields);
        boolean emailConfirmed = confirmEmail();
        return allFieldsAreValid && emailConfirmed;
    }

    private boolean confirmEmail() {
        String emailFromView = loginEmailEditText.getText().toString();
        boolean confirmedEmail = userEmail.equals(emailFromView);
        if (!confirmedEmail) {
            loginEmailEditText.setError(getString(R.string.error_invalid_email));
        }
        return confirmedEmail;
    }

    private void doSendVerificationCode() {
        progressDialog = ProgressDialog.show(this, getString(R.string.please_wait), getString(R.string.sending_request), true, false);
        Intent service = new Intent(this, VerificationCodeSenderService.class);
        service.setAction(userEmail);
        startService(service);
    }

    private BroadcastReceiver sendVerificationCodeBroadcast = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if(VerificationCodeSenderService.SEND_VERIFICATION_CODE_SUCCESS.equals(action)) {
                progressDialog.dismiss();
                continueResetPassAction(intent.getStringExtra(VerificationCodeSenderService.VERIFICATION_CODE));

            } else {
                progressDialog.dismiss();
                Toast.makeText(ResetPasswordActivity.this, R.string.action_error, Toast.LENGTH_SHORT).show();
            }
        }
    };

    private void continueResetPassAction(String verificationCode) {
        verificationCodeExpiration = DateHelper.generateExpirationTime(DateHelper.HALF_TIME_INTERVAL);
        this.verificationCode = verificationCode;
        showSecondStepFragment();
    }

    private void showSecondStepFragment() {
        secondStepFragment = new ConfirmVerificationCodeFragment();
        showFragment(secondStepFragment);
    }

    public void validateCode(View view) {
        secondStepFragment.setVerificationCodeValidListener(this);
        secondStepFragment.validateCode(verificationCode, verificationCodeExpiration);
    }

    @Override
    public void onVerificationCodeValid() {
        showFinalStepFragment();
    }

    private void showFinalStepFragment() {
        finalStepFragment = new ResetPasswordFragment();
        showFragment(finalStepFragment);
    }

    private void showFragment(Fragment fragment) {
        FragmentManager fragmentManager = getFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.fragment_reset_pass_container, fragment);
        fragmentTransaction.commit();
    }

    public void resetPassword(View view) {
        finalStepFragment.resetPassword();
    }
}

package br.com.missio.semcrise.presenters.activities;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Calendar;
import java.util.Date;

import br.com.missio.semcrise.R;
import br.com.missio.semcrise.model.Expense;
import br.com.missio.semcrise.model.MonetaryEvent;
import br.com.missio.semcrise.model.Receipt;
import br.com.missio.semcrise.presenters.ViewFinder;
import br.com.missio.semcrise.presenters.ViewManager;
import br.com.missio.semcrise.presenters.fragments.ConfirmNegotiationDialogFragment;
import br.com.missio.semcrise.presenters.fragments.DatePickerDialogFragment;
import br.com.missio.semcrise.services.ExpensesService;
import br.com.missio.semcrise.services.NegotiationService;
import br.com.missio.semcrise.services.ReceiptsService;
import br.com.missio.semcrise.utils.DateHelper;

import static br.com.missio.semcrise.presenters.fragments.ConfirmNegotiationDialogFragment.CONFIRM_DIALOG_TAG;

public class ShowEventActivity extends SemCriseActivity implements ViewFinder,ConfirmNegotiationDialogFragment.OnConfirmNegotiationListener, DatePickerDialog.OnDateSetListener {

    private static final String TAG = "ViewPageNegotiation";
    private boolean isReceipt;
    private ExpensesService expensesService;
    private ReceiptsService receiptsService;
    private Receipt receipt;
    private Expense expense;
    private ProgressDialog progressDialog;
    private String eventId;
    private ViewManager viewManager;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_event);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        assert getSupportActionBar() != null;
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setUpMonetaryEventService();
    }

    private void setUpMonetaryEventService() {
        Intent intent = getIntent();
        eventId = intent.getAction();
        String eventType = intent.getStringExtra(SemCriseActivity.EVENT_TYPE_TAG);
        setEventType(eventType);
        isReceipt = MonetaryEvent.RECEIPT.equals(eventType);
        String title;
        if(isReceipt) {
            receiptsService = new ReceiptsService(this);
            receipt = receiptsService.findById(eventId);
            title = getString(R.string.receipt);
        } else {
            expensesService = new ExpensesService(this);
            expense = expensesService.findById(eventId);
            String especialExpenseType = intent.getStringExtra(ESPECIAL_EXPENSE_TYPE);
            if(especialExpenseType != null) {
                dismissNotification(especialExpenseType);
            }
            title = getString(R.string.expense);
        }
        setTitle(title);
    }

    @Override
    protected void onResume() {
        super.onResume();
        MonetaryEvent event = isReceipt ? receipt : expense;
        viewManager = new ViewManager(this,this,event);
        setUpNegotiateButton();
        setUpMakePaymentButton();
        setUpEventNameTextView();
        setUpMoneyValueTextView();
        setUpActualDateTextView();
        setUpExpenseTypeAndPaymentMethodTextView();
        setUpCreditorOrSource();
        IntentFilter filter = new IntentFilter();
        filter.addAction(NegotiationService.NEGOTIATION_SUCCESS);
        filter.addAction(NegotiationService.NEGOTIATION_UNSUCCESSFUL);
        LocalBroadcastManager.getInstance(this).registerReceiver(negotiationSuccessBroadcast, filter);
    }

    @Override
    protected void onPause() {
        super.onPause();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(negotiationSuccessBroadcast);
    }

    private void setUpNegotiateButton() {
        Button negotiateButton = (Button) findViewById(R.id.negotiate_button);
        MonetaryEvent e = isReceipt ? receipt : expense;
        if(e.isNegotiationActive() || !e.isLate() || isReceipt) {
            negotiateButton.setVisibility(View.GONE);
        }
    }

    private void setUpMakePaymentButton() {
        Button makePaymentButton = (Button) findViewById(R.id.make_payment_button);
        boolean isEffective = isReceipt ? receipt.isEffective() : expense.isEffective();
        if(isEffective) {
            makePaymentButton.setVisibility(View.GONE);
        }
    }

    private void setUpEventNameTextView() {
        TextView nameTextView = (TextView) findViewById(R.id.event_name_text_view);
        if (isReceipt) {
            nameTextView.setText(receipt.getName());
        } else {
            nameTextView.setText(expense.getName());
        }
    }

    private void setUpMoneyValueTextView() {
        viewManager.createMonetaryValueTextView();
    }

    public void setUpActualDateTextView() {
        viewManager.createActualDateTextView();
    }

    public void setUpExpenseTypeAndPaymentMethodTextView() {
        LinearLayout additionalInformationLinearLayout = (LinearLayout) findViewById(R.id.additional_information_linear_layout);
        if(isReceipt) {
            additionalInformationLinearLayout.setVisibility(View.GONE);
        } else {
            TextView expenseTypeTextView = (TextView) findViewById(R.id.expense_type_text_view);
            expenseTypeTextView.setText(expense.getCleanExpenseType());
            TextView paymentMethodTextView = (TextView) findViewById(R.id.payment_method_text_view);
            paymentMethodTextView.setText(expense.getCleanPaymentMethod());
        }
    }

    private void setUpCreditorOrSource() {
        viewManager.setUpCreditorOrSourceTextViewOrEditText(false);
        viewManager.getEditCreditorActionButton().setVisibility(View.GONE);
    }

    public void negotiate(View view) {
        showDialogToConfirmNegotiation();
    }

    private void showDialogToConfirmNegotiation() {
        ConfirmNegotiationDialogFragment dialogFragment = new ConfirmNegotiationDialogFragment();
        dialogFragment.setConfirmNegotiationListener(this);
        dialogFragment.show(getSupportFragmentManager(), CONFIRM_DIALOG_TAG);
    }

    @Override
    public void onConfirmNegotiation(boolean confirmResponse) {
        if(confirmResponse) {
            doNegotiation();
        } else {
            doNothingAndShowCancelMessage();
        }
    }

    private void doNegotiation() {
        progressDialog = ProgressDialog.show(this, getString(R.string.please_wait), getString(R.string.sending_request), true, false);
        Intent service = new Intent(this, NegotiationService.class);
        service.setAction(Expense.EXPENSE_ID_TAG + SemCriseActivity.HASHTAG_SEPARATOR + expense.getId());
        startService(service);
    }

    private void doNothingAndShowCancelMessage() {
        Toast.makeText(this, R.string.cancel_negotiation_message, Toast.LENGTH_SHORT).show();
    }

    private BroadcastReceiver negotiationSuccessBroadcast = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            Log.i(TAG, "Receiving action: " + action);
            if(NegotiationService.NEGOTIATION_SUCCESS.equals(action)) {
                progressDialog.dismiss();
                registerNegotiation();
                Toast.makeText(ShowEventActivity.this, R.string.negotiation_done_message, Toast.LENGTH_SHORT).show();
                finish();
            } else {
                progressDialog.dismiss();
                Toast.makeText(ShowEventActivity.this, R.string.action_network_error, Toast.LENGTH_LONG).show();
            }
        }
    };

    private void registerNegotiation() {
        expensesService.updateExpenseNegotiationStatus(expense, true);
    }

    public void makePayment(View view) {
        showDateDialog();
    }

    private void showDateDialog() {
        DatePickerDialogFragment dialogFragment = new DatePickerDialogFragment();
        dialogFragment.setDateSetListener(this);
        dialogFragment.show(getSupportFragmentManager(), DateHelper.DATE_DIALOG_TAG);
    }

    @Override
    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
        updateEffectiveDate(year, monthOfYear, dayOfMonth);
        int message;
        if(isReceipt) {
            message = R.string.receipt_effective_message;
        } else {
            message = R.string.expense_effective_message;
        }
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
        finish();
    }

    private void updateEffectiveDate(int year, int monthOfYear, int dayOfMonth) {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.DAY_OF_MONTH,dayOfMonth);
        calendar.set(Calendar.MONTH,monthOfYear);
        calendar.set(Calendar.YEAR, year);
        Date effectiveDate = calendar.getTime();
        if(isReceipt) {
            receiptsService.updateEffectiveDate(receipt,effectiveDate);
        } else {
            expensesService.updateEffectiveDate(expense, effectiveDate);
        }
    }

    @Override
    public void onDestroy() {
        if(isReceipt) {
            receiptsService.closeDatabase();
        } else {
            expensesService.closeDatabase();
        }
        viewManager.closeDatabase();
        super.onDestroy();
    }

    public void delete(View view) {
        doDelete();
    }

    private void doDelete() {
        if(isReceipt) {
            String expenseName = receipt.getName();
            receiptsService.delete(receipt);
            Toast.makeText(this, getString(R.string.receipt) + " " + expenseName + " " + getString(R.string.delete_success), Toast.LENGTH_SHORT).show();
        } else {
            String expenseName = expense.getName();
            expensesService.delete(expense);
            Toast.makeText(this, getString(R.string.expense) + " " + expenseName + " " + getString(R.string.delete_success), Toast.LENGTH_SHORT).show();
        }
        finish();
    }

    public void goToEditPage(View view) {
        Intent editIntent = new Intent(this,AddOrUpdateEventActivity.class);
        editIntent.putExtra(EVENT_TYPE_TAG,getEventType());
        editIntent.setAction(eventId);
        startActivity(editIntent);
    }
}

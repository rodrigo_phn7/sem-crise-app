package br.com.missio.semcrise.presenters;

import android.support.annotation.IdRes;
import android.view.View;

public interface ViewFinder {

    View findViewById(@IdRes int id);
}